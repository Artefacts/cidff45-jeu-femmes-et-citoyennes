## Femmes et Citoyennes, le jeu

Le CIDFF 45 et ses partenaires nous ont commandé un portage "en ligne" (sur le web) de leur jeu de plateau "Femmes et Citoyennes".

## installation

- installer un Wordpress tout 9
- installer le plugin Advanced Custom Fields (ACF)

- importer le SQL pour avoir une configuration pré-remplie "cidff45.sql"
  - `mysql -u <user> -p cidff45.sql`
  - ou avec phpmyadmin ;-)
- remplacer les urls:
  - le site du SQL pré-rempli est "https://cidff45.devhost"
  - j'utilise [wp-cli](https://wp-cli.org/) mais vous avez surement votre technique
    - `wp-cli search-replace 'https://cidff45.devhost' '<http[s]://votre nom de domaine>'`
    - `wp-cli search-replace 'root@comptoir.net' '<votre adresse email>'`
- changer le mot de passe de l'admin, ici c'est "SuperMan"
  - `wp-cli user update SuperMan --prompt=user_pass`
  - ou utiliser 'mot de passe oublié' si l'envoi d'email fonctionne
- installer le thème cidff45-jeu-femmes-et-citoyennes et le plugin cidffQuiz
  - en copiant les 2 dossiers dans les dossiers correspondant (thèmes et plugin)
  - ou avec git:
    - cd <wordpress folder>
    - git init
    - git remote add origin git@framagit.org:Artefacts/cidff45-jeu-femmes-et-citoyennes.git
    - git branch --set-upstream-to=origin/master master
    - git fetch
    - git reset --hard HEAD
- pour la suite consulter :
  - Thème [README](wp-content/themes/cidff45-jeu-femmes-et-citoyennes/README.md)
  - Plugin [README](wp-content/plugins/cidffQuiz/README.md)

