/**
 * Script to play on Quiz 
 */

Array.prototype.shuffle = function ()
{
    var l = this.length + 1;
    while (l--) {
        var r = ~~(Math.random() * l),
            o = this[r];
        this[r] = this[0];
        this[0] = o;
    }
    return this;
}

var Quiz = function( $, cidff_quiz, options )
{
	"use strict";

	console.log( cidff_quiz );

    var levels_labels = {
        //1: 'facile', 2: 'moyen', 3: 'difficile'
        1: 'découvrir', 2: 'approfondir'
    };

    var $screen = null ;
    var current_question = 0 ;
    var in_question = false ;
    var score = 0 ;
    var self = this ;

    // default config
	var config = {
		screen_id: 'quiz'
    };
    // override config with options parameter
	for( var a in config )
	{
		if( typeof options[a] !== 'undefined' )
			config[a] = options[a];
    }
    //console.log( config );

    var data = cidff_quiz.quiz ;
    // Index ordonné des questions pour la partie
    var play_questions = [];

    /**
     * Gère le formulaire pour paramétrer le Quiz
     * - nombre de questions par catérogie,
     * - choix des catégories
     * - choix des levels
     * Le bouton "lancer le Quiz n'est activé que si les paramètres sont correctes.
     * Le nombre de questions pour la partie est recalculé à chaque changement des paramètres.
     */
    this.settings = function()
    {
        $screen = $('#'+config.screen_id);

        $screen
            .removeClass('is-answer')
            .removeClass('is-question')
            .removeClass('is-finish');

        $('.quiz-steps', $screen).hide();
        $('.score', $screen).hide();
        $('.finish', $screen).hide();
        $('.answer', $screen).hide();
        $('.question', $screen).hide();
        $('.responses', $screen).hide();
        $('.navigation', $screen).hide();

        $('#quiz-presets-modal', $screen).on('show.bs.modal', fill_presets_dialog);
        $('.quiz-presets-button', $screen).on('click', function(evt)
        {
            $('#quiz-presets-modal', $screen).modal();
        });

        var settings = {
            category_questions_count: 0,
            categories: [],
            levels: []
        };
        var $settings = $('.settings', $screen);

        $('.play_questions_count', $settings ).html( '?' );

        // Categories:

        var $categories = $('.categories', $settings );
        var cat_count = 0 ;
        for( var i in data.categories )
        {
            var c = data.categories[i] ;
            //console.log( c );

            var $li = $('li',$categories).eq(cat_count);
            // Add a item if needs
            if( $li.length == 0 )
            {
                $li = $('li:first()',$categories)
                    .clone()
                    .appendTo( $categories );
            }
            $li.show();
            //$('input', $e).attr('value', c.id);
            //$('span', $e).html( c.label );
            var $e ;
            $e = $('input', $li);
            $e.attr('id', $e.attr('id').replace(/[0-9]+/, i+1) );
            $e.attr('value', c.id );
            $e = $('label', $li);
            $e.attr('for', $e.attr('for').replace(/[0-9]+/, i+1) );
            $e.html( c.label );

            cat_count ++ ;
        }
        // hide unused items
        $('li:nth-child('+(cat_count)+') ~ li',$categories)
            .hide();

        // Levels
        var $levels = $('.levels', $settings );
        for( var i=0; i<data.consts.level_max; i++ )
        {
            var $li = $('li',$levels).eq(i);
            // Add a item if needs
            if( $li.length == 0 )
            {
                $li = $('li:first()',$levels)
                    .clone()
                    .appendTo( $levels );
            }
            $li.show();
            var $e ;
            $e = $('input', $li);
            $e.attr('id', $e.attr('id').replace(/[0-9]+/, i+1) );
            $e.attr('value', i+1 );
            $e = $('label', $li);
            $e.attr('for', $e.attr('for').replace(/[0-9]+/, i+1) );
            $e.html( levels_labels[i+1] );
        }

        $('form', $settings).on('change', function(e)
        {
            //settings.category_questions_count = $('input[name="category_questions_count"]', $settings).val();
            settings.category_questions_count = $('input[name="number[]"]:checked', $settings).val();
            settings.categories = [];
            $('.categories input:checked', $settings).each(function(i,e)
            {
                settings.categories.push( $(e).val() );
            });
            settings.levels = [];
            $('.levels input:checked', $settings).each(function(i,e)
            {
                settings.levels.push( $(e).val() );
            });

            //console.log(settings);
            $('.play_questions_count', $settings ).html( '0' );
            $('.btn-start', $settings).attr('disabled',true);

            if( settings.category_questions_count < 1 )
            {}                
            else if( settings.categories.length < 1 )
            {}
            else if( settings.levels.length < 1 )
            {}
            else
            {
                build_questions_list(
                    settings.categories,
                    settings.category_questions_count,
                    settings.levels
                    );

                /*
                console.log('Questions count:', play_questions.length);
                for( var i=0; i<play_questions.length; i++ )
                {
                    console.log( play_questions[i], data.questions[play_questions[i]].category);
                }
                */

                if( play_questions.length > 0 )
                {
                    $('.btn-start', $settings).attr('disabled',false);
                    $('.play_questions_count', $settings ).html( play_questions.length );
                }

            }

        });

        $('.btn-start', $settings)
            .attr('disabled',true)
            .on('click', function()
            {
                self.start();
            });
    }

    this.start = function()
    {
        $screen = $('#'+config.screen_id);

        $('.settings', $screen).hide();
        $('.score', $screen).hide();
        $('.finish', $screen).hide();
        $('.answer', $screen).hide();

        $('.question', $screen).show();
        $('.responses', $screen).show();
        $('.navigation', $screen).show();

        current_question = 0 ;
        //console.log( 'start() play_questions:', play_questions );
        question( play_questions[current_question] );
        steps_update();

        $('.quiz-steps', $screen).show();

        $('.next-btn', $screen).on('click', function(ev)
        {
        	//console.log('next-btn');
            next();
        });

    };

    function fill_presets_dialog( evt )
    {
        //console.log('fill_presets_dialog()', data.presets);
        var $dlg = $(evt.target);
        var $presets = $('ul.presets', $dlg);

        // unselect "li" presets
        $('li', $presets).removeClass('selected');
        // disable "presets-done" button
        $('.presets-done',$dlg).prop('disabled',true);

        // already filled
        if( $presets.children().length > 0 )
            return ;

        // fill "ul" with presets
        data.presets.forEach( function(p, i)
        {
            $('<li data-preset="'+i+'">'+p.title+' ('+p.questions.length+')'+'</li>').appendTo($presets);
        });

        // click on presets "li"
        $('li', $presets).on('click', function(ev)
        {
            var $p = $(ev.target);
            if( $p.hasClass('selected'))
                return ;
            $('li', $presets).removeClass('selected');
            $p.addClass('selected');
            $('.presets-done',$dlg).prop('disabled',false);
        });

        // click on "presets-done"
        $('.presets-done', $dlg).on('click', function(ev)
        {
            $dlg.modal('hide');

            var p = $('li.selected', $presets).data('preset');
            play_questions = data.presets[p].questions;

            self.start();
        });
    };

    function build_questions_list( category_ids, cat_quest_count, levels)
    {
        var qIds = Object.keys(data.questions);
        play_questions = [] ;

        // pour compter l'utilisation des catégories
        var cat_sels = {};
        category_ids.forEach(function(e,i)
        {
            cat_sels[e] = 0;
        });

        // passe les questions au crible des options demandées,
        // après les avoirs mélangées
        qIds.shuffle().forEach( function( qId, i)
        {
            var q = data.questions[qId];
            //console.log(i,qId, q);

            // dans une catégorie sélectionnée ?
            if( typeof cat_sels[q.category] === 'undefined' )
                return ;
            // le nombre de questions dans cette catégorie est-il atteint ?
            if( cat_sels[q.category] == cat_quest_count )
                return ;
            // yat'il des levels définis ?
            if( typeof levels !== 'undefined')
            {
                // if CidffPlugin::QUESTION_LEVEL_MAX has changed but questions not updated
                if( q.level > data.consts.level_max )
                    q.level = data.consts.level_max ;
                // La question correspont-elle ?
                if( levels.indexOf( q.level ) < 0 )
                    return ;
            }
            cat_sels[q.category] ++ ;
            play_questions.push(qId);
        });
    }

    /**
     * Display the Quiz steps.
     */
    function steps_update()
    {
        //console.log(current_question, play_questions.length);

        $('.steps-counter .current',$steps).html( current_question + 1 );
        $('.steps-counter .total',$steps).html( play_questions.length );

        var $steps = $('.quiz-steps .steps-percent', $screen);
        if( current_question == 0 )
        {
            $steps.children().remove();
            $('<div class="step past"></div>').appendTo($steps);
            for( var i=0; i<(play_questions.length-1); i++ )
                $('<div class="step"></div>').appendTo($steps);
        }
        else
        {
            $steps
                .children('div:nth-child('+(current_question + 1)+')')
                .addClass('past');
        }
    }

    /**
     * Quiz next step : Question or Answer
     */
    function next()
    {
    	//console.log('next() in_question:'+in_question+', current_question:'+current_question);

        if( in_question )
        {
            response( play_questions[current_question] );
        }
        else
        {
            current_question ++ ;
            if( current_question == play_questions.length )
                finish();
            else
            {
                question( play_questions[current_question] );
                steps_update();
            }
        }
    }

    /**
     * Display a Question
     */
    function question( question_id )
    {
        in_question = true ;
        var q = data.questions[question_id];

        if( q === undefined )
        {
            alert('Question id='+question_id+' non trouvée !');
            window.location.reload();
            return ;
        }

        $screen.removeClass('is-answer').addClass('is-question');

        $('.next-btn-validation', $screen).show();
        $('.next-btn-question', $screen).hide();
        $('.next-btn-last', $screen).hide();        	

        $('.answer', $screen).hide();

        $('.theme', $screen ).html( data.categories[q.category].label );

        $('.question .text', $screen)
            .html(
                q.question.replace(/\n/g,'<br/>')
            );

        var $responses = $('.responses ul', $screen );

        // Update or create Question's responses
		var responses_count = 0;
		var good_answers_count = 0 ;
        //q.responses.forEach( function( e, i)
        for( var i in q.choices )
        {
        	var c = q.choices[i] ;
            //console.log( 'choice: '+i, 'c:', c );

            if( typeof c.good !=='undefined' && c.good )
            	good_answers_count ++ ;

            var a = $('li',$responses).eq(i);
            // Add a item if needs
            if( a.length == 0 )
            {
                a = $('li:first()',$responses)
                    .clone()
                    .appendTo( $responses );
            }
            a.removeClass('active good bad')
            	.html( c.text )
            	.show();
            responses_count ++ ;
        };
        // hide unused items
        $('li:nth-child('+(responses_count-1)+') ~ li',$responses)
            .hide();

        //console.log('good_answers_count : '+good_answers_count );
        switch( good_answers_count )
        {
		case 0:
            //$('.help', $screen).html( 'Aucun choix, discutez :-)');
            $('.help', $screen).html('');
    		break;
		case 1:
    		//$('.help', $screen).html( '1 choix possible');
            $('.help', $screen).html('');
    		break;
    	default:
            //$('.help', $screen).html( good_answers_count + ' choix possibles');
            $('.help', $screen).html('Plusieurs réponses possibles');
        }

		$('.next-btn', $screen).prop('disabled',
			good_answers_count > 0 ? true : false
		);

        // remove before add, to don't have several times the event handler.
        $('li', $responses).off('click');
        // when more than one reponse, make them clickable.
        if( good_answers_count > 0 )
        {
            $responses.addClass('in-question');
            $('li', $responses).on('click', function(ev)
	        {
	            click_response( ev.target, good_answers_count );
	        });
        }
    }

    /**
     * Handle a Answer click
     */
    function click_response( ele, answers_count )
    {
        if( ! in_question )
            return ;

        if( answers_count == 0 )
            return ;

        // one good answer
        if( answers_count == 1 )
        {
            // One active response at a time
            $('.responses li', $screen).removeClass('active');
            $(ele).addClass('active');
            // Enable 'Next' button
            $('.next-btn', $screen).prop('disabled', false);
            return ;
        }

        // more than one good answer
        var resp_count = $('.responses li.active', $screen).length;
        if( $(ele).hasClass('active') )
        {
            $(ele).removeClass('active');
            resp_count -- ;
        }
        else if( resp_count < answers_count )
        {
            $(ele).addClass('active');
            resp_count ++ ;
        }
        //if( resp_count == answers_count )
        if( resp_count > 0 )
            $('.next-btn', $screen).prop('disabled', false);
        else
            $('.next-btn', $screen).prop('disabled', true);

    }

    /**
     * Display an Answer.
     */
    function response( question_id )
    {
        in_question = false ;
        var q = data.questions[question_id];

        $screen
            .removeClass('is-question')
            .addClass('is-answer');

        $('.next-btn-validation', $screen).hide();

        // Last question ?
        if( (current_question+1) == play_questions.length )
    	{
            $('.next-btn-question', $screen).hide();
            $('.next-btn-last', $screen).show();        	
    	}
        else
    	{
            $('.next-btn-question', $screen).show();
            $('.next-btn-last', $screen).hide();
    	}

        $('.help', $screen).html( 'réponse');

        $('.answer', $screen)
        	.html(
                q.answer.replace(/\n/g,'<br/>')
            )
        	.slideToggle();

        var $responses = $('.responses ul', $screen); 

        $responses.removeClass('in-question');
        $('li', $responses).off('click');

        var good_answers = 0,
        	good_responses = 0 ;

        for( var i in q.choices )
        {
        	var c = q.choices[i];
        	var r = $('li',$responses).eq(i);
        	if( typeof c.good !=='undefined' && c.good )
        	{
        		//console.log('good: '+i);
        		good_answers ++ ;
        		if( r.hasClass('active') )
    			{
            		r.removeClass('active');
            		good_responses ++ ;
    			}
        		r.addClass('good');
        	}
        	else
    		{
        		//console.log('bad: '+i);
        		if( r.hasClass('active') )
        			r.addClass('bad');
        		r.removeClass('active')
    		}
        }

        // Ne pas compter les questions ouvertes (sans bonne réponse).
        // @see #74
        if( good_answers > 0 )
        {
            score += good_answers == good_responses ? 1 : 0 ;
        }
        else
        {
            score ++ ;
        }

        if( typeof answer_post !=='undefined' )
        {
            $.post( answer_post, {
            	_token: window._token,
            	question_id: q.id,
            	response: r.index() + 1
            });
        }

    }

    /**
     * Display Finish screen
     */
    function finish()
    {
    	//console.log( 'finish()');

        $screen
            .removeClass('is-question')
            .removeClass('is-answer')
            .addClass('is-finish');

        $('.responses',$screen).hide();
        $('.question',$screen).hide();
        $('.answer',$screen).hide();
        $('.next-btn',$screen).hide();
        $('.quiz-steps', $screen).hide();

        $('.finish', $screen).show();
        $('.score', $screen).show();

        if( score > 1 )
        {
            $('.score .score-plural', $screen).show();
            $('.score .score-singular', $screen).hide();
        }
        else
        {
            $('.score .score-plural', $screen).hide();
            $('.score .score-singular', $screen).show();
        }
        $('.score .good-responses-count', $screen).html( score );
        $('.score .questions-count', $screen).html( play_questions.length );

        if( data.text_end )
    	{
        	$('.score .text-end', $screen).html( data.text_end );	
    	}

        $('.score', $screen).show();

    }

};
