<?php
/**
 * Plugin Name
 *
 * @package           CidffQuiz
 * @author            Artéfacts, Cyrille Giquello
 * @copyright         2019 Artéfacts and CIDFF-45
 * @license           GPL-2.0-or-later (Wordpress compatible)
 *
 * @wordpress-plugin
 * Plugin Name:       CIDFF Quiz
 * Plugin URI:        https://framagit.org/Artefacts/cidff45-jeu-femmes-et-citoyennes
 * Description:       Description of the plugin.
 * Version:           1.0.0
 * Requires at least: 5.2
 * Requires PHP:      7.3
 * Author:            Artéfacts, Cyrille Giquello
 * Author URI:        https://framagit.org/Cyrille37
 * Text Domain:       cidff-quiz
 * License:           GPL v2 or later (Wordpress compatible)
 * License URI:       https://www.gnu.org/licenses/old-licenses/gpl-2.0.txt
 */

require_once ( __DIR__.'/src/CidffPlugin.php');

//register_uninstall_hook(__FILE__, '\CidffPlugin\Install::wp_uninstall' );
register_activation_hook( __FILE__, 'CidffPlugin\Installer::wp_activation' );
register_deactivation_hook( __FILE__, 'CidffPlugin\Installer::wp_deactivation' );

$cidffPlugin = \CidffPlugin\CidffPlugin::getInstance() ;
