<?php
/**
 * Question form, question's view
 * used by CidffAdmin::wp_edit_form_after_editor()
 */
namespace CidffPlugin ;

/**
 * @var mixed $data
 */

?>

<h2>
    <?php if(isset($errors[CidffPlugin::CPT_CARD.'-question']) ){ ?>
        <span class="dashicons dashicons-warning" style="color: red;"></span>
    <?php } ?>
    La question
</h2>

<?php
// https://developer.wordpress.org/reference/functions/wp_editor/
// https://developer.wordpress.org/reference/classes/_wp_editors/parse_settings/
wp_editor( wpautop($data,true), CidffPlugin::CPT_CARD.'-question', array(
    'wpautop' =>  true,
    'media_buttons' => false,
    'textarea_name' => CidffPlugin::CPT_CARD.'-question',
    'textarea_rows' => 6,
    // (bool) Whether to output the minimal editor config. Examples include Press This and the Comment editor. Default false.
    'teeny'  =>  true,
    //'tinymce' => false,
    /*'quicktags' => array(
        'buttons' => 'em', // quicktags.dev.js:573  Uncaught TypeError: Cannot read property 'callback' of undefined
    ),*/
    'quicktags' => false,
    'tinymce'       => [
        'toolbar1'      => 'bold,italic,underline,separator,separator,link,unlink,undo,redo',
        'toolbar2'      => '',
        'toolbar3'      => '',
    ],
));
