<?php
/**
 * _quizPreset.php
 * 
 * Quiz preset form
 * used by CidffAdminQuizPreset::wp_edit_form_after_editor()
 * 
 * Variables from includer:
 * - $post_ids
 * 
 */
namespace CidffPlugin ;

$post_id = $_REQUEST['post'];
//$post_ids = get_post_meta( $post_id, CidffPlugin::META_PRESET, true );

/**
 * @var mixed $questions
 */

?>
<div id="cidff-quizpreset">
    <p>&nbsp;</p>
    <h3>Importer liste de questions:</h3>
        <input type="file" name="cidff_preset_file" class="button button-secondary" />
        <input type="submit" value="Importer" class="button button-primary" />

    <h3>Exporter ce quiz:</h3>
        <?php if( empty($post_ids) ) { ?>
            <p>Le Quiz est vide.</p>
        <?php } else { ?>
            <p>
            <a href="<?php echo add_query_arg(['action'=>CidffAdminQuizPreset::ACTION_EXPORTPRESET,'post_ID'=>$post_id]); ?>"
            class="button button-secondary" target="_blank"
            >Exporter</a>
            </p>
        <?php } ?>

    <h3>Liste des questions:</h3>

    <?php if( count($questions) == 0 ) { ?>
        <p>Aucune question.</p>

    <?php } else { ?>
        <table>
            <thead>
                <tr>
                    <th>#</th>
                    <th>ID</th>
                    <th>Catégorie</th>
                    <th title="Q: QCM, F: Forum">Type</th>
                    <th title="Nombre de choix">Choix</th>
                    <th title="Nombre de bonnes réponses">Rép.</th>
                    <th title="Niveau de difficulté">Niv.</th>
                    <th>Question</th>
                </tr>
            </thead>
            <tbody>
                <?php for( $i=0; $i<count($questions); $i++ ) {
                    $q = $questions[$i];
                ?>
                <tr>
                    <td><?php echo $i+1 ?></td>
                    <td style="white-space: nowrap;">
                        <?php echo $q->id ?>
                        <a href="<?php echo get_edit_post_link($q->id) ?>" style="text-decoration: none;"><span class="dashicons dashicons-edit"></span></a>
                    </td>
                    <td>
                        <?php if( empty($q->category) ) { ?>
                            <i style="color:red;">sans catégorie</i>
                        <?php } else { ?>
                            <b><?php echo $q->category ?></b>
                        <?php } ?>
                    </td>
                    <td style="text-align: center"><?php echo $q->type ?></td>
                    <td style="text-align: center"><?php echo $q->choix ?></td>
                    <td style="text-align: center"><?php echo $q->responses ?></td>
                    <td style="text-align: center"><?php echo $q->level ?></td>
                    <td><?php echo $q->title ?></td>
                </tr>
                <?php } ?>
            </tbody>
        </table>
    <?php } ?>

</div>
