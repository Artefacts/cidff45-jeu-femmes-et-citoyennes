<?php
namespace CidffPlugin ;

/**
 * Variables:
 * - $categories_status
 */
?>

<div>
	<h3>Gestion des PDF par catégorie de question:</h3>
	<ul>
	<?php foreach( $categories_status as $status ) { ?>
		<li>
			<span class="name">
				<?php echo $status['term']->name ; ?>
				(<?php echo $status['term']->count ; ?>)
			</span>
			<span class="pdf">
				<?php if( $status['pdf'] ) { ?>
					<a class="download" href="<?php echo $status['pdf']->guid; ?>" target="_blank">
						<?php echo $status['pdf']->post_title; ?>
					</a>
					<?php echo 'le '.get_post_datetime($status['pdf'],'post_modified','gmt')
						->format(get_option('date_format').' à '.get_option('time_format')).'.';
					?>
				<?php } else { ?>
				<?php } ?>
				<br/>
				<a href="#" class="button button-primary btn-generate-dialog" data-slug="<?php echo $status['term']->slug ; ?>">Générer le PDF</a>
			</span>
			<?php if( ! empty($status['alerts']) ) { ?>
				<span class="alert">
					<?php foreach( $status['alerts'] as $alert ) { ?>
						<?php echo $alert; ?>&nbsp;
					<?php } ?>
				</span>
			<?php } ?>
		</li>
	<?php } ?>
	</ul>
</div>

<!-- PDF maker Modal dialog -->
<div id="<?php echo CidffPlugin::PLUGIN_NAME; ?>-dialog" class="hidden" style="min-width:300px; max-width:1000px">

	<?php
	//global $wp_query;
	//CidffPlugin::debug(__METHOD__,'$wp_query:',$wp_query);

	$taxo = CidffPlugin::TAX_CARD ;
	// https://codex.wordpress.org/Function_Reference/wp_dropdown_categories
    wp_dropdown_categories(
    [
        'id' => 'cat-generate',
        'show_option_all' =>  'Toutes les catégories',
        'taxonomy'        =>  $taxo,
        'name'            =>  $taxo,
        'value_field' => 'slug', // use "slug" to be compatible with default list filter in category column
        'orderby'         =>  'name',
        //'selected'        =>  isset($wp_query->query[$taxo]) ? $wp_query->query[$taxo] : null,
        'hierarchical'    =>  true,
        //'depth'           =>  3,
        'show_count'      =>  true, // Show # questions count
        'hide_empty'      =>  true, // Don't show term w/o question
    ]);
    ?>
	<button class="button button-primary btn-generate" >Générer le PDF</button>.

	<!-- 3 blocks:
	   - wait-box : at dialog open, explain what to do.
	   - progress-box : while making pdf, display progress information
	   - result-box : when pdf done, display result information
	 -->
	<div class="report-container">
		<div class="report-box">
			<div class="progress-box hidden" >
    			<div class="progressbar">
		    	</div>
	    	</div>
			<div class="result-box hidden" >
	    	</div>
			<div class="wait-box" >
				Choisissez une catégorie et cliquer sur "générer le PDF".
	    	</div>
    	</div>
	</div>

</div>
