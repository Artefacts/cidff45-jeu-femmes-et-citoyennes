<?php
/**
 * Question form, choices's view
 * used by CidffAdmin::wp_edit_form_after_editor()
 */
namespace CidffPlugin ;

/**
 * @var mixed $data
 * 
    Array
    (
        [0] => Array
            (
                ['good'] => on
                ['text'] => azerty
            )
    
        [1] => Array
            (
                ['text'] => qwerty
            )
    )
 */

//CidffPlugin::debug( 'View "choices": ', $data );

?>

<style type="text/css">

#cidff_card-choices-table .dashicons.choice-action:hover {
    color: orange;
}
#cidff_card-choices-table .dashicons.choice-action-disabled {
    color: lightgray;
}
  
</style>

<h2>
	<?php if(isset($errors[CidffPlugin::CPT_CARD.'-choices']) ){ ?>
        <span class="dashicons dashicons-warning" style="color: red;"></span>
    <?php } ?>
	Les choix
</h2>

<div>
	<table id="cidff_card-choices-table">
		<thead>
			<tr>
				<th>Bonne réponse</th>
				<th>Texte</th>
				<th>Actions</th>
			</tr>
		</thead>
		<tbody>
			<?php
			$last_idx = count($data)-1;
			foreach( $data as $idx => $choice )
			{
			    $prefix = CidffPlugin::CPT_CARD.'-choices['.$idx.']' ;
			    //CidffPlugin::debug( '"$choice": ', $choice );
		    ?>
			<tr>
				<td>
					<input type="checkbox"
						name="<?php echo $prefix.'[good]' ?>"
					   <?php echo ( isset($choice['good']) ? ( $choice['good'] ? 'checked="checked"' : '' ) : '') ?>
					   />
				</td>
				<td>
					<input type="text" size="32" name="<?php echo $prefix.'[text]' ?>"
						value="<?php echo ( isset($choice['text']) ? $choice['text'] : '' ) ?>"
						/>
				</td>
				<td class="choice-actions">
					<?php if( $idx > 0 ) { ?>
					<span class="dashicons dashicons-arrow-up-alt choice-action choice-up"></span>
					<?php } else { ?>
					<span class="dashicons dashicons-arrow-up-alt choice-action-disabled choice-up"></span>
					<?php } ?>
					<?php if( $idx < $last_idx ) { ?>
					<span class="dashicons dashicons-arrow-down-alt choice-action choice-down"></span>
					<?php } else { ?>
					<span class="dashicons dashicons-arrow-down-alt choice-action-disabled choice-down"></span>
					<?php } ?>
					<span class="dashicons dashicons-trash choice-action choice-trash"></span>
					<span class="dashicons dashicons-plus choice-action choice-add"></span>
				</td>
			</tr>
			<?php } ?>
		</tbody>
		<tfoot>
		</tfoot>
	</table>
</div>

<script type="text/javascript">

(function($)
{
	"use strict" ;

	var table = $('#cidff_card-choices-table');

	var bActions = $('td.choice-actions',table);

	$('.choice-up', bActions).on('click', function(ev)
	{
		var $e = $(ev.target);
		if( ! $e.hasClass('choice-action') )
			return ;
		var $tr = $e.parents('tr');
		var $p = $tr.prev();
		$tr.detach().insertBefore( $p );
		updateIndexes();
	});

	$('.choice-down', bActions).on('click', function(ev)
	{
		var $e = $(ev.target);
		if( ! $e.hasClass('choice-action') )
			return ;
		var $tr = $e.parents('tr');
		var $n = $tr.next();
		$tr.detach().insertAfter( $n );
		updateIndexes();
	});

	$('.choice-trash', bActions).on('click', function(ev)
	{
		var $e = $(ev.target);
		if( ! $e.hasClass('choice-action') )
			return ;
		var $tr = $e.parents('tr');
		$tr.remove();
		updateIndexes();
	});

	$('.choice-add', bActions).on('click', function(ev)
	{
		var $e = $(ev.target);
		if( ! $e.hasClass('choice-action') )
			return ;
		var tr = $e.parents('tr');
		tr.clone(true,true).insertAfter(tr);
		updateIndexes();
	});

	var fieldname_regex = new RegExp('\\d+') ;

	/**
	 * Order all fields name array index,
	 * and refresh buttons.
	 */
	function updateIndexes()
	{
		// rename input fields
		$('tbody tr',table).each( function(i1,$tr)
		{
			$('input',$tr).each( function(i2,$input)
			{
				$input.name = $input.name.replace( fieldname_regex, i1 );
			} );
		} );
		// refresh
		bActions = $('td.choice-actions',table);
		// Compute enables/disabled actions
		if( bActions.length == 1 )
		{
			$('.choice-trash',bActions).removeClass('choice-action').addClass('choice-action-disabled');
			$('.choice-up',bActions).removeClass('choice-action').addClass('choice-action-disabled');
			$('.choice-down',bActions).removeClass('choice-action').addClass('choice-action-disabled');
		}
		else
		{
			$('.choice-trash',bActions).removeClass('choice-action-disabled').addClass('choice-action');
			$('.choice-up',bActions).removeClass('choice-action-disabled').addClass('choice-action');
			$('.choice-up',bActions.first() ).removeClass('choice-action').addClass('choice-action-disabled');
			$('.choice-down',bActions).removeClass('choice-action-disabled').addClass('choice-action');
			$('.choice-down',bActions.last() ).removeClass('choice-action').addClass('choice-action-disabled');
		}
	}

})(jQuery);

</script>
