<?php
/**
 * Question form, level's view
 * used by CidffAdmin::wp_edit_form_after_editor()
 */
namespace CidffPlugin ;

/**
 * @var mixed $data
 */

?>

<h2>
    <?php if(isset($errors[CidffPlugin::CPT_CARD.'-level']) ){ ?>
        <span class="dashicons dashicons-warning" style="color: red;"></span>
    <?php } ?>
    Le niveau (difficulté)
</h2>

<select type="text" size="1" name="<?php echo CidffPlugin::CPT_CARD.'-level' ?>"
    >
    <?php for( $l=CidffPlugin::QUESTION_LEVEL_MIN; $l<=CidffPlugin::QUESTION_LEVEL_MAX; $l ++) { ?>
    <option value="<?php echo $l ?>"
        <?php if( $l == $data ) echo 'selected="selected"'; ?>
        ><?php echo CidffPlugin::QUESTION_LEVEL_LABELS[$l];?></option>
    <?php } ?>
</select>
