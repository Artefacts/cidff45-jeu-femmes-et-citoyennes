<?php

namespace CidffPlugin ;

if( isset($errors) && ! empty($errors) )
{
    ?>
    <div id="cidff-errors">
    <?php
    foreach( $errors as $error )
    {
    ?>
        <div class="error">
            <p><?php echo $error; ?></p>
        </div>
    <?php
    }
    ?>
    </div>
    <?php
}
