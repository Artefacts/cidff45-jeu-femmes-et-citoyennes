<?php
namespace CidffPlugin ;

?>
<!--
    button button-primary 
-->
<a class="button button-primary"
    href="<?php echo add_query_arg('action', CidffAdminQuizPreset::ACTION_EXPORTALL); ?>"
    target="_blank" >
    Télécharger toutes les questions
</a>
