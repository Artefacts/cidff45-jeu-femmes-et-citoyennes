<?php
namespace CidffPlugin ;

/**
 * Variables: none
 */

?>

<h3>Configuration</h3>

<div>
<?php
//global $wp_query;
//CidffPlugin::debug(__METHOD__,'$wp_query:',$wp_query);

$taxo = CidffPlugin::TAX_CARD ;
// https://codex.wordpress.org/Function_Reference/wp_dropdown_categories
wp_dropdown_categories(
[
    'id' => 'cat-generate',
    'show_option_all' =>  'Toutes les catégories',
    'taxonomy'        =>  $taxo,
    'name'            =>  $taxo,
    'value_field' => 'slug', // use "slug" to be compatible with default list filter in category column
    'orderby'         =>  'name',
    //'selected'        =>  isset($wp_query->query[$taxo]) ? $wp_query->query[$taxo] : null,
    'hierarchical'    =>  true,
    //'depth'           =>  3,
    'show_count'      =>  true, // Show # questions count
    'hide_empty'      =>  true, // Don't show term w/o question
]);
?>
</div>

<div>
<textarea id="cidff-pdf-config" name="cidff-pdf-config" rows="16" cols="20" style="float: left;"></textarea>
</div>
<br style="clear: both;"/>

<div>
	<label for="cidff-question-first">1ère question:</label>
	<input id="cidff-question-first" type="number" value="0"/>
</div>
<div>
	<label for="cidff-questions-max">Nombre de questions maxi:</label>
	<input id="cidff-questions-max" type="number" value="15"/>
</div>

<div>
<button id="cidff-preview-btn" class="button button-primary" >Visualier le PDF</button>.
</div>

<br style="clear: both;"/>
