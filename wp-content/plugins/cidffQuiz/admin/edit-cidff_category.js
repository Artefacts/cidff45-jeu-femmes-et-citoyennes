/**
 * edit-cidff_category.js
 */
(function($)
{
    // hack #22 to remove size because WP make width & size = 1 in wp_get_attachment_image_src()
    // let taxonomy-term-image plugin set size with style ;-)
    $('table.wp-list-table tbody tr td.column-term_image img.attachment-thumbnail')
        .removeAttr('width').removeAttr('height');

})(jQuery);
