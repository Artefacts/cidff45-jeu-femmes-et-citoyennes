/**
 * cidff_card.js
 */
(function($)
{
    //console.log('cidff-card', cidff_card);

    var $div = $('#submitpost');

    // Hide preview button
    $('#preview-action', $div)
        .hide();

    // Replace post_publish date by post_modified date
    var d = new Date(cidff_card.post.post_modified);
    $('#misc-publishing-actions .misc-pub-curtime', $div)
        .hide()
        .after(
            '<div class="misc-pub-section curtime misc-pub-curtime">'
            + '<span class="post-modified">Mise à jour le&nbsp;: <b>'+d.toLocaleString()+'</b></span>'
            + '</div>'    
        );

})(jQuery);
