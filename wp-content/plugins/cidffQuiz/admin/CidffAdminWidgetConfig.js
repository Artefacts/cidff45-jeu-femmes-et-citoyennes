/**
 * CidffQuiz plugin, admin side script.
 */

function updateQueryStringParameter(uri, key, value) {
	  var re = new RegExp("([?&])" + key + "=.*?(&|$)", "i");
	  var separator = uri.indexOf('?') !== -1 ? "&" : "?";
	  if (uri.match(re)) {
	    return uri.replace(re, '$1' + key + "=" + value + '$2');
	  }
	  else {
	    return uri + separator + key + "=" + value;
	  }
	};

var CidffAdminWidgetConfig = function($)
{
	//"use strict";
	//console.debug('CidffAdminWidgetConfig()', 'cidff_config:', cidff_config);

	/**
	 * The Widget.
	 */
	var $w = $('#'+cidff_config.widget_id);

	var windowName = 'cidff-pdf-preview' ;

	$('#cidff-preview-btn').on('click', function(ev)
	{
		ev.preventDefault();

		var pdf_config = null ;
		try {
			var pdf_config = encodeURIComponent(
				JSON.stringify(
					JSON.parse($('#cidff-pdf-config').val())
				));
		}
		catch(ex)
		{
			alert('Json invalid');
			return ;
		}

		var cat_slug = $('#cat-generate').val();
		if( cat_slug.length < 2 )
		{
			alert('cat_slug invalid');
			return ;
		}

		var url = cidff_config.urls.preview_pdf ;

		url = updateQueryStringParameter(
				url, 'cat_slug', cat_slug );
		url = updateQueryStringParameter(
				url , 'pdf_config', pdf_config );
		url = updateQueryStringParameter(
				url , 'question_first', $('#cidff-question-first').val() );
		url = updateQueryStringParameter(
				url , 'questions_max', $('#cidff-questions-max').val() );
		var w = window.open( url, windowName );
	});

	$('#cat-generate', $w).on('change', function(ev)
	{
		var slug = $(ev.currentTarget).val();
		switch( slug )
		{
		case 0:
		case '0':
		case '':
	    	$('.btn-generate', $w).prop('disabled',true);
			break;
		default:
	    	$('.btn-generate', $w).prop('disabled',false);
		}

		if( cidff_config.pdf_configs[slug] )
		{
			$('#cidff-pdf-config', $w).val( JSON.stringify(cidff_config.pdf_configs[slug],null,1) );			
		}
		else
		{
			$('#cidff-pdf-config', $w).val('');
		}

	});

};

(function($, document)
{
	//console.debug('ajaxurl: ' + ajaxurl);
	//console.debug( cidff_quiz );

	var plug = new CidffAdminWidgetConfig($);

})(jQuery,document);
