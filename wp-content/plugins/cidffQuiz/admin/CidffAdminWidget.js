/**
 * CidffQuiz plugin, admin side script.
 */

var CidffAdminWidget = function($)
{
	"use strict";
	console.log('CidffAdminWidget()');
	console.debug( cidff_quiz );

	/**
	 * The Widget.
	 */
	var $w = $('#'+cidff_quiz.widget_id);

	var $progressLabel ;
	var $dialogContainer ;

	/**
	 * The modal dialog.
	 */
	var $dialog = $('#'+cidff_quiz.dialog_id).dialog(
	{
		title: 'Générer les fichiers PDF',
		modal: true,
		width: 'auto',
		//dialogClass: 'wp-dialog',
		autoOpen: false,
		draggable: true,
		resizable: true,
		closeOnEscape: false,
		create: function()
		{
	    },
	    open: function()
	    {
	    	$dialogContainer = $dialog.parent('.ui-dialog');

	    	/*
	        // close dialog by clicking the overlay behind it
	        $('.ui-widget-overlay').bind('click', function()
    		{
	        	$dialog.dialog('close');
	        });
	        */
	    	// enable Close button
	    	$('.ui-dialog-titlebar-close', $dialogContainer).prop('disabled',false);

	    	$('.wait-box', $dialog).show();

	    	$('.result-box', $dialog).hide();
	    	$('.progress-box', $dialog).hide();

			$('#cat-generate', $dialog)
				.prop('disabled',false)
				// Trigger onchange to set ihm according to selectBox value.
				.trigger('change');

	    },
	});

	/**
	 * Create progess bar.
	 */
	var $progressbar = $('.progressbar', $dialog).progressbar({
        value: false
	});
	// Add a label over the progress bar
	$progressLabel = $('<span class="progressbar-label">...</span>');
	$('.ui-progressbar-value', $progressbar )
		.append( $progressLabel );

	/**
	 * Category selectBox change.
	 */
	$('#cat-generate').on('change', function(ev)
	{
		var slug = $(ev.currentTarget).val();
		console.log('onchange() slug='+slug);
		switch( slug )
		{
		case null:
		case 0:
		case '0':
		case '':
	    	$('.btn-generate', $dialog).prop('disabled',true);
			break;
		default:
	    	$('.btn-generate', $dialog).prop('disabled',false);
		}
	});

	/**
	 * Button open dialog "generate PDF".
	 */
	$('.btn-generate-dialog', $w).on('click', function(ev)
	{
		ev.preventDefault();

		// Preset category selectBox
		$('#cat-generate').val( $(ev.target).data('slug') );
		// Open dialog
		$dialog.dialog('open');
	});

	/**
	 * Button launch "generate PDF"
	 */
	$('.btn-generate', $dialog).on('click', function(ev)
	{
		ev.preventDefault();

		var cat_slug = $('#cat-generate').val();
		if( cat_slug.length <= 1)
		{	// Should not append ...
			alert('Erreur: Catégorie invalide !');
		}

		// disable buttons
		$('.btn-generate', $dialog).prop('disabled',true);
		$('#cat-generate', $dialog).prop('disabled',true);
    	$('.ui-dialog-titlebar-close', $dialogContainer).prop('disabled',true);

    	// hide blocks
		$('.wait-box',$dialog).hide();
		$('.result-box',$dialog).hide();

		// show blocks
    	$('.progress-box', $dialog).show();

    	makePdf( ajaxurl, cat_slug, $progressbar );
	});

	var makePdfDone = function( msg )
	{
		// enable buttons
		$('.btn-generate', $dialog).prop('disabled',false);
		$('#cat-generate', $dialog).prop('disabled',false);
		$('.ui-dialog-titlebar-close', $dialogContainer).prop('disabled',false);

		// hide blocks
    	$('.progress-box', $dialog).hide();

    	// show Result block
		try
		{
			var d = JSON.parse( msg );
			$('.result-box',$dialog)
				.html( d.msg )
				.show();
		}
		catch(ex)
		{
			$('.result-box',$dialog)
				.html( 'Error: '+ex+' with message "'+msg+'"' )
				.show();			
		}

	};

	var makePdfFail = function( err )
	{
		// enable buttons
		$('.btn-generate', $dialog).prop('disabled',false);
		$('#cat-generate', $dialog).prop('disabled',false);
		$('.ui-dialog-titlebar-close', $dialogContainer).prop('disabled',false);

		// hide blocks
    	$('.progress-box', $dialog).hide();

    	// show Result block
		$('.result-box',$dialog)
			.html( 'Error: '+ err )
			.show();
	};

	/**
	 * Demande et suivi de la génération. 
	 */
	var makePdf = function( url, cat_slug, $progressbar )
	{
	    var data = {
			'action': 'makePdf',
			'cat_slug': cat_slug
		};

	    /**
	     * Decode message "m" as Json,
	     * then display it with $progressLabel. 
	     */
		var progressDisplay = function( m )
		{
			//console.log( m );
			var s ;
			try
			{
				var d = JSON.parse( m );
				switch( d.type )
				{
				case 'progress':
					var item ;
					s = '' ;
					for( var i in d.items )
					{
						item = d.items[i];
						s += (s!='' ? ', ' : '') ;
						s += item[0]+': '+item[1]+'/'+item[2] ;
					}
					break;
				case 'done':
					s = d.msg ;
					break;
				default:
					s = 'Unknow message: ' + m ;
					break;
				}				
			}
			catch(ex)
			{
				s='Error '+ ex+' with message "'+m+'"';
			}

			$progressLabel.html(s);
		};

		var lines = [];
		var linesCount = 0 ;
		var linesProgCount = 0 ;

		$.ajax( url,
		{
			method: 'POST',
			data: data,
			xhrFields:
			{
				/**
				 * Connexion à l'event de progression.
				 * Les données de progression de l'evt sont invalides,
				 * on prend celles contenues dans la réponse http.
				 */
				onprogress: function (evt)
				{
			    	var resp = evt.currentTarget.response.split("\n");
			    	var sCount = 0 ;
			        for( var i in resp )
			        {
			        	var s = resp[i].trim();
			        	if( s == '' )
			        		continue ;
			        	if( linesCount <= sCount )
			        	{
				        	lines.push( s);
				        	linesCount ++ ;
			        	}
			        	sCount ++ ;
			        }
			        //console.log( lines );
			        if( linesProgCount < lines.length )
			        	progressDisplay( /*linesProgCount+'/'+linesCount+' -> '+*/ lines[linesProgCount ++]);

				}// onprogress()
			},
		})
		.done(function( ev )
		{
			console.debug('ajax success.')
			// dequeue any remaining messages.
	    	while( linesProgCount < lines.length )
	    	{
	    		progressDisplay( /*linesProgCount+'/'+linesCount+' -> '+*/ lines[linesProgCount ++]);
	    	}
			//console.debug( lines );
			makePdfDone( lines[lines.length-1] );
		})
		.fail(function( ev )
		{
			console.error('ajax fail', ev);
			makePdfFail( ev );
		})
		.always(function() {
			console.debug('ajax done.')
		})
		;
	};// makePdf()

};

(function($, document)
{
	//console.log('ajaxurl: ' + ajaxurl);
	//console.log( cidff_quiz );
	
	var plug = new CidffAdminWidget($);

})(jQuery,document);
