# CidffQuiz

## Libraries & Plugins

### Site target

#### WP Plugin for Image on Taxonomy

https://github.com/daggerhart/taxonomy-term-image

Pull request - merged !:
- [Permits SVG image rendering](https://github.com/daggerhart/taxonomy-term-image/pull/12)

#### Make PDF with TCPDF

6.3.2-patch-waiting-PR146

Pull request:
- bug correction for ImageSVG [PR146](https://github.com/tecnickcom/TCPDF/pull/146)

### Performance

#### APC Object Cache Backend

- Par Mark Jaquith
- https://fr.wordpress.org/plugins/apc/

### Developpement

#### Debug Bar

- Par wordpressdotorg
- https://fr.wordpress.org/plugins/debug-bar/

#### Query Monitor

- Par John Blackbourn
- https://fr.wordpress.org/plugins/query-monitor/

## TODO

### data validation

Décidément, ce `wp_insert_post_empty_content` n'est pas le bon filter à utiliser. Même l'upload d'image passe par là, et aucun paramètre n'est fournit pour détecter le type de contenu ... Quelle galère :{
[CPT meta validation](#2) #2

### capabilities

Comme pour les Posts.

## Documentation

### Wordpress Javascript & CSS

- Default Scripts and JS Libraries Included and Registered by WordPress
    - https://developer.wordpress.org/reference/functions/wp_enqueue_script/

### Custom fields & Meta box

Don't use metaboxes with TinyMCE !
https://make.wordpress.org/core/2012/12/01/more-hooks-on-the-edit-screen/

- [Adding Custom Meta Boxes to the WordPress Admin Interface](https://www.sitepoint.com/adding-custom-meta-boxes-to-wordpress/)
- [Adding Custom Fields To WordPress Programmatically](https://code.tutsplus.com/tutorials/adding-custom-fields-to-wordpress-programmatically--cms-20838)
    - add_post_meta()
- [add WYSIWYG Editor to TextArea Custom Meta Box in WordPress](https://code.rohitink.com/2017/11/30/add-wysiwyg-editor-textarea-custom-meta-box-wordpress/)
    - wp_editor()

- https://themefoundation.com/wordpress-meta-boxes-guide/

Metaboxes et Ajax:

- https://developer.wordpress.org/plugins/metadata/custom-meta-boxes/

### Long running script

Php side:
- https://stackoverflow.com/questions/7049303/show-progress-for-long-running-php-script

Javascript side:
- // http://www.dave-bond.com/blog/2010/01/JQuery-ajax-progress-HMTL5/

### JQuery-UI

- progressbar https://www.tutorialspoint.com/jqueryui/jqueryui_progressbar.htm

