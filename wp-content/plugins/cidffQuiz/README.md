# Cidff45 Jeu Femmes et citoyennes

Un Quiz pour jouer et apprendre à se confronter à ses droits et devoirs de femme citoyenne.

Projet piloté par le [CIDFF Loiret](http://www.infofemmes.com/v2/p/Contact/cidff-du-loiret-orl-ans/2318) (*Centre d'information sur les droits des femmes et des familles du Loiret*).

Plugin Wordpress:
- Front office: Jouer en ligne, télécharger des PDF par catégorie de questions ;
- Back office: Gestion des questions et catégories, générer les PDF ;
    - Image en SVG pour les fonds de carte des catégories
	- Génération PDF avec TCPDF

## Mise en place

- Le plugin Advanced Custom Fields (ACF) doit être installé et activé
- Le plugin Classic editor et Courant Alternatif (non obligatoire) est un plus
- Créer des catégories dans "Questions Quiz" et leur ajoindre une image SVG pour les fonds de cartes utilisés pour la génération des PDF pour jouer en physique (*le plugin Taxonomy Term Image est intégré dans le plugin cidffQuiz*)
- Créer des questions et les assigner aux catégories du Quiz
- Créer un page avec pour Template celui fourni dans le plugin `wp-content/plugins/cidffQuiz/theme-front/templates/page_technical_quiz.php`
- La génération des PDF se fait sur le dashboard
  - la configuration de la mise en page des PDF via le bouton "Configurer" dans la barre du Widget !
- Liste des liens pour le téléchargement avec le shortcode `quiz_cards_download`.

## More inside

- Utilise le Custom Post Type (CPT) 'cidff_card' (`CidffPlugin\CidffPlugin::CPT_CARD`)

Pour les notes techniques voir le fichier [README-DEV.md](README-DEV.md).

<img alt="Image de Framasoft: Liberté, Égalité et Fraternité"
	src="https://framasoft.org/img/fr/stallmanoramix.png" width="260" style="float:right;" />

Ce programme est publié sous licence «[GPLv2 or later](https://www.gnu.org/licenses/old-licenses/gpl-2.0.html)» par ses auteurs :
- Cyrille Giquello via la scop Artéfacts
- ...
