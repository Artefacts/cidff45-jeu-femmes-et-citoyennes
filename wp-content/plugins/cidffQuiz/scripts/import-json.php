#!/usr/bin/env php
<?php
/**
 * Parce qu'il y a des questions saisies au format json.
 * 
 * run with wordpress folder and json file :
 * $ wordpress/wp-content/plugins/cidffQuiz/scripts/import-json.php ./wordpress ./PoC/quiz-01.json
 * Optionnal quizz categoy "slug" !! NE FONCTIONNE PAS : Term not found !!
 * $ wordpress/wp-content/plugins/cidffQuiz/scripts/import-json.php ./wordpress ./PoC/quiz-01.json etre_citoyenne
 *
 */

if( $argc < 3 )
    die('Need the Wordpress folder and the json file.'."\n");

// wordpress folder

$wp_dir = $argv[1];
if( ! file_exists($wp_dir.'/wp-load.php') )
    die('Bad wordpress folder'."\n");

// json file

//$file = '/home/cyrille/Taf/CIDFF-45/PoC/quiz-01.json' ;
$file = $argv[2];
if( ! file_exists($file) )
    die('Bad json file'."\n");

// optional category slug
if( $argc == 4 )
    $cat_slug = $argv[3];
else
    $cat_slug = null ;

//
// End of initialiaation
//

require_once( $wp_dir. '/wp-load.php' );

require_once ( __DIR__.'/../src/CidffPlugin.php');
use CidffPlugin\CidffPlugin;
//new \CidffPlugin\CidffPlugin() ;
use CidffPlugin\Installer;
Installer::register_post_type();

// Retrieve the optional category

if( $cat_slug )
{
    // https://developer.wordpress.org/reference/functions/get_term_by/
    // get_term_by( string $field, string|int $value, string $taxonomy = '', string $output = OBJECT, string $filter = 'raw' )
    if( empty( $term ))
    $term = get_term_by('slug', $cat_slug, CidffPlugin::TAX_CARD);
        die('Term "'.$cat_slug.'" not found.'."\n");
    print_r( $term );
}

$data = json_decode( file_get_contents($file) );
if( empty($data) )
    die('Failed to decode file ['.$file.']'."\n");

foreach( $data as $datum )
{
    if( empty($datum->q) )
        continue ;

    $choices = [];

    foreach( $datum->r as $ctext)
    {
        $choices[] = [
            'text' => $ctext
        ];
    }

    if( is_array($datum->a) )
    {
        foreach( $datum->a as $idx )
            $choices[$idx]['good'] = 'on' ;
    }
    else if( is_numeric($datum->a) )
    {
        $choices[$datum->a]['good'] = 'on' ;
    }

    $meta = [
        CidffPlugin::META_QUESTION => $datum->q,
        CidffPlugin::META_CHOICES => $choices,
        CidffPlugin::META_ANSWER => $datum->at,
    ];

    $post = [
        'post_type' => CidffPlugin::CPT_CARD,
        'post_status'  => 'publish', // 'draft',
        'meta_input' => $meta,
        /* Does not work because no current user
        'tax_input'    => array(
            CidffPlugin::TAX_CARD => [$cat_slug],
        ),*/
    ];

    $post_id = wp_insert_post( $post, $wp_error = true );
    if( $post_id instanceof \WP_Error )
        die( $post_id->get_error_message()."\n");

    // https://developer.wordpress.org/reference/functions/wp_set_object_terms/
    // wp_set_object_terms( int $object_id, string|int|array $terms, string $taxonomy, bool $append = false )
    wp_set_object_terms( $post_id, $term->term_id, CidffPlugin::TAX_CARD );
}
