#!/usr/bin/env php
<?php
/**
 * Parce qu'il y a des questions saisies au format json.
 * 
 * run with wordpress folder and json file :
 * $ wordpress/wp-content/plugins/cidffQuiz/scripts/import-json.php ./wordpress ./PoC/quiz-01.json
 * Optionnal quizz categoy "slug" !! NE FONCTIONNE PAS : Term not found !!
 * $ wordpress/wp-content/plugins/cidffQuiz/scripts/import-json.php ./wordpress ./PoC/quiz-01.json etre_citoyenne
 *
 */

use CidffPlugin\CidffPlugin;

if( $argc != 2 )
    die('Need the Wordpress folder.'."\n");

// wordpress folder

$wp_dir = $argv[1];
if( ! file_exists($wp_dir.'/wp-load.php') )
    die('Bad wordpress folder'."\n");

require_once( $wp_dir. '/wp-load.php' );

require_once ( __DIR__.'/../src/CidffPlugin.php');

/**
 * @var \wpdb $wpdb
 */
global $wpdb ;
$rows = $wpdb->get_results( $wpdb->prepare( 'SELECT ID, post_title, post_name FROM '.$wpdb->posts.' WHERE post_type = %s and post_status="publish"', CidffPlugin::CPT_CARD ), ARRAY_A) ;
$stats=[
    'rows' => 0,
] ;
foreach( $rows as $row )
{
    $stats['rows'] ++ ;
    $question = get_post_meta( $row['ID'], CidffPlugin::META_QUESTION, true);

    $row['post_title'] = wp_trim_words($question) ;
    $row['post_name'] = sanitize_title($question);

    var_dump($row);
    if ( false === $wpdb->update( $wpdb->posts, $row, $where=['ID'=>$row['ID']] ) )
    {
        throw new \RuntimeException('Failed to update');
    }
}

echo '=== Stats:',"\n",print_r($stats,true),"\n";
