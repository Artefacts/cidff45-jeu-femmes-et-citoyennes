<?php
/**
 * Template Name: Technical Quiz Page
 *
 * The template for playing on Quiz.
 *
 * @package Cidff
 * @subpackage Quiz
 * @since 1.0.0
 */

/**
 * TODO : how to remove header and footer but get enqueued scripts and styles ?
 */

?><!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<link rel="profile" href="https://gmpg.org/xfn/11" />
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
    <?php wp_body_open(); ?>
    <div id="page" class="site">
    	<div id="content" class="site-content">

            <div id="cidff-quiz">

                <div class="steps">
                    <div class="step step-first">
                        <span class="badge active">first</span>
                    </div>
                    <div class="step step-next">
                        <hr/>
                        <span class="badge">next</span>
                    </div>
                </div>

                <div class="question">
                    <h3 class="text">Question ?</h3>
                </div>

                <div class="answer" >
                    <p>Réponse</p>
                </div>

                <div class="responses">
                	<p class="help"></p>
                    <ul>
                        <li>réponse 1</li>
                        <li>réponse 2</li>
                        <li>réponse 3</li>
                    </ul>
                </div>

                <div class="finish">
                    <h3 class="text">Votre score</h3>
                </div>

                <div class="score">
                    <div class="score-plural">                    
                        <p>Vous avez <span class="good-responses-count">0</span> bonnes réponses sur <span class="questions-count">0</span> questions.</p>
                    </div>
                    <div class="score-singular">
                        <p>Vous avez <span class="good-responses-count">0</span> bonne réponse sur <span class="questions-count">0</span> questions.</p>
                    </div>
                    <div class="text-end">
                    </div>
                </div>

                <div class="navigation">
                    <button class="btn btn-success next-btn next-btn-validation" disabled="disabled" >réponse</button>
                    <button class="btn btn-success next-btn next-btn-question" disabled="disabled" >suivant</button>
                    <button class="btn btn-success next-btn next-btn-last" disabled="disabled" >voir score</button>
                    <button class="btn btn-warning btn-finish" >terminer</button>
                </div>

                <div class="settings">
                    <p>Configurez la partie :</p>
                    <form id="settings">
                    Nombre de questions par catégorie <input type="number" name="category_questions_count" min="1" max="100" value="5" />
                    <br/>
                    Catégories:
                    <ul class="categories">
                        <li>
                            <input type="checkbox" name="categories[]" id="settings-category-01"/>
                            <label for="settings-category-01">Catégorie 1</label>
                        </li>
                    </ul>
                    Difficultés:
                    <ul class="levels">
                        <li>
                            <input type="checkbox" name="levels[]" id="settings-level-01"/>
                            <label for="settings-level-01">Level 1</label>
                        </li>
                    </ul>
                    <p>Nombre de questions pour la partie: <span class="play_questions_count"></span></p>
                    <button type="button" class="btn btn-success btn-start" >lancer le Quiz</button>
                    </form>
                </div>
            </div>

    	</div><!-- /#content -->
	</div><!-- /#page -->

<?php wp_footer(); ?>

<script type="text/javascript">

(function($, document)
{
	console.log('doc ready');
	new Quiz( $, cidff_quiz, {
		screen_id: 'cidff-quiz'
	} ).settings();

})(jQuery,document);

</script>
</body>
</html>
