<?php
namespace CidffPlugin ;

require_once( __DIR__.'/Quiz/Quiz.php');

use Cidff\Quiz\Quiz;
use Cidff\Quiz\Category ;
use Cidff\Quiz\Question ;

class CidffQuizPlay
{
    public $questions_per_category = 5 ;
    /**
     * etre_citoyenne, etre_en_couple
     * @var array
     */
    public $category_slugs = [] ;
}

class CidffQuiz 
{
 
    /*
    protected static function getSqlPostsByCategory( \WP_Term $term, $questions_max = null, $question_first = 0, $random = false  )
    {
        
        $sql = '
        SELECT
            P.*,
            PM_Q.meta_value as question,
            PM_C.meta_value as choices,
            PM_A.meta_value as answer,
            PM_L.meta_value as level
        FROM wp_posts P
            left join wp_postmeta PM_Q on (PM_Q.post_id = P.ID AND PM_Q.meta_key = "'.CidffPlugin::META_QUESTION.'")
            left join wp_postmeta PM_C on (PM_C.post_id = P.ID AND PM_C.meta_key = "'.CidffPlugin::META_CHOICES.'")
            left join wp_postmeta PM_A on (PM_A.post_id = P.ID AND PM_A.meta_key = "'.CidffPlugin::META_ANSWER.'")
            left join wp_postmeta PM_L on (PM_L.post_id = P.ID AND PM_L.meta_key = "'.CidffPlugin::META_LEVEL.'")
            left join wp_term_relationships TR on TR.object_id = P.ID
        WHERE
            P.post_type="'.CidffPlugin::CPT_CARD.'"
            AND P.post_status = "'.'publish'.'"
            AND TR.term_taxonomy_id = '.$term->term_id.'
        ';
        if( $random )
            $sql.=' ORDER BY RAND()';
        if( ! empty($questions_max) )
        {
            $sql .= ' LIMIT '.intval($question_first).','.intval($questions_max);
        }
        return $sql ;
    }
    */

    protected static function getSqlPosts( Array $category_ids, $questions_max = null, $question_first = 0, $random = true )
    {
        $sql = '
        SELECT
            P.ID,
            TR.term_taxonomy_id as category_id,
            PM_Q.meta_value as question,
            PM_C.meta_value as choices,
            PM_A.meta_value as answer,
            PM_L.meta_value as level
        FROM wp_posts P
            left join wp_postmeta PM_Q on (PM_Q.post_id = P.ID AND PM_Q.meta_key = "'.CidffPlugin::META_QUESTION.'")
            left join wp_postmeta PM_C on (PM_C.post_id = P.ID AND PM_C.meta_key = "'.CidffPlugin::META_CHOICES.'")
            left join wp_postmeta PM_A on (PM_A.post_id = P.ID AND PM_A.meta_key = "'.CidffPlugin::META_ANSWER.'")
            left join wp_postmeta PM_L on (PM_L.post_id = P.ID AND PM_L.meta_key = "'.CidffPlugin::META_LEVEL.'")
            left join wp_term_relationships TR on TR.object_id = P.ID
        WHERE
            P.post_type="'.CidffPlugin::CPT_CARD.'"
            AND P.post_status = "'.'publish'.'"
            AND TR.term_taxonomy_id IN ('.implode(',',$category_ids).')
        ';
        if( $random )
            $sql.=' ORDER BY RAND()';
        if( ! empty($questions_max) )
        {
            $sql .= ' LIMIT '.intval($question_first).','.intval($questions_max);
        }
        return $sql ;
    }

    public static function getByCategory( \WP_Term $term, $questions_max = null, $question_first = 0, $random=true )
    {
        global $wpdb ;

        // Init the Quiz Category with term data
        $term = get_term_by( 'slug', $term->slug, CidffPlugin::TAX_CARD );
        //CidffPlugin::debug(__METHOD__,'$term:', $term);
        
        $imageFile = get_attached_file( $term->term_image );
        //CidffPlugin::debug(__METHOD__,'$image:', $imageFile );
        
        $category = (new Category())
            ->setSlug( $term->slug )
            ->setLabel( $term->name )
            ->setBackgroundFile( $imageFile )
        ;

        foreach( $wpdb->get_results(
                self::getSqlPosts( [$term->term_id], $questions_max, $question_first, $random)
                )
            as $post )
        {
            //CidffPlugin::debug(__METHOD__,'$post:', $post);
            
            $choices = maybe_unserialize( $post->choices );
            
            $category
                ->addCard((new Question())
                    ->setQuestion( $post->question )
                    ->setResponses( $choices )
                    ->setAnswer( $post->answer )
                );
            
        }
        
        $quiz = new Quiz();
        $quiz->addCategory( $category );
        
        return $quiz ;
    }

    public static function getPlay()
    {
        global $wpdb ;

        // Toutes les catégories du Quiz qui ont des cartes.
        $terms = get_terms( array(
            'taxonomy' => CidffPlugin::TAX_CARD,
            'hide_empty' => true,
        ) );
        //CidffPlugin::debug('$terms',$terms);

        $flat_quiz = [
            'consts' => [
                'level_max' => CidffPlugin::QUESTION_LEVEL_MAX
            ],
            'categories' => [],
            'questions' => [],
            'presets' => [],
        ];

        // fill $flat_quiz.categories

        $term_ids = [];
        foreach( $terms as $term )
        {
            $term_ids[] = $term->term_id ;
            // don't need image file
            //$imageFile = get_attached_file( $term->term_image );
            $flat_quiz['categories'][$term->term_id] = [
                'id' => $term->term_id,
                'slug' => $term->slug,
                'label' => $term->name,
                //'image' => $imageFile,
            ];
        }

        // fill $flat_quiz.questions

        //CidffPlugin::debug( self::getSqlPosts( $term_ids ) );
        foreach( $wpdb->get_results( self::getSqlPosts( $term_ids ) )
            as $post )
        {
            $choices = maybe_unserialize( $post->choices );

            $flat_quiz['questions'][$post->ID]=[
                //'id' => $post->ID,
                'category' => $post->category_id,
                'question' => $post->question,
                'level' => ($post->level==null ? 1*CidffPlugin::QUESTION_LEVEL_MIN : $post->level),
                'choices' => $choices,
                'answer' => $post->answer,
            ];

        }

        // fill $flat_quiz.presets

        $query = new \WP_Query( [
            'post_type' => CidffPlugin::CPT_QUIZPRESET,
            'post_status' => array( 'publish' ),
            /*'fields' => 'ids',*/
            'nopaging' => true,
        ] );
        if( $query->have_posts() )
        {
            foreach( $query->posts as $post )
            {
                $questions = get_post_meta( $post->ID, CidffPlugin::META_PRESET, true );
                if( !empty($questions)  )
                {
                    $flat_quiz['presets'][] = [
                        'title'=>$post->post_title,
                        'questions' => $questions,
                    ];
                }
            }    
        }

        return $flat_quiz ;
    }

    /*
    public static function getPlay_1( CidffQuizPlay $play = null )
    {
        global $wpdb ;

        if( $play == null )
            $play = new CidffQuizPlay() ;

        $terms = null ;

        //CidffPlugin::debug('$play->category_slugs',$play->category_slugs);

        if( empty($play->category_slugs) )
        {
            // Sélection toutes les catégories.
            $terms = get_terms( array(
                'taxonomy' => CidffPlugin::TAX_CARD,
                'hide_empty' => true,
            ) );
            CidffPlugin::debug('$terms',$terms);
        }
        else
        {
            foreach( $play->category_slugs as $slug )
            {
                $term = get_term_by( 'slug', $slug, CidffPlugin::TAX_CARD );
                if( $term )
                    $terms[] = $term ;
            }

        }
        //CidffPlugin::debug($terms);

        $quiz = new Quiz();
        $flat_quiz = [
            'categories' => [],
            'questions' => [],
        ];
        foreach( $terms as $term )
        {
            $imageFile = get_attached_file( $term->term_image );
            //CidffPlugin::debug(__METHOD__,'$image:', $imageFile );

            $category = (new Category())
                ->setSlug( $term->slug )
                ->setLabel( $term->name )
                ->setBackgroundFile( $imageFile )
                ;
            $quiz->addCategory( $category );

            $flat_quiz['categories'][] = [
                $category->getSlug() => $category->getLabel()
            ];

            foreach( $wpdb->get_results(
                self::getSqlPostsByCategory(
                    $term,
                    $play->questions_per_category,
                    $question_first = 0,
                    $random = true )
                )
                as $post )
            {
                $choices = maybe_unserialize( $post->choices );

                $flat_quiz['questions'][]=[
                    'question' => $post->question,
                    'level' => $post->level,
                    'choices' => $choices,
                    'answer' => $post->answer,
                    'category' => $category->getSlug(),
                ];

                // @TODO À quoi ça sert d'ajouter des cartes à cet objet ??
                $category
                    ->addCard((new Question())
                        ->setQuestion( $post->question )
                        ->setLevel( $post->level )
                        ->setResponses( $choices )
                        ->setAnswer( $post->answer )
                    );

            }
        }

        return $flat_quiz ;
    }
    */

}
