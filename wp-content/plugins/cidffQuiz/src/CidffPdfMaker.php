<?php

namespace CidffPlugin\CidffPdfMaker ;

abstract class Message
{
    public abstract function getData(); 
    public function toJson()
    {
        return json_encode(
            $this->getData(),
            ~JSON_PRETTY_PRINT
        );
    }
}

class MsgProgress extends Message
{
    protected $items ;
    public function __construct( Array ...$items )
    {
        $this->items = $items ;
    }
    public function getData()
    {
        return [
            'type'=>'progress',
            'items' => $this->items
        ];
    }
}

class MsgDone extends Message
{
    protected $msg ;
    public function __construct( string $msg )
    {
        $this->msg = $msg ;
    }
    public function getData()
    {
        return [
            'type'=>'done',
            'msg' => $this->msg
        ];
    }
}

namespace CidffPlugin ;

//require_once( CidffPlugin::plugin_dir().'lib/tecnickcom/tcpdf/tcpdf.php');
require_once( CidffPlugin::plugin_dir().'vendor/autoload.php');
require_once( __DIR__.'/CidffQuiz.php');
require_once( __DIR__.'/Quiz/Renderers/RenderTcpdf.php');

use Cidff\Quiz\Quiz ;
use Cidff\Quiz\Category ;
use Cidff\Quiz\Question ;

use Cidff\Quiz\Renderers\RenderTcpdf ;
use Cidff\Quiz\Renderers\RenderListener ;

use CidffPlugin\CidffPdfMaker\MsgProgress ;
use CidffPlugin\CidffPdfMaker\MsgDone;
use CidffPlugin\CidffPdfMaker\Message;

class CidffPdfMaker implements RenderListener
{
    protected $config = [
        'cards' => [
            // Card position and size
            'width' => 50,
            'height' => 70,
            'marginX' => 4,
            'marginY' => 4,
            // Question text
            'textX' => 10.5,
            'textY' => 14,
            'textW' => 27.5, // text width
            'textH' => 20, // text height
            'textA' => 8.4, // text angle
            'textFS' => 6.5, // text font-size
            'textFF' => 'dejavusansb', // text font-family
            'textC' => '#492A63', // text color
            'numFS' => 7 , // num question font-size
            'numX' => 40.8 , // num question
            'numY' => 2.1 , // num question
        ],
    ];

    const PREVIEW_MAX_QUESTIONS = 5 ;

    public function __construct()
    {
    }

    public function getConfig()
    {
        return $this->config ;
    }

    /**
     * In preview mode don't user sendResponse !!
     * @var boolean
     */
    protected $preview = false ;

    /**
     * Générate PDF with progress information, then die.
     */
    public function make( $term_slug, $preview = false, $pdf_config = null, $questions_max = null, $question_first = 0 )
    {
        //CidffPlugin::debug(__METHOD__,'$preview:',$preview);
        //CidffPlugin::debug(__METHOD__,'$pdf_config:',$pdf_config);

        if( ! empty($pdf_config) )
            $this->config = array_replace_recursive( $this->config, $pdf_config );

        $this->preview = $preview ;

        $term = get_term_by( 'slug', $term_slug, CidffPlugin::TAX_CARD );
        //CidffPlugin::debug(__METHOD__,'$term:',$term);

        $this->sendResponse( new MsgProgress( ['cartes','?','?'], ['pages','?','?'] ) );

        // Make Quiz data from Wordpress content.

        $quiz = CidffQuiz::getByCategory(
            $term,
            $questions_max, $question_first,
            $random = false
            );

        $category = $quiz->getCategory( $term->slug ) ;

        // Generation ...

        $r = new RenderTcpdf( $this->config );
        if( ! $this->preview )
            $r->setListener( $this );
        $r->render( $category );

        // Output !

        $filename = 'FetC_cartes_' . $term_slug.'.pdf' ;

        //$temp_file = tempnam( sys_get_temp_dir(), 'CidffPdfMaker_');
        //$r->outputFile($temp_file);

        if( $preview )
        {
            $r->outputDirect($filename);
        }
        else 
        {
            // Not a preview, update Wordpress data

            $term_pdf_attach_id = get_term_meta($term->term_id, CidffPlugin::TERM_META_PDF_ATTACH, true );
            //CidffPlugin::debug(__METHOD__,'term_pdf_attach_id:', $term_pdf_attach_id, 'get_attached_file:', get_attached_file($term_pdf_attach_id));

            /*
             * GUID in never|not updated ...
             * on ne peut pas mettre à jour un post attachement, il vaut mieux le supprimer.
             * À cause du guid qui ne doit pas changer selon les règles du coeur de Wordpress.
             */

            if( $term_pdf_attach_id )
            {
                // wp_delete_attachment() will call wp_delete_attachment_files()
                wp_delete_attachment( $term_pdf_attach_id, true );
            }

            $file_data = wp_upload_bits( $filename,null, $r->outputString(), $time = null );
            //CidffPlugin::debug(__METHOD__,'$file_data:',$file_data);

            $attach_post_id = wp_insert_attachment( [
                //'ID' => $term_pdf_attach_id,
                'guid'           => $file_data['url'],
                'post_mime_type' => $file_data['type'],
                'post_title'     => $filename,
                'post_content'   => '',
                'post_status'    => 'inherit'
                ],
                $file_data['file']
            );
            if( $attach_post_id == 0 )
                throw new \RuntimeException('wp_insert_attachment() failed');

            //CidffPlugin::debug(__METHOD__,'attach_post_id:', $attach_post_id, 'get_attached_file:', get_attached_file($attach_post_id));

            update_term_meta( $term->term_id, CidffPlugin::TERM_META_PDF_ATTACH, $attach_post_id );

            $this->sendResponse( new MsgDone('Fichier PDF généré "<a href="'.$file_data['url'].'">'.$filename.'</a>"') );
        }

        die();
    }

    /**
     * https://stackoverflow.com/questions/7049303/show-progress-for-long-running-php-script
     * @param integer $i
     */
    protected function sendResponse( Message $msg )
    {
        static $buffer_filler = null ;

        if( $this->preview )
            return ;

        // First call
        if( $buffer_filler == null )
        {
            $buffer_filler = str_repeat(' ', 4*1024) . "\n" ;

            header('Content-Type: application/octet-stream');
            header('Cache-Control: no-cache'); // recommended to prevent caching of event data.

            // Turn off output buffering
            ini_set('output_buffering', 'off');
            // Turn off PHP output compression
            ini_set('zlib.output_compression', false);
            // Implicitly flush the buffer(s)
            ini_set('implicit_flush', true);
            ob_implicit_flush(true);
        }

        //error_log( 'progress '. $i .'...' );
        //echo 'cartes: ', $cards[0],'/'.$cards[1],', pages: ',$pages[0],'/'.$pages[1],'',"\n";
        echo $msg->toJson(), "\n";
        // MUST fill output to avoid http server and/or browser to wait for enough data.
        echo $buffer_filler ;
        ob_flush();
    }

    public function onProgress($cardsCount, $cardsTotal, $pagesCount)
    {
        $this->sendResponse( new MsgProgress(
            ['cartes', $cardsCount, $cardsTotal],
            ['pages',$pagesCount,'?']
            ) );
    }

}
