<?php

namespace Cidff\Quiz\Renderers ;

require_once(__DIR__.'/Render.php');

use TCPDF ;
use TCPDF_STATIC ;
use Cidff\Quiz\Category;
use Cidff\Quiz\Question;
use CidffPlugin\CidffPlugin;
use RuntimeException;

// Extend the TCPDF class to create custom Header and Footer
class RenderTcpdf_CustomTcpdf extends TCPDF {

    public $h_title ;
    public $h_text1 ;
    public $h_text2 ;

    /**
     * wp-content/plugins/cidffQuiz/src/resources/logo-cidff45.jpg
     */
    public $resources_path ;
    public $logos = [
        /*[
            'src' => 'logo-cidff45.jpg',
            'x'=>0,'y'=>220,'w'=>30,
        ],*/
        [
            'src'=>'pdf-footer-logos.png',
            'x'=>48,'y'=>280,'w'=>110,
        ]
    ];

    public function setResourcesPath( $path )
    {
        if( ! file_exists($path))
            throw new RuntimeException('Resources path not found "'.$path.'"');
        $this->resources_path = $path ;
    }

    //Page header
    public function Header()
    {
        // Set font
        $this->SetFont('helvetica', 'B', 20);
        // Title
        $this->Cell(0, 15, $this->title, 0, false, 'C', 0, '', 0, false, 'M', 'M');

        $this->SetFont('helvetica', '', 10);
        $this->Ln(8);
        $this->Cell(0, 6, 'Auteurs: Le CIDFF du Loiret avec les CIDFF de la région Centre Val de Loire.',
            $border=0, $ln=true, $align='C', $fill=0, $link='', $stretch=0, $ignore_min_height=false, $calign='M', $valign='M');
        $this->Ln(2);
        $this->Cell(0, 6, 'Œuvre mise à disposition selon la Licence CC-BY-ND www.creativecommons.org/licenses/by-nd/3.0/fr/',
            $border=0, $ln=true, $align='C', $fill=0, $link='', $stretch=0, $ignore_min_height=false, $calign='M', $valign='M');
    }

    // Page footer
    public function Footer()
    {

        //         $bgSvg = '@'.TCPDF_STATIC::fileGetContents( $category->getBackgroundFile() );

        if( $this->resources_path!=null && ! empty($this->logos) )
        {
            foreach( $this->logos as &$logo )
            {
                if( ! isset($logo['data']))
                {
                    $logo['data'] = '@'.TCPDF_STATIC::fileGetContents( $this->resources_path.'/'.$logo['src'] );
                }
                $this->setImageScale(PDF_IMAGE_SCALE_RATIO);

                // Image($file, $x='', $y='', $w=0, $h=0, $type='', $link='', $align='', $resize=false, $dpi=300, $palign='', $ismask=false, $imgmask=false, $border=0, $fitbox=false, $hidden=false, $fitonpage=false)

                $this->Image($logo['data'], $logo['x'], $logo['y'], $logo['w'] , $h='' , $type='', $link='', $align='',
                    $resize=false, $dpi=150, $palign='', $ismask=false, $imgmask=false, $border=0,
                    $fitbox=false, $hidden=false, $fitonpage=false);
            }
        }

        $this->SetX(12);
        $this->SetY(-12);
        $this->SetFont('helvetica', 'I', 8);
        $d = date('d/m/Y H:i');
        $this->Cell(0, 8, 'Version '.$d, 0, false, 'L', $fill=0, '', 0, false, 'M', 'B');

        $this->SetX(0);
        $this->SetY(-22);
        $this->SetFont('helvetica', 'N', 10);
        $this->Cell(0, 8, 'www.femmesetcitoyennes.fr CC-BY-ND '.date('Y'), $border=0, $ln=false, $align='C', $fill=0, '', 0, false, 'M', 'B');

        $this->SetX(0);
        $this->SetY(-12);
        $this->SetFont('helvetica', 'I', 10);
        $this->Cell(0, 8, 'Page '.$this->getAliasNumPage().'/'.$this->getAliasNbPages(), 0, false, 'R', $fill=0, '', 0, false, 'M', 'B');
        
    }
}

class RenderTcpdf implements Render
{    
    protected $default_config = [
        'resources_path' => __DIR__.'/../../resources',
        'cards' => [
            // Card position and size
            'width' => 50,
            'height' => 70,
            'marginX' => 4,
            'marginY' => 2,
            // Question text
            'textX' => 10.5,
            'textY' => 14,
            'textW' => 27.5, // text width
            'textH' => 20, // text height
            'textA' => 8.4, // text angle
            'textFS' => 6.5, // text font-size
            'textFF' => 'dejavusansb', // text font-family
            'textC' => '#492A63', // text color
            'numFS' => 7 , // num question font-size
            'numX' => 40.8 , // num question
            'numY' => 2.1 , // num question
        ],
    ];

    protected $pdf ;
    protected $cardX, $cardY ;

    /**
     * @var RenderListener 
     */
    protected $listener ;

    public function __construct( Array $config = [] )
    {
        // Permits override of partial configuration directives.
        if( ! empty($config) )
            $this->config = array_replace_recursive( $this->default_config, $config );

        //error_log( 'config: '.print_r($this->config ,true) );
    }

    public static function getDefaultConfig()
    {
        return self::$default_config ;
    }

    public function setListener( RenderListener $listener )
    {
        $this->listener = $listener ;
    }

    public function render( Category $category )
    {
        //echo 'Cards count: ',count($category->getCards()),"\n";
        //error_log( 'config: '.print_r($this->config ,true) );

        $cards = $category->getCards() ;

        $totalItems = 2 * count($cards);
        $itemsCount = 0 ;
        $pagesCount = 0 ;

        $this->pdf = new RenderTcpdf_CustomTcpdf(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
        $this->pdf->setJPEGQuality(90);

        $this->pdf->setResourcesPath( $this->config['resources_path'] );

        $this->pdf->h_title = 'Quiz Femmes et Citoyennes - '. $category->label ;

        $this->pdf->SetCreator(PDF_CREATOR);
        $this->pdf->SetAuthor('CIDFF Loiret (45) et ses partenaires');
        $this->pdf->SetTitle('Quiz Femmes et Citoyennes - '. $category->label );
        $this->pdf->SetSubject('Un jeu sur l’accès au droit et l’exercice de la citoyenneté');
        $this->pdf->SetKeywords('citoyenneté, droit, santé, couple, parents, travail');

        $this->pdf->setMargins(8,30,8,true);
        $this->pdf->setHeaderMargin(10);

        /*
        $this->pdf->setHeaderData($ln='', $lw=0,
            $ht='Quiz Femmes et Citoyennes - '. $category->label,
            $hs='Ce(tte) œuvre est mise à disposition selon la Licence Creative Commons Attribution - Pas de Modification 3.0.',
            $tc=array(0,0,0), $lc=array(255,255,255));
        $this->pdf->setFooterData(array(0,64,0), array(0,64,128));
        */

        $this->pdf->AddPage();
        $pagesCount ++ ;

        $this->fireOnProgress( $itemsCount, $totalItems, $pagesCount );

        //$pW = $this->pdf->getPageWidth();
        //$pH = $this->pdf->getPageHeight();

        //echo 'Pagesize: ', $pW, ' ', $pH,"\n";
        //echo 'Margins: ',print_r($this->pdf->getMargins(),true),"\n";
        //echo 'PageDimensions: ',print_r($pdf->getPageDimensions(),true),"\n";
        //echo 'tagvspaces: ' , print_r($this->pdf->tagvspaces, true),"\n";
        //echo 'getFontsList: ' , print_r($this->pdf->fontlist, true),"\n";

        $bgSvg = '@'.TCPDF_STATIC::fileGetContents( $category->getBackgroundFile() );

        $pageMarginLeft = $this->pdf->getMargins()['left'];
        $pageMarginTop = $this->pdf->getMargins()['top'];
        $pageWidth = $this->pdf->getPageWidth() - ($pageMarginLeft + $this->pdf->getMargins()['right']);
        $pageHeight = $this->pdf->getPageHeight() - ($pageMarginTop + $this->pdf->getMargins()['bottom']);

        $this->cardX = $pageMarginLeft;
        $this->cardY = $pageMarginTop;

        $cc = &$this->config['cards'] ;

        // Les pages des cartes
        $question_num = 0 ;
        $this->cardY += $cc['marginY'] ;
        foreach( $cards as $card )
        {
            $question_num ++ ;

            $this->drawCard($card, $bgSvg, $question_num);

            $itemsCount ++ ;

            $this->cardX += ($cc['width'] + $cc['marginX']);
            if( $this->cardX + $cc['width'] > $pageWidth )
            {
                $this->cardX = $pageMarginLeft ;

                $this->cardY += ($cc['height'] + $cc['marginY']);
                if( $this->cardY + $cc['height'] > $pageHeight )
                {
                    $this->pdf->AddPage();
                    $pagesCount ++ ;
                    $this->cardY = $pageMarginTop ;
                }
            }

            $this->fireOnProgress( $itemsCount, $totalItems, $pagesCount);
        }

        //
        // Les pages des réponses
        //

        // reset pointer to the last page
        //$this->pdf->lastPage();
        //$this->pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

        $this->pdf->AddPage();
        $pagesCount ++ ;

        $this->pdf->writeHTML( '<h2 align="center" style="font-size: 18pt">Les questions et leurs réponses</h2>' );

        $question_num = 0 ;
        foreach( $cards as $card )
        {
            $question_num ++ ;

            $this->drawAnswer($card, $question_num);

            $itemsCount ++ ;

            $this->fireOnProgress( $itemsCount, $totalItems, $pagesCount);
        }
    }

    public function outputFile( $filename )
    {
        $this->pdf->Output( $filename, 'F');
    }

    public function outputString()
    {
        return $this->pdf->Output( null, 'S');
    }

    public function outputDirect( $filename )
    {
        $this->pdf->Output( $filename, 'I');
    }

    protected function fireOnProgress( $cardsCount, $cardsTotal, $pagesCount )
    {
        if( $this->listener )
            $this->listener->onProgress($cardsCount, $cardsTotal, $pagesCount);
    }

    protected function drawCard( Question $card, &$bgSvg, $question_num )
    {
        $this->drawCardBack($card, $bgSvg, $question_num);
        $this->drawCardQuestion($card, $question_num);
    }

    protected function drawCardBack( Question $card, &$bgSvg, $question_num )
    {
        static $sw = null ;

        $cc = &$this->config['cards'] ;
        $this->pdf->ImageSVG($bgSvg, $this->cardX, $this->cardY, $cc['width'] , $cc['height'] , $link='', $align='', $palign='', $border=0, $fitonpage=false);

        $this->pdf->SetFont('helvetica', '', $cc['numFS']);

        if( $sw == null )
            $sw = $this->pdf->GetStringWidth( '99' ) / 2 / 2 ;

        if( $question_num < 10 )
            $ox = $sw ;
        else
            $ox = 0 ;
        // bug ?? le "1" est plus haut que les autres ...
        if( $question_num == 1 )
            $oy = 1 ;
        else
            $oy = 0 ;

        $html = $question_num ;

        $this->pdf->writeHTMLCell( '', '',
            $this->cardX + $cc['numX'] + $ox,
            $this->cardY + $cc['numY'] + $oy,
            $html, $border=0, 1, 0, true, 'L');
    }

    protected function formatCardQuestionText( Question $card )
    {
        $cc = &$this->config['cards'] ;

        $this->pdf->setListIndentWidth( 2 );
        $this->pdf->setHtmlVSpace(
            [
                'li' => [['h' => 1, 'n' => 0.8 ], ['h' => 1, 'n' => 1 ]],
                'ul' => [['h' => 0.1, 'n' => 0 ], ['h' => 1, 'n' => 1 ]],
                'ol' => [['h' => 1, 'n' => 0.1 ], ['h' => 1, 'n' => 0.1 ]],
                'p' =>  [['h' => 1, 'n' => 0 ],   ['h' => 1.5, 'n' => 1 ]],
            ]
        );

        $this->pdf->setCellPadding( 1 );

        $q = str_replace( "\n", '<br/>', $card->getQuestion() );
        $html = '<p align="center" style="color: '.$cc['textC'].'">'.$q.'</p>';

        $responses = $card->getResponses();
        if( count($responses) == 1 )
        {
            // Un seul choix: pas de liste
            $html.= '<p align="center" style="color: '.$cc['textC'].'">'.$responses[0]['text'].'</p>';
        }
        else
        {
            // Plusieurs choix: une liste
            $html.='<ol type="a" align="center" style="color: '.$cc['textC'].'">';
            foreach( $responses as $resp )
            {
                $html .= '<li>'.$resp['text'].'</li>';
            }
            $html.='</ol>';    
        }

        $html = str_replace([
                ' ?',
                //' ;'
            ], [
                '&nbsp;?',
                //';'
            ], $html);

        return $html ;
    }

    protected function drawCardQuestion( Question $card, $question_num )
    {
        $cc = &$this->config['cards'] ;

        $html = $this->formatCardQuestionText( $card );

        $this->pdf->SetFont($cc['textFF'], '', $cc['textFS']);

        $tx = $this->cardX + $cc['textX'];
        $ty = $this->cardY + $cc['textY'];
        $this->pdf->StartTransform();
        $this->pdf->Rotate( $cc['textA'], $tx, $ty);
        $this->pdf->writeHTMLCell( $cc['textW'], $cc['textH'], $tx, $ty, $html, $border=0, 1, 0, true, 'J');
        $this->pdf->StopTransform();

    }

    protected function drawAnswer( Question $card, $question_num )
    {
        $this->pdf->SetFont( 'helvetica', '', 10);
        $this->pdf->setHtmlVSpace([
            'h3'=> [ ['h' => 0.1, 'n' => 0 ], ['h' => 1, 'n' => 1 ]],
            'p' => [['h' => 1, 'n' => 1 ], ['h' => 1, 'n' => 1 ]],
            'blockquote' => [['h' => 1, 'n' => 1 ], ['h' => 0.1, 'n' => 1 ]],
            'div' => [['h' => 0.1, 'n' => 1 ], ['h' => 0.1, 'n' => 1 ]],
        ]);

        //$this->pdf->Ln(1);

        $html = '' ;
        $html.= '<div style="page-break-inside: avoid; line-height: 1">' ;
        // 
        $html.= '<h3 align="left">';
        //CidffPlugin::debug('q:', $card->question);
        $html.= $question_num.') '. $card->question ;
        $html.='</h3>';

        if( count($card->responses) == 1 )
        {
            $html.= '<blockquote align="left">';
            $html.= $card->answer ;
            $html.='</blockquote>';
        }
        else
        {
            $html.= '<blockquote>';
            $br = [];
            foreach( $card->responses as $rep )
            {
                if( isset($rep['good']) && $rep['good'] )
                    $br[] = $rep['text'];
            }
            if( count($br)>1)
                $html.='<p>Bonnes réponses : ';
            else
                $html.='<p>Bonne réponse : ';
            $html.= implode(' - ',$br);
            $html.='</p>';
            $html.= $card->answer ;
            $html.='</blockquote>';
        }
        $html.= '</div>' ;

        $this->pdf->writeHTML( $html );
    }

}
