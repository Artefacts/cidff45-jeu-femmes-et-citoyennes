<?php

namespace Cidff\Quiz\Renderers ;

require_once(__DIR__.'/RenderListener.php');

use Cidff\Quiz\Category;

interface Render
{
    public function render( Category $category );
    public function setListener( RenderListener $listener );
}
