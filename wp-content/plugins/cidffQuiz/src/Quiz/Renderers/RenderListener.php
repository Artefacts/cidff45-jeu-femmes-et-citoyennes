<?php

namespace Cidff\Quiz\Renderers ;

interface RenderListener
{
    public function onProgress( $cardsCount, $cardsTotal, $pagesCount );
}
