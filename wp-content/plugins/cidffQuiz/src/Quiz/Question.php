<?php

namespace Cidff\Quiz ;

class Question {

    public $question ;
    public $responses ;
    public $answer ;
    public $level ;

    public function __construct()
    {
    }

    public function setQuestion( $question )
    {
        $this->question = $question ;
        return $this ;
    }
    public function getQuestion()
    {
        return $this->question ;
    }

    public function setResponses( array $responses )
    {
        $this->responses = $responses ;
        return $this ;
    }
    public function getResponses()
    {
        return $this->responses ;
    }

    public function setAnswer( $answer )
    {
        $this->answer = $answer ;
        return $this ;
    }

    public function getAnswer()
    {
        return $this->answer ;
    }

    public function setLevel( $level )
    {
        $this->level = $level ;
        return $this ;
    }

    public function getLevel()
    {
        return $this->level ;
    }
}
