<?php

namespace Cidff\Quiz ;

require_once( __DIR__.'/Question.php');
require_once( __DIR__.'/Category.php');

use Cidff\Quiz\Renderers\RenderTcpdf;

class Quiz {
    
    public $categories ;

    public function __construct()
    {
        $this->categories = [];
    }

    /**
     * @return Category[]
     */
    public function getCategories()
    {
        return $this->categories ;
    }

    public function addCategory( Category $category )
    {
        $this->categories[ $category->getSlug() ] = $category ;
        return $this ;
    }

    public function getCategory( $slug )
    {
        if( isset($this->categories[$slug]) )
            return $this->categories[$slug] ;
        return null ;
    }

}
