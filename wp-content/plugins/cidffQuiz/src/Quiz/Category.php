<?php

namespace Cidff\Quiz ;

class Category {

    public $slug ;
    public $label ;
    /**
     * Ordered array of Question.
     * @var \Cidff\Quiz\Question[]
     */
    public $questions ;
    /**
     * @var string
     */
    public $backgroundFile ;
    const BackgroundTypes = ['SVG'];
    /**
     * @var string
     */
    protected $backgroundType ;

    public function __construct()
    {
        $this->questions = [];
    }
    
    public function setLabel( string $label )
    {
        $this->label = $label ;
        return $this ;
    }
    public function getLabel()
    {
        return $this->label ;
    }

    public function setSlug( string $slug )
    {
        $this->slug = $slug ;
        return $this ;
    }
    public function getSlug()
    {
        return $this->slug ;
    }

    public function setBackgroundFile( string $filename )
    {
        $this->backgroundFile = $filename ;
        return $this ;
    }
    public function getBackgroundFile()
    {
       return $this->backgroundFile; 
    }

    public function getCards()
    {
        return $this->questions ;
    }
    /**
     * 
     * @return number
     */
    public function getCardsCount()
    {
        return count( $this->questions );
    }

    public function addCard( Question $question )
    {
        $this->questions[] = $question ;
        return $this ;
    }

    public function removeCard( Question $card )
    {
        throw new \RuntimeException('Not yet implemented');
    }

    public function cardUp( $actualPosition )
    {
        throw new \RuntimeException('Not yet implemented');
    }

    public function cardDown( $actualPosition )
    {
        throw new \RuntimeException('Not yet implemented');
    }

    public function shuffle()
    {
        throw new \RuntimeException('Not yet implemented');
    }
}
