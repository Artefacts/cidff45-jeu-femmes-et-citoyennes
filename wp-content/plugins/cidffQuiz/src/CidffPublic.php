<?php
namespace CidffPlugin ;

class CidffPublic
{
    public function __construct()
    {
        CidffPlugin::debug(__METHOD__ );
    }

    public function wp_init()
    {
        CidffPlugin::debug(__METHOD__ );

        if( is_admin() )
        {

        }
        else
        {
            add_shortcode( CidffPlugin::SHORTCODE_CARDSDOWNLOAD, [$this,'wp_shortcode_cardsdownload'] );
            add_action('wp_enqueue_scripts', [$this,'wp_enqueue_scripts'], 100 );

            add_filter('wp_sitemaps_users_pre_url_list', [$this, 'wp_sitemaps_users_pre_url_list']);
            add_filter('wp_sitemaps_post_types', [$this, 'wp_sitemaps_post_types']);
            add_filter('wp_sitemaps_taxonomies', [$this, 'wp_sitemaps_taxonomies']);
        }

    }

    /**
     * Pas trouvé mieux, ça génère une liste vide des utilisateurs dans le sitemap
     */
    public function wp_sitemaps_users_pre_url_list()
    {
        return true ;
    }

    /**
     * Remove some post type from sitemap
     */
    public function wp_sitemaps_post_types( &$post_types )
    {
        CidffPlugin::debug('wp_sitemaps_post_types()', 'post_types:', array_keys($post_types));

        foreach( $post_types as $type => $post_type)
        {
            if( in_array( $type, CidffPlugin::$sitemap_exclude['post']) )
            {
                unset( $post_types[$type]);
            }
        }
        return $post_types;
    }

    /**
     * Remove some taxonomies from sitemap
     */
    public function wp_sitemaps_taxonomies( &$taxonomies )
    {
        CidffPlugin::debug('wp_sitemaps_taxonomies()', 'taxonomies:', array_keys($taxonomies));

        foreach( $taxonomies as $type => $taxonomy)
        {
            if( in_array( $type, CidffPlugin::$sitemap_exclude['taxo']) )
            {
                unset( $taxonomies[$type]);
            }
        }
        return $taxonomies;
    }

    public function wp_shortcode_cardsdownload()
    {
        ob_start();
        ?>
        <ul class="dc-list">
        <?php
        foreach( get_terms( [
            'taxonomy' => CidffPlugin::TAX_CARD,
            'hide_empty' => false,
        ] ) as $term )
        {
            $pdf_id = get_term_meta($term->term_id, CidffPlugin::TERM_META_PDF_ATTACH, true );
            if( ! $pdf_id )
                continue ;
			$pdf_post = get_post( $pdf_id );
            ?>
            <li class="dc-list-item">
                <a class="dc-list-link" href="<?php echo $pdf_post->guid; ?>" target="_blank">
					<p class="dc-list-title"><?php echo $term->name; ?></p>
					<p class="dc-list-infos"><span class="dc-list-pdf"><?php echo $pdf_post->post_title; ?></span> - <span class="dc-list-modified"><?php echo $pdf_post->post_modified; ?></span></p>
                </a>
                
            </li>
            <?php
        }
        ?>
        </ul>
        <?php
        $output_string = ob_get_contents();
        ob_end_clean();
        return $output_string;
    }

    public function wp_enqueue_scripts()
    {
        CidffPlugin::debug(__METHOD__ );

        //
        // FIXME : remove theme styles
        // It does not work :{ to late ???
        //
        wp_dequeue_style( 'twentynineteen-style' );
        wp_deregister_style( 'twentynineteen-style' );
        wp_dequeue_style( 'twentynineteen-print-style' );
        wp_deregister_style( 'twentynineteen-print-style' );
        wp_dequeue_style( 'twentynineteen-editor-customizer-styles' );
        wp_deregister_style( 'twentynineteen-editor-customizer-styles' );
        wp_deregister_style( 'original-register-stylesheet-handle' );

        wp_register_script( CidffPlugin::PLUGIN_NAME.'-quiz', CidffPlugin::asset_url().'quiz.js',
            ['jquery'],
            CidffPlugin::VERSION, true );
        wp_enqueue_script( CidffPlugin::PLUGIN_NAME.'-quiz' );
        
        wp_register_style(CidffPlugin::PLUGIN_NAME.'-quiz', CidffPlugin::asset_url().'quiz.css',
            ['wp-jquery-ui-dialog'],
            CidffPlugin::VERSION);
        wp_enqueue_style( CidffPlugin::PLUGIN_NAME.'-quiz' );


        $quiz = CidffPlugin::getInstance()->getQuiz() ;
        //CidffPlugin::debug(__METHOD__,'$quiz:', $quiz );

        // in JavaScript, object properties are accessed as ajax_object.ajax_url, ajax_object.we_value
        wp_localize_script( CidffPlugin::PLUGIN_NAME.'-quiz', 'cidff_quiz',
        [
            'quiz' => $quiz,
        ] );

    }

}
