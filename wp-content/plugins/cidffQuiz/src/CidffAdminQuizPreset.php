<?php
namespace CidffPlugin ;

/**
 * Administration WP des presets de Quiz
 */
class CidffAdminQuizPreset
{
    const ACTION_EXPORTALL = 'cidff_exportall' ;
    const ACTION_EXPORTPRESET = 'cidff_exportpreset' ;

    public function wp_init()
    {
        if( ! is_blog_admin() )
            return ;

        //CidffPlugin::debug(__METHOD__);

        // Process actions: export & import
        add_action( 'admin_init', [$this,'wp_admin_init'] );
        // Customize views: list & edit
        add_action( 'current_screen', [$this, 'wp_current_screen'] );

        // Add edition blocs.
        add_action( 'edit_form_after_editor', [$this,'wp_edit_form_after_editor'] );
        add_action( 'admin_enqueue_scripts', array( $this, 'wp_admin_enqueue_scripts' ) );

        /**
         * Add button 'export all cards'
         */
        add_filter( 'views_edit-'.CidffPlugin::CPT_QUIZPRESET, function ( $views )
        {
            ob_start();
            include( CidffPlugin::views_dir(true).'_button_presets-export-all.php' );
            //$views['my-button'] = ob_get_contents();
            $s = ob_get_contents();
            ob_end_clean();
            echo '<div>'.$s, '</div>' ;
            return $views;
        });

    }

    /**
     * Customize views: list & edit
     */
    public function wp_current_screen()
    {
        /**
         * @var \WP_Screen $screen
         */
        $screen = get_current_screen();
        //CidffPlugin::debug(__METHOD__ , '$currentScreen', $screen );

        if( $screen->post_type !== CidffPlugin::CPT_QUIZPRESET )
            return ;

        // Quiz preset edit
        if( $screen->id == CidffPlugin::CPT_QUIZPRESET )
        {
            $this->customize_edit_view();
        }
        // Quiz presets listing
        else if( $screen->id == 'edit-'.CidffPlugin::CPT_QUIZPRESET )
        {
            $this->customize_list_view();
            // Customize admin CPT's listing
        }

    }

    /**
     * It runs on admin-ajax.php and admin-post.php as well.
     * Another typical usage is to register a new setting for use by a plugin.
     * 
     * https://codex.wordpress.org/Plugin_API/Action_Reference/admin_init
     */
    public function wp_admin_init()
    {
        //CidffPlugin::debug(__METHOD__ );

        if( isset($_REQUEST['action']) && $_REQUEST['action'] == self::ACTION_EXPORTALL )
        {
            $this->export_all();
            return ;
        }

        if( isset($_REQUEST['action']) && $_REQUEST['action'] == 'editpost' && isset($_FILES['cidff_preset_file']))
        {
            $this->import_preset( $_REQUEST['post_ID'] );
            return ;
        }

        if( isset($_REQUEST['action']) && $_REQUEST['action'] == self::ACTION_EXPORTPRESET )
        {
            $this->export_preset( $_REQUEST['post_ID'] );
            return ;
        }

    }

    public function wp_admin_enqueue_scripts()
    {
        $screen = get_current_screen();
        if( $screen->id != CidffPlugin::CPT_QUIZPRESET )
            return ;
        //CidffPlugin::debug(__METHOD__, '$screen->id:', $screen->id);

        wp_register_script( CidffPlugin::PLUGIN_NAME.'-adminQuizPresets', CidffPlugin::asset_url().'CidffAdminQuizPresets.js',
            ['jquery'],
            CidffPlugin::VERSION, true );
        wp_enqueue_script( CidffPlugin::PLUGIN_NAME.'-adminQuizPresets' );
        // in JavaScript, object properties are accessed as ajax_object.ajax_url, ajax_object.we_value
        wp_localize_script( CidffPlugin::PLUGIN_NAME.'-adminQuizPresets', 'cidff_export',
            [
                //'ajax_url' => admin_url( 'admin-ajax.php' ),
                'urls' => [
                    'export_all' => add_query_arg('action', self::ACTION_EXPORTALL),
                ],
            ] );

    }

    /**
     * Adjust columns size for CPT_QUIZPRESET
     */
    public function wp_admin_head_cpt_quizpreset()
    {
        global $post_type;
        if( $post_type != CidffPlugin::CPT_QUIZPRESET )
            return ;

        ?>
        <style type="text/css">
            /*.column-questions_count { width: 10%; }*/
            /* Custom, iPhone Retina */ 
            @media only screen and (min-width : 320px) {
            }
            /* Extra Small Devices, Phones */ 
            @media only screen and (min-width : 480px) {
            }
            /* Small Devices, Tablets */
            @media only screen and (min-width : 768px) {
                .column-title { width: 40%; }
            }
            /* Medium Devices, Desktops */
            @media only screen and (min-width : 992px) {
            }
            /* Large Devices, Wide Screens */
            @media only screen and (min-width : 1200px) {
                .column-title { width: 50%; }
            }
        </style>
        <?php
    }

    /**
     * Construct the form for question and responses.
     * 
     * https://make.wordpress.org/core/2012/12/01/more-hooks-on-the-edit-screen/
     * 
     * @param \WP_Post|\stdClass $post
     */
    public function wp_edit_form_after_editor( $post )
    {
        global $pagenow ;
        if( ! isset($post->post_type))
            return ;

        if( $post->post_type != CidffPlugin::CPT_QUIZPRESET )
            return ;
        //CidffPlugin::debug(__METHOD__, 'pagenow:', $pagenow, 'post:', $post);

        $questions = [];

        $post_ids = null ;
        if( $post->ID > 0 )
            $post_ids = get_post_meta( $post->ID, CidffPlugin::META_PRESET, true );
        //$post_ids = [103,104,105];
        //CidffPlugin::debug(__METHOD__, '$post_ids:', $post_ids );

        if( ! empty($post_ids))
        {
            $query = new \WP_Query( [
                'post_type' => CidffPlugin::CPT_CARD,
                'post_status' => array( 'publish' ),
                'post__in'=> $post_ids,
                'fields' => 'ids',
                'nopaging' => true,
            ] );

            if( $query->have_posts() )
            {
                //CidffPlugin::debug(__METHOD__, '$query->posts', $query->posts );

                //foreach( $query->posts as $post_id )
                foreach( $post_ids as $post_id )
                {
                    if( ! in_array( $post_id,  $query->posts ) )
                    {
                        continue ;
                    }

                    $q = new Class() {
                        public $id ;
                        public $title ;
                        public $category ;
                        public $type ;
                        public $level ;
                        public $choix ;
                        public $responses ;
                    };

                    $q->id = $post_id ;

                    $q->title = get_post_meta( $post_id, CidffPlugin::META_QUESTION, true );

                    $v = get_post_meta( $post_id, CidffPlugin::META_LEVEL, true );
                    $q->level = empty($v) ? 1*CidffPlugin::QUESTION_LEVEL_MIN : $v ;

                    $terms = wp_get_post_terms($post_id,CidffPlugin::TAX_CARD);
                    $q->category = isset($terms[0]) ? $terms[0]->name : '' ;

                    $c = get_post_meta( $post_id, CidffPlugin::META_CHOICES, true );
                    $q->choix = count($c); 
                    $q->type = count($c) > 1 ? 'Q': 'F' ;
                    $q->responses = 0 ;
                    foreach( $c as $cc )
                        if( isset($cc['good']))
                            $q->responses ++ ;
        
                    $questions[] = $q ;
                }    
            }
        }
        // load the view _questionsExport
        include( CidffPlugin::views_dir().'_quizPreset.php');    


    }

    /**
     * Customize Quiz preset list view
     */
    protected function customize_list_view()
    {
        add_action('admin_head', [$this,'wp_admin_head_cpt_quizpreset']);

        add_filter('post_row_actions',
            // Remove quick edit
            function( $actions )
            {
                //CidffPlugin::debug(__METHOD__, 'actions',$actions);
                unset($actions['inline hide-if-no-js']);
                unset($actions['view']);
                return $actions;
            },10,1);

        add_filter( 'manage_'.CidffPlugin::CPT_QUIZPRESET.'_posts_columns',
            // Columns definition
            function( Array $columns )
            {
                //CidffPlugin::debug(__METHOD__ , '$columns', $columns );
                /* Default columns:
                'cb' => '<input type="checkbox" />',
                'title' => 'Titre',
                'author' => 'Auteur',
                'comments' => '<span class="vers comment-grey-bubble" title="Commentaires"><span class="screen-reader-text">Commentaires</span></span>',
                'date' => 'Date',
                */

                static $cols = null ;
                if( $cols != null )
                    return $cols ;

                $cols = [];
                $cols['cb'] = $columns['cb'];
                $cols['title'] = $columns['title'];
                $cols['author'] = $columns['author'];
                $cols['questions_count'] = 'Nbr questions';
                $cols['updated_at'] = 'Modification';

                return $cols ;
            } );

        add_action( 'manage_'.CidffPlugin::CPT_QUIZPRESET.'_posts_custom_column',
            // Fill custom columns value
            function( $column, $post_id )
            {
                global $post ;
                //CidffPlugin::debug(__METHOD__ , '$column', $column );
                switch ( $column )
                {
                    case 'questions_count':
                        //CidffPlugin::debug(__METHOD__ , '$post', $post );
                        $data = get_post_meta($post_id, CidffPlugin::META_PRESET, true) ;
                        echo (! empty($data) ? count($data) : '<span style="color:red;">0</span>') ;
                        break;
                    case 'updated_at':
                        $dt = get_post_datetime( $post, 'modified' );
                        //echo $dt->format( get_option('date_format') ), ' ', $dt->format( get_option('time_format') );
                        //echo $dt->format( __( 'Y/m/d g:i a' ) );
                        echo $dt->format( __( 'd/m/Y H:i' ) );
                        break;
                }            
            }, 10, 2 );

        add_filter( 'manage_edit-' . CidffPlugin::CPT_QUIZPRESET . '_sortable_columns',
            // Define sortable columns
            function( $columns )
            {
                $columns['updated_at'] = 'updated_at';
                return $columns;
            } );

        add_action( 'pre_get_posts',
            // Operate sortable columns
            function( $query )
            {
                $orderby = $query->get( 'orderby');
                if( $orderby == 'updated_at' )
                {
                    $query->set( 'orderby', 'modified' );
                }
            } );

    }

    /**
     * Customize Quiz preset edit view
     */
    protected function customize_edit_view()
    {
        add_action( 'post_edit_form_tag',
        // Change form's method for file upload
        function()
        {
            echo ' enctype="multipart/form-data" ';
        });
    }

    /**
     * Internal method.
     * Add a Question $row to $sheet.
     * If $post_id is null, add headers to $sheet
     */
    protected function _export_question( \PhpOffice\PhpSpreadsheet\Worksheet\Worksheet $sheet, $row, $post_id = null )
    {
        $cell = ord('A');

        if( $post_id == null )
        {
            $sheet->setCellValue(chr($cell++).$row, 'ID' );
            $sheet->setCellValue(chr($cell++).$row, 'Catégorie' );
            $sheet->setCellValue(chr($cell++).$row, 'Type' );
            $sheet->setCellValue(chr($cell++).$row, 'Choix' );
            $sheet->setCellValue(chr($cell++).$row, 'Réponse(s)' );
            $sheet->setCellValue(chr($cell++).$row, 'Niveau' );
            $sheet->setCellValue(chr($cell++).$row, 'Question' );
            return ;
        }

        $sheet->setCellValue(chr($cell++).$row, $post_id );
        $terms = wp_get_post_terms($post_id,CidffPlugin::TAX_CARD);
        //CidffPlugin::debug(__METHOD__,$terms[0]->name);
        $sheet->setCellValue(chr($cell++).$row, isset($terms[0]) ? $terms[0]->name : '' );
        $c = get_post_meta( $post_id, CidffPlugin::META_CHOICES, true );
        if( count($c) > 1 )
        {
            $sheet->setCellValue(chr($cell++).$row, 'QCM' );
            $sheet->setCellValue(chr($cell++).$row, count($c) );
            $reps = 0 ;
            foreach( $c as $cc )
            {
                if( isset($cc['good']))
                    $reps ++ ;
            }
            $sheet->setCellValue(chr($cell++).$row, $reps );
        }
        else
        {
            $sheet->setCellValue(chr($cell++).$row, 'Forum' );
            $sheet->setCellValue(chr($cell++).$row, 0 );
            $sheet->setCellValue(chr($cell++).$row, 0 );
        }

        $v = get_post_meta( $post_id, CidffPlugin::META_LEVEL, true );
        $sheet->setCellValue(chr($cell++).$row, empty($v) ? 1*CidffPlugin::QUESTION_LEVEL_MIN : $v );

        $s = get_post_meta( $post_id, CidffPlugin::META_QUESTION, true );
        $sheet->setCellValue(chr($cell++).$row, $s );

    }

    protected function export_preset( $post_id )
    {
        //CidffPlugin::debug(__METHOD__);

        $post_ids = get_post_meta( $post_id, CidffPlugin::META_PRESET, true );

        if( empty($post_ids) )
        {
            return ;
        }

        $query = new \WP_Query( [
            'post_type' => CidffPlugin::CPT_CARD,
            'post_status' => array( 'publish' ),
            'post__in'=> $post_ids,
            'fields' => 'ids',
            'nopaging' => true,
        ] );

        if( ! $query->have_posts() )
        {
            return ;
        }

        require_once( CidffPlugin::plugin_dir().'vendor/autoload.php');

        $spreadsheet = new \PhpOffice\PhpSpreadsheet\Spreadsheet();
        $spreadsheet->getProperties()
            ->setCreator('femmesetcitoyennes.fr')
            ->setLastModifiedBy('femmesetcitoyennes.fr')
            ->setTitle('Preset Quiz femmesetcitoyennes.fr')
            ->setSubject('Sélection de questions Quiz femmesetcitoyennes.fr')
            ->setDescription('Export d’une sélection de questions publiées sur femmesetcitoyennes.fr.')
            ->setKeywords('femmes citoyennes quiz cidff droit')
            ->setCategory('Quiz');

        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setTitle('Toutes les questions publiées');

        //CidffPlugin::debug(__METHOD__,'posts count:',count($query->posts));

        $row = 1 ;

        // Add headers
        $this->_export_question( $sheet, $row );

        //foreach( $query->posts as $post_id )
        foreach( $post_ids as $post_id )
        {
            if( ! in_array($post_id, $query->posts) )
                continue ;
            $row ++ ;
            $this->_export_question( $sheet, $row, $post_id );
        }

        $filename = 'Preset quiz '.wp_date('Y-m-d H\hi').'.xlsx';
        // redirect output to client browser
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="'.$filename.'"');
        header('Cache-Control: max-age=0');

        $writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($spreadsheet);
        $writer->save('php://output');

        $spreadsheet->disconnectWorksheets();
        unset($spreadsheet);

        exit();
    }

    protected function export_all()
    {
        //CidffPlugin::debug(__METHOD__);

        require_once( CidffPlugin::plugin_dir().'vendor/autoload.php');
        
        $spreadsheet = new \PhpOffice\PhpSpreadsheet\Spreadsheet();
        $spreadsheet->getProperties()
            ->setCreator('femmesetcitoyennes.fr')
            ->setLastModifiedBy('femmesetcitoyennes.fr')
            ->setTitle('Questions Quiz femmesetcitoyennes.fr')
            ->setSubject('Questions Quiz femmesetcitoyennes.fr')
            ->setDescription('Export de toutes les questions publiées sur femmesetcitoyennes.fr.')
            ->setKeywords('femmes citoyennes quiz cidff droit')
            ->setCategory('Quiz');

        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setTitle('Toutes les questions publiées');

        $query = new \WP_Query( [
            'post_type' => CidffPlugin::CPT_CARD,
            'post_status' => array( 'publish' ),
            'fields' => 'ids',
            'nopaging' => true,
        ] );
        if( ! $query->have_posts() )
        {
            return ;
        }

        $row = 1 ;

        // Add headers
        $this->_export_question( $sheet, $row );

        foreach( $query->posts as $post_id )
        {
            $row ++ ;
            $this->_export_question( $sheet, $row, $post_id );
        }

        $filename = 'Questions quiz '.wp_date('Y-m-d H\hi').'.xlsx';
        // redirect output to client browser
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="'.$filename.'"');
        header('Cache-Control: max-age=0');

        $writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($spreadsheet);
        $writer->save('php://output');

        $spreadsheet->disconnectWorksheets();
        unset($spreadsheet);

        exit();
    }

    protected function import_preset( $preset_post_id )
    {
        //CidffPlugin::debug(__METHOD__, '$_FILES',$_FILES);

        if( ! isset($_FILES['cidff_preset_file']) || empty($_FILES['cidff_preset_file']['tmp_name']) )
            return ;

        //CidffPlugin::debug(__METHOD__, 'cidff_preset_file', $_FILES['cidff_preset_file']);

        require_once( CidffPlugin::plugin_dir().'vendor/autoload.php');

        $questions_postid = [] ;

        //$spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load( $_FILES['cidff_preset_file']['tmp_name'] );
        $reader = \PhpOffice\PhpSpreadsheet\IOFactory::createReaderForFile( $_FILES['cidff_preset_file']['tmp_name'] );
        $reader->setReadDataOnly(true);
        $spreadsheet = $reader->load( $_FILES['cidff_preset_file']['tmp_name'] );
        $col = 1 ;
        $row = 1 ;
        $gotIt = false ;
        do {
            $cellValue = $spreadsheet->getActiveSheet()->getCellByColumnAndRow($col, $row)->getValue();
            $row ++ ;
            //CidffPlugin::debug(__METHOD__, '$cellValue', $cellValue );

            if( $cellValue == 'ID' )
            {
                $gotIt = true ;
            }
            else if( $gotIt && ! empty($cellValue) )
            {
                $post = get_post( $cellValue );
                //CidffPlugin::debug(__METHOD__, '$post', $post );
                if( empty($post) )
                {
                    // No error, ya souvent des lignes vides qui trainent ...
                    //CidffPlugin::addError('Question ID='.$cellValue.' n’existe pas.');
                    //continue ;
                }
                if( $post->post_type != CidffPlugin::CPT_CARD )
                {
                    CidffPlugin::addError('Le document ID='.$cellValue.' n’est pas une question.');
                    continue ;
                }
                $questions_postid[] = $post->ID ;
            }
        }
        while( ! empty($cellValue) );

        if( count($questions_postid) == 0 )
        {
            CidffPlugin::addError('Aucune question trouvée dans le fichier de preset.');
            return ;
        }

        //CidffPlugin::debug(__METHOD__, '$questions_postid', $questions_postid);
        update_post_meta( $preset_post_id, CidffPlugin::META_PRESET, $questions_postid );

    }

}
