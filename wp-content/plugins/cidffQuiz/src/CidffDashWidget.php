<?php
namespace CidffPlugin ;

class CidffDashWidget
{
    const ACTION_PDFPREVIEW = 'cidff_pdfpreview' ;

    /**
     *
     * @var string
     */
    protected $widget_id ;

    public function __construct( $widget_id )
    {
        $this->widget_id = $widget_id ;
    }

    public function getWidgetId()
    {
        return $this->widget_id ;
    }

    public function render()
    {
        // https://developer.wordpress.org/reference/functions/wp_add_dashboard_widget/
        // https://codex.wordpress.org/Example_Dashboard_Widget
        wp_add_dashboard_widget(
            $this->getWidgetId(),
            'Jeu Quiz CIDFF',
            [$this, 'wp_callback_display'],
            // Wordpress will generate a "configure" link
            [$this, 'wp_callback_config'],
            $callback_args = null
        );
    }

    /**
     * Scripts and Styles for Admin widget
     */
    public function wp_callback_display()
    {
        //CidffPlugin::debug(__METHOD__);

        wp_register_style(CidffPlugin::PLUGIN_NAME.'-adminWidget', CidffPlugin::asset_url().'CidffAdminWidget.css',
        ['wp-jquery-ui-dialog'],
        CidffPlugin::VERSION, 'all');
        wp_enqueue_style( CidffPlugin::PLUGIN_NAME.'-adminWidget' );

        wp_register_script( CidffPlugin::PLUGIN_NAME.'-adminWidget', CidffPlugin::asset_url().'CidffAdminWidget.js',
            ['jquery','jquery-ui-core','jquery-ui-dialog','jquery-ui-progressbar'],
            CidffPlugin::VERSION, true );
        wp_enqueue_script( CidffPlugin::PLUGIN_NAME.'-adminWidget' );

        // in JavaScript, object properties are accessed as ajax_object.ajax_url, ajax_object.we_value
        wp_localize_script( CidffPlugin::PLUGIN_NAME.'-adminWidget', 'cidff_quiz',
            [
                //'ajax_url' => admin_url( 'admin-ajax.php' ),
                'widget_id' => $this->getWidgetId(),
                'dialog_id' => CidffPlugin::PLUGIN_NAME .'-dialog',
            ] );
        
        //
        // Create a status list of Taxo items
        //
        
        $categories_status = [];

        foreach( get_terms( [
            'taxonomy' => CidffPlugin::TAX_CARD,
            'hide_empty' => false,
        ] ) as $term )
        {
            $alerts_msg = [];

            //CidffPlugin::debug(__METHOD__,'$term:',$term);
            //$term_id = get_term_meta($term->term_id, CidffPlugin::TERM_META_PDF_ATTACH,true);
            $tm_pdf_attach_id = get_term_meta($term->term_id, CidffPlugin::TERM_META_PDF_ATTACH, true );
            //CidffPlugin::debug(__METHOD__,'$tm_pdf_attach_id:',$tm_pdf_attach_id);
            $pdf_post = null ;
            if( $tm_pdf_attach_id )
            {
                $pdf_post = get_post( $tm_pdf_attach_id );
                //$column_content = '<a target="_blank" href="'.$post->guid.'">'.$post->post_title.'</a><br/>'.$post->post_modified ;
            }

            $pdf_config = get_term_meta( $term->term_id, CidffPlugin::TERM_META_PDF_CONFIG, true );
            if( empty($pdf_config) )
                $alerts_msg[] = 'Manque la configuration pour le Pdf.';

            $categories_status[] = [
                'term' => $term ,
                'pdf' => $pdf_post,
                'alerts' => $alerts_msg,
            ] ;
        }

        // Add errors view
        require_once( CidffPlugin::views_dir().'_errors.php');

        // load the view
        require_once( CidffPlugin::views_dir().'_dashboard.php');

    }

    /**
     * TODO manage widget configuration.
     *
     * Configuration link is generated by Wordpress,
     * just hover over the widget title and click on the "Configure" link.
     */
    public function wp_callback_config()
    {
        CidffPlugin::debug(__METHOD__);

        if( ! empty($_POST) )
            $this->widget_config_save();
        else
            $this->widget_config_display();
    }

    protected function widget_config_display()
    {
        CidffPlugin::debug(__METHOD__);

        //
        // Scripts and Styles for Admin widget configuration.
        //

        wp_register_script( CidffPlugin::PLUGIN_NAME.'-adminWidgetConfig', CidffPlugin::asset_url().'CidffAdminWidgetConfig.js',
            ['jquery','jquery-ui-core','jquery-ui-dialog','jquery-ui-progressbar'],
            CidffPlugin::VERSION, true );
        wp_enqueue_script( CidffPlugin::PLUGIN_NAME.'-adminWidgetConfig' );

        wp_register_style(CidffPlugin::PLUGIN_NAME.'-adminWidgetConfig', CidffPlugin::asset_url().'CidffAdminWidgetConfig.css',
            ['wp-jquery-ui-dialog'],
            CidffPlugin::VERSION);
        wp_enqueue_style( CidffPlugin::PLUGIN_NAME.'-adminWidgetConfig' );

        $categories_config = [];

        foreach( get_terms( [
            'taxonomy' => CidffPlugin::TAX_CARD,
            'hide_empty' => false,
        ] ) as $term )
        {
            $pdf_config = get_term_meta( $term->term_id, CidffPlugin::TERM_META_PDF_CONFIG, true );

            if( ! empty($pdf_config) )
            {
                // Should we validate config ?
                // ...
            }

            if( empty($pdf_config) )
            {
                // Fill pdf_config with a default configuration.
                $pdf_config = CidffAdmin::getDefaultPdfConfig();
            }

            $categories_config[ $term->slug ] = $pdf_config ;
        }

        // in JavaScript, object properties are accessed as ajax_object.ajax_url, ajax_object.we_value
        wp_localize_script( CidffPlugin::PLUGIN_NAME.'-adminWidgetConfig', 'cidff_config',
            [
                //'ajax_url' => admin_url( 'admin-ajax.php' ),
                'widget_id' => $this->getWidgetId(),
                'urls' => [
                    'preview_pdf' => add_query_arg('action', self::ACTION_PDFPREVIEW),
                ],
                'pdf_configs' => $categories_config,
            ] );

        // Add errors view
        require_once( CidffPlugin::views_dir().'_errors.php');

        require_once( CidffPlugin::views_dir().'_dashboard-config.php');

    }

    protected function widget_config_save()
    {
        CidffPlugin::debug(__METHOD__);
        //CidffPlugin::debug(__METHOD__,'$_POST',$_POST);

        // cidff_category cidff-pdf-config
        $category = isset($_POST['cidff_category']) ? $_POST['cidff_category'] : null ;
        $pdf_config = isset($_POST['cidff-pdf-config']) ? $_POST['cidff-pdf-config'] : null ;

        $term = get_term_by('slug', $category, CidffPlugin::TAX_CARD, true );
        if( empty($term) )
        {
            CidffPlugin::addError('Catégorie invalide "'.$category.'".');
            return ;
        }

        $pdf_config = stripcslashes(urldecode($pdf_config));
        $pdf_config = json_decode($pdf_config,JSON_OBJECT_AS_ARRAY);
        if( empty($pdf_config) )
        {
            CidffPlugin::addError('Pdf config invalide.');
            return ;
        }

        //$pdf_config = json_decode( $pdf_config );
        CidffPlugin::debug(__METHOD__,'$pdf_config',$pdf_config);

        update_term_meta( $term->term_id, CidffPlugin::TERM_META_PDF_CONFIG, $pdf_config );

    }

}
