<?php
namespace CidffPlugin ;

class CidffAdmin
{
    protected $errors ;

    /**
     * 
     * @var string
     */
    protected $widget_id ;

    public function __construct()
    {
        //CidffPlugin::debug(__METHOD__ );

        $this->widget_id = strtolower( CidffPlugin::PLUGIN_NAME.'_dashboard_widget' );
    }

    public function wp_init()
    {
        CidffPlugin::debug(__METHOD__ );

        add_action( 'admin_init', [$this,'wp_admin_init'] );

        if( is_blog_admin() )
        {
            // allow svg upload
            add_filter( 'upload_mimes', [$this,'wp_upload_mimes_svg'] /*, 10, 1 */);
            add_filter( 'wp_check_filetype_and_ext', array( $this, 'wp_check_filetype_and_ext_fix_mime_type_svg' ), 75, 4 );

            add_action( 'admin_init', [$this,'wp_admin_init_blog_widget_pdf_preview'] );
            add_action( 'admin_init', [$this,'wp_admin_init_blog_admin'] );
            add_action( 'admin_enqueue_scripts', array( $this, 'wp_admin_enqueue_scripts' ) );
        }

        // FIXME form validation
        // Does not work because media upload failed with "Le contenu, le titre et l’extrait sont vides.".
        // And this filter does not have parameters to detect data type...
        //add_filter('wp_insert_post_empty_content', [$this, 'wp_insert_post_empty_content']);

        add_filter('wp_insert_post_data', [$this, 'wp_insert_post_data'], 10, 3 );
        add_action( 'pre_post_update', [$this,'wp_pre_post_update'], 10, 2);

        // For searching in Question must use those 2 filters
        add_filter( 'pre_get_posts', [$this,'wp_pre_get_posts_search_query']);
        add_filter( 'posts_search', [$this,'wp_posts_search']);
    }

    /**
     * Remove some metaboxes.
     * 
     * It runs on admin-ajax.php and admin-post.php as well.
     * Another typical usage is to register a new setting for use by a plugin.
     * 
     * https://codex.wordpress.org/Plugin_API/Action_Reference/admin_init
     */
    public function wp_admin_init()
    {
        CidffPlugin::debug(__METHOD__);

        // Ajax generate PDF
        add_action( 'wp_ajax_makePdf', function()
        {
            if( !isset($_POST['cat_slug']) || empty($_POST['cat_slug']) )
                throw new \InvalidArgumentException('Input "cat_slug" must be valid.');

            $cat_slug = $_POST['cat_slug'];

            $term = get_term_by('slug', $cat_slug, CidffPlugin::TAX_CARD );
            $pdf_config = get_term_meta( $term->term_id, CidffPlugin::TERM_META_PDF_CONFIG, true);

            require_once( __DIR__.'/CidffPdfMaker.php');
            $pdfMaker = new CidffPdfMaker();
            $pdfMaker->make( $cat_slug, false, $pdf_config );
            return ;
        } );

        add_action( 'admin_bar_menu', [$this,'wp_admin_bar_menu'], 999 );

        if( is_blog_admin() )
        {
            add_action('admin_head', [$this,'wp_admin_head_cpt_question']);

            if( ! current_user_can('administrator') )
            {
                remove_menu_page( 'tools.php' ); 	// outils
                remove_menu_page( 'edit-comments.php' ); // commentaires
                remove_menu_page( 'edit.php' ); // articles
                remove_menu_page( 'upload.php' ); // medias
            }
        }

    }

    /**
     * Adjust columns size for CPT_CARD
     */
    public function wp_admin_head_cpt_question()
    {
        global $post_type;
        if( $post_type != CidffPlugin::CPT_CARD )
            return ;

        ?>
        <style type="text/css">
            .column-cidff_card_level { width: 8%; }
            .column-choices_count { width: 5%; }
            .column-cidff_category { width: 10%; }
            /* Custom, iPhone Retina */ 
            @media only screen and (min-width : 320px) {
            }
            /* Extra Small Devices, Phones */ 
            @media only screen and (min-width : 480px) {
            }
            /* Small Devices, Tablets */
            @media only screen and (min-width : 768px) {
                .column-question { width: 40%; }
            }
            /* Medium Devices, Desktops */
            @media only screen and (min-width : 992px) {
            }
            /* Large Devices, Wide Screens */
            @media only screen and (min-width : 1200px) {
                .column-question { width: 50%; }
            }
        </style>
        <?php
    }

    /**
     * Cleanup admin bar for no adminstrator users.
     */
    public function wp_admin_bar_menu( $wp_admin_bar )
    {
        CidffPlugin::debug(__METHOD__);

        if( current_user_can('administrator') )
            return ;

        $wp_admin_bar->remove_menu('customize');
        $wp_admin_bar->remove_node('new-content');
		$wp_admin_bar->remove_node('view');
		$wp_admin_bar->remove_node('archive');
        $wp_admin_bar->remove_node('comments');
		$wp_admin_bar->remove_node('updates');

    }

    public static function getDefaultPdfConfig()
    {
        require_once( __DIR__.'/CidffPdfMaker.php');
        
        return (new CidffPdfMaker())->getConfig();
    }

    /**
     * Action to preview PDF
     * with url query as "edit=cidffquiz_dashboard_widget&action=preview"
     */
    public function wp_admin_init_blog_widget_pdf_preview()
    {
        // Catch only query with "edit=cidffquiz_dashboard_widget"
        if( ! isset($_GET['edit']) || $_GET['edit']!= $this->widget_id )
            return ;

        require_once('CidffDashWidget.php');

        // Catch only query with "action=preview"
        if( ! isset($_GET['action']) || $_GET['action'] != CidffDashWidget::ACTION_PDFPREVIEW )
            return ;

        /* Test :
        global $pagenow ;
        CidffPlugin::debug(__METHOD__,'$pagenow:',$pagenow);
        header('X-Test: coucou'); // recommended to prevent caching of event data.
        echo 'hello' ;
        die();
        */

        if( ! isset($_GET['cat_slug']) )
        {
            CidffPlugin::addError('Missing taxonomy term');
            return ;
        }
        $cat_slug = $_GET['cat_slug'];
        $term = get_term_by('slug', $cat_slug, CidffPlugin::TAX_CARD);
        if( empty($term) )
        {
            CidffPlugin::addError('Unknow taxonomy term "'.$cat_slug.'" ');
            return ;
        }

        $pdf_config = null ;
        if( isset($_GET['pdf_config']) )
        {
            $pdf_config = stripcslashes(urldecode($_GET['pdf_config']));
            //CidffPlugin::debug(__METHOD__,'$pdf_config:',$pdf_config);
            $pdf_config = json_decode($pdf_config,JSON_OBJECT_AS_ARRAY);
            //CidffPlugin::debug(__METHOD__,'$pdf_config:',$pdf_config);
        }

        $questions_max = isset($_GET['questions_max']) ? intval($_GET['questions_max']) : null ;
        $question_first = isset($_GET['question_first']) ? intval($_GET['question_first']) : 0 ;

        //
        // Render PDF preview.
        //

        require_once( __DIR__.'/CidffPdfMaker.php');
        $pdfMaker = new CidffPdfMaker();
        $pdfMaker->make( $cat_slug, $preview=true, $pdf_config, $questions_max, $question_first );

    }

    public function wp_admin_init_blog_admin()
    {
        // get_current_screen(): function is not yet defined.
        // $post_type is not yet defined.
        global $pagenow ;

        $post_type = isset($_REQUEST['post_type']) ? $_REQUEST['post_type'] : null ;
        $taxonomy = isset($_REQUEST['taxonomy']) ? $_REQUEST['taxonomy'] : null ;
        CidffPlugin::debug(__METHOD__,'$pagenow:',$pagenow, '$post_type:', $post_type, '$taxonomy:',$taxonomy);

        add_action ('wp_dashboard_setup', [$this,'wp_dashboard_setup']);

        if( $pagenow == 'edit.php' && $post_type == CidffPlugin::CPT_CARD )
        {
            /**
             * Add button 'export all cards'
             */
            add_filter( 'views_edit-'.CidffPlugin::CPT_CARD, function ( $views )
            {
                ob_start();
                include( CidffPlugin::views_dir(true).'_button_presets-export-all.php' );
                $s = ob_get_contents();
                ob_end_clean();
                echo '<div>'.$s, '</div>' ;
                return $views;
            });
    
            // Customize admin list of CPT_CARD
            add_filter( 'manage_'.CidffPlugin::CPT_CARD.'_posts_columns', [$this,'wp_manage_posts_columns'] );
            add_action( 'manage_'.CidffPlugin::CPT_CARD.'_posts_custom_column' , [$this,'wp_manage_posts_custom_column'], 10, 2 );
            add_filter( 'manage_edit-' . CidffPlugin::CPT_CARD . '_sortable_columns', [$this,'wp_manage_posts_sortable_column'] );
            add_action( 'pre_get_posts', [$this,'wp_pre_get_posts_sort_column'] );
            add_action( 'restrict_manage_posts',[$this,'wp_restrict_manage_posts']);

            // Remove quick edit & view
            add_filter('post_row_actions', function( $actions )
            {
                //CidffPlugin::debug(__METHOD__, 'actions',$actions);
                //if( isset($_REQUEST['post_type']) && $_REQUEST['post_type'] == CidffPlugin::CPT_CARD )
                //{
                    unset($actions['inline hide-if-no-js']);
                    unset($actions['view']);
                //}
                return $actions;
            },10,1);

        }
        else if( $pagenow == 'post.php' || $pagenow == 'post-new.php' )
        {
            // Add edition blocs for quiz's question, choices and answer.
            add_action( 'edit_form_after_editor', [$this,'wp_edit_form_after_editor'] );

        }
        else if( $pagenow == 'edit-tags.php' && $taxonomy == CidffPlugin::TAX_CARD )
        {
            // custom admin taxonomy term list columns
            add_filter( 'manage_edit-'.CidffPlugin::TAX_CARD.'_columns',  [$this, 'wp_manage_taxo_columns'] );
            add_filter( 'manage_edit-' . CidffPlugin::TAX_CARD . '_sortable_columns', [$this,'wp_manage_taxo_sortable_column'] );
            add_filter( 'manage_'.CidffPlugin::TAX_CARD.'_custom_column', [$this, 'wp_manage_taxo_custom_column'], 10, 3 );

            // Remove quick edit & view
            add_filter(CidffPlugin::TAX_CARD.'_row_actions', function( $actions )
            {
                //CidffPlugin::debug(__METHOD__, 'actions',$actions);
                unset($actions['inline hide-if-no-js']);
                unset($actions['view']);
                return $actions;
            },10,1);


        }


        // Add some meta boxes ...
        //add_action('add_meta_boxes', [$this, 'wp_add_meta_boxes']);

    }

    /**
     * Add and remove dashboard widgets.
     * 
     * https://developer.wordpress.org/reference/functions/wp_dashboard_setup/
     */
    public function wp_dashboard_setup()
    {
        CidffPlugin::debug(__METHOD__);

        // remove Welcome panel
        remove_action('welcome_panel', 'wp_welcome_panel');
        // Remove "Évènements et nouveautés WordPress"
        remove_meta_box( 'dashboard_primary', 'dashboard', 'side' );
        // remove "Brouillon rapide"
        remove_meta_box( 'dashboard_quick_press', 'dashboard', 'side' );
        /*
         * Other blocks:
         remove_meta_box( 'dashboard_incoming_links', 'dashboard', 'normal' );
         remove_meta_box( 'dashboard_plugins', 'dashboard', 'normal' );
         remove_meta_box( 'dashboard_secondary', 'dashboard', 'normal' );
         remove_meta_box( 'dashboard_recent_drafts', 'dashboard', 'side' );
         remove_meta_box( 'dashboard_recent_comments', 'dashboard', 'normal' );
         // D'un coup d'oeil
         remove_meta_box( 'dashboard_right_now', 'dashboard', 'normal' );
         // Activité
         remove_meta_box( 'dashboard_activity', 'dashboard', 'normal');
         */

        //
        // Add the Dashboard Widget.
        //

        require_once(__DIR__.'/CidffDashWidget.php');
        (new CidffDashWidget($this->widget_id))
            ->render();

    }

    /**
     * Allow SVG upload for editor+ only.
     * - https://bjornjohansen.no/svg-in-wordpress
     * @param Array|null $upload_mimes
     * @return void|Array
     */
    public function wp_upload_mimes_svg( $upload_mimes )
    {
        //CidffPlugin::debug(__METHOD__);
        //CidffPlugin::debug(__METHOD__, $upload_mimes);

        if( ! current_user_can('edit_others_posts') )
            return ;

        $upload_mimes['svg'] = 'image/svg+xml';
        $upload_mimes['svgz'] = 'image/svg+xml';        
        return $upload_mimes;
    }

    /**
     * Fixes the issue in WordPress 4.7.1 being unable to correctly identify SVGs
     *
     * @thanks @lewiscowles
     *
     * @param null $data
     * @param null $file
     * @param null $filename
     * @param null $mimes
     *
     * @return null
     */
    public function wp_check_filetype_and_ext_fix_mime_type_svg( $data = null, $file = null, $filename = null, $mimes = null )
    {
        if( ! current_user_can('edit_others_posts') )
            return $data ;

        $ext = isset( $data['ext'] ) ? $data['ext'] : '';
        if ( strlen( $ext ) < 1 ) {
            $exploded = explode( '.', $filename );
            $ext      = strtolower( end( $exploded ) );
        }
        if ( $ext === 'svg' ) {
            $data['type'] = 'image/svg+xml';
            $data['ext']  = 'svg';
        } elseif ( $ext === 'svgz' ) {
            $data['type'] = 'image/svg+xml';
            $data['ext']  = 'svgz';
        }

        return $data;
    }

    /**
     * Here we validate, or not, the question data.
     * Managing errors with WP:
     * - https://www.sitepoint.com/displaying-errors-from-the-save_post-hook-in-wordpress/
     * 
     * https://developer.wordpress.org/reference/hooks/wp_insert_post_empty_content/
     * @param bool $maybe_empty
     * @return bool Return true to cancel post save.
     */
    function wp_insert_post_empty_content( bool $maybe_empty )
    {
        static $calls = 0, $validation_ok = true ;

/*
        // Only our CPT 
        if ( $post->post_type !== CidffPlugin::CPT_CARD )
            return;
        // Check ACL
        if( ! current_user_can('edit_post', $post_id))
            return;
        // Do nothing on revision or autosave
        if( wp_is_post_revision($post_id) || wp_is_post_autosave($post_id) )
            return ;
*/

        CidffPlugin::debug(__METHOD__, '$maybe_empty:', $maybe_empty, '$call:',$calls ++ );
        //CidffPlugin::debug(__METHOD__, debug_string_backtrace() );
        //CidffPlugin::debug(__METHOD__, $_POST );

        // don't mind about second call
        if( $calls == 2 )
            return  ! $validation_ok ;

        // check validation for each meta datum.
        foreach( CidffPlugin::getMetaKeys() as $meta )
        {
            if( ! $this->{'validate_'.$meta}( $_POST ) )
                $validation_ok = false ;
        }

        return ! $validation_ok ;
    }

    /**
     * Question validation.
     * 
     * @param array $data
     * @return boolean
     */
    protected function validate_question( Array $data )
    {
        $k = CidffPlugin::CPT_CARD.'-'.CidffPlugin::META_QUESTION ;
        if( ! isset($data[$k]) || empty($data[$k]) )
        {
            CidffPlugin::addError('La question ne peut pas être vide.', $k );
            return false ;
        }
        return true ;
    }

    protected function validate_choices( Array $data )
    {
        $k = CidffPlugin::CPT_CARD.'-'.CidffPlugin::META_CHOICES ;
        if( ! isset($data[$k]) || empty($data[$k]) || (! is_array($data[$k])) )
        {
            CidffPlugin::addError('Les choix ne peuvent pas être vide.', $k );
            return false ;
        }

        $reponses_count = 0 ;
        foreach( $data[$k] as $idx => $choice )
        {
            if( ! isset($choice['text']) || empty($choice['text']) )
            {
                CidffPlugin::addError('Le choix doit contenir un texte.', $k );
                return false ;
            }
            if( isset($choice['good']) )
                $reponses_count ++ ;
        }

        if( count($data[$k])>1 && $reponses_count == 0 )
        {
            CidffPlugin::addError('Au moins une bonne réponse est nécessaire quand il y a plusieurs choix.', $k );
            return false ;
        }

        return true ;
    }

    protected function validate_answer( Array $data )
    {
        $k = CidffPlugin::CPT_CARD.'-'.CidffPlugin::META_ANSWER ;
        if( ! isset($data[$k]) || empty($data[$k]) )
        {
            CidffPlugin::addError('La réponse ne peut pas être vide.', $k );
            return false ;
        }
        return true ;
    }

    protected function validate_level( Array $data )
    {
        $k = CidffPlugin::CPT_CARD.'-'.CidffPlugin::META_LEVEL ;
        if( ! isset($data[$k]) || empty($data[$k]) )
        {
            CidffPlugin::addError('Le niveau ne peut pas être null.', $k );
            return false ;
        }
        $i = intval($data[$k]);
        if( $i < CidffPlugin::QUESTION_LEVEL_MIN || $i > CidffPlugin::QUESTION_LEVEL_MAX )
        {
            CidffPlugin::addError('Le niveau doit être compris entre '.CidffPlugin::QUESTION_LEVEL_MIN.' et '.CidffPlugin::QUESTION_LEVEL_MAX.'.', $k );
            return false ;
        }
        return true ;
    }

    /**
     * Filters slashed post data just before it is inserted into the database.
     *
     * @since 2.7.0
     * @since 5.4.1 `$unsanitized_postarr` argument added.
     *
     * @param array $data                An array of slashed, sanitized, and processed post data.
     * @param array $postarr             An array of sanitized (and slashed) but otherwise unmodified post data.
     * @param array $unsanitized_postarr An array of slashed yet *unsanitized* and unprocessed post data as
     *                                   originally passed to wp_insert_post().
     */
    public function wp_insert_post_data( $data, $postarr, $unsanitized_postarr )
    {
        // Only our CPT 
        if ( $data['post_type'] !== CidffPlugin::CPT_CARD )
            return $data ;

        //CidffPlugin::debug(__METHOD__, '$data:', $data);

        if( isset($_POST['cidff_card-question']) && ! empty($_POST['cidff_card-question']) )
        {
            $data['post_title'] = wp_trim_words($_POST['cidff_card-question']) ;
            $data['post_name'] = sanitize_title($_POST['cidff_card-question']);
        }
        return $data ;
    }

    /**
     * Fires immediately before an existing post is updated in the database.
     */
    public function wp_pre_post_update( $post_id, Array $post_data )
    {
        //CidffPlugin::debug(__METHOD__, '$post_id:', $post_id);

        // Only our CPT 
        if ( $post_data['post_type'] !== CidffPlugin::CPT_CARD )
            return;

        //CidffPlugin::debug(__METHOD__, '$post_data:', $post_data, $_POST );

        // Check ACL
        if( ! current_user_can('edit_post', $post_id))
            return;
        // Do nothing on revision or autosave
        //CidffPlugin::debug(__METHOD__, 'wp_is_post_revision:', wp_is_post_revision($post_id), 'wp_is_post_autosave:', wp_is_post_autosave($post_id));
        if( wp_is_post_revision($post_id) || wp_is_post_autosave($post_id) )
            return ;

        $valid = true ;
        foreach( CidffPlugin::getMetaKeys() as $meta )
        {
            // check validation and will store a message if not valid.
            if( ! $this->{'validate_'.$meta}( $_POST ) )
            {
                $valid = false ;
            }
            else
            {
                $value = $_POST[CidffPlugin::CPT_CARD.'-'.$meta];
                //CidffPlugin::debug(__METHOD__,'meta:',$meta,'$value:',$value);
                update_post_meta( $post_id, $meta, $value);
            }
        }
        if( ! $valid )
        {
            wp_safe_redirect( get_edit_post_link($post_id, 'redirect') );
        }

    }

    public function wp_admin_enqueue_scripts()
    {
        /**
         * @var \WP_Post
         */
        global $post;
        $screen = get_current_screen();

        //CidffPlugin::debug(__METHOD__, 'screen:', $screen->id);
        //CidffPlugin::debug(__METHOD__, 'screen:', $screen->id, 'post:',$post);

        /**
         * Card's taxonomy list
         */
        switch( $screen->id )
        {
        case 'edit-' . CidffPlugin::TAX_CARD:
            wp_register_script( CidffPlugin::PLUGIN_NAME.'-edit-cidff-category', CidffPlugin::asset_url() . '/edit-cidff_category.js',
                array( 'jquery' ), CidffPlugin::VERSION, true );
            wp_enqueue_script( CidffPlugin::PLUGIN_NAME.'-edit-cidff-category' );
            break;

        /**
         * Cards list
         */
        case 'edit-' . CidffPlugin::CPT_CARD:
            wp_register_script( CidffPlugin::PLUGIN_NAME.'-edit-cidff-card', CidffPlugin::asset_url() . 'edit-cidff_card.js',
                array( 'jquery' ), CidffPlugin::VERSION, true );
            wp_enqueue_script( CidffPlugin::PLUGIN_NAME.'-edit-cidff-card' );
            break;

        /**
         * Card edit
         */
        case CidffPlugin::CPT_CARD:
            wp_register_style(CidffPlugin::PLUGIN_NAME.'-cidff-card', CidffPlugin::asset_url().'cidff_card.css',
                null,
                CidffPlugin::VERSION, 'all');
            wp_enqueue_style( CidffPlugin::PLUGIN_NAME.'-cidff-card' );
    
            wp_register_script( CidffPlugin::PLUGIN_NAME.'-cidff-card', CidffPlugin::asset_url() . 'cidff_card.js',
                array( 'jquery' ), CidffPlugin::VERSION, true );
            wp_enqueue_script( CidffPlugin::PLUGIN_NAME.'-cidff-card' );
            // in JavaScript, object properties are accessed as ajax_object.ajax_url, ajax_object.we_value
            wp_localize_script( CidffPlugin::PLUGIN_NAME.'-cidff-card', 'cidff_card',
            [
                'post' => $post ,
            ] );

            break;
        }
        
    }

    /**
     * Construct the form for question and responses.
     * 
     * https://make.wordpress.org/core/2012/12/01/more-hooks-on-the-edit-screen/
     * 
     * @param \WP_Post $post
     */
    public function wp_edit_form_after_editor( /*\WP_Post*/ $post )
    {
        if( ! isset($post->post_type))
            return ;
        if( $post->post_type != CidffPlugin::CPT_CARD )
            return ;
        //CidffPlugin::debug(__METHOD__, 'post_type:', $post->post_type);

        $k = CidffPlugin::getTransientKeyForSession( CidffPlugin::CPT_CARD.'-errors-');
        if( $k )
        {
            $errors = \get_transient( $k );
            \delete_transient( $k );    
        }
        else
        {
            $errors = null ;
        }

        require_once( CidffPlugin::views_dir().'_errors.php');

        foreach( CidffPlugin::getMetaKeys() as $meta )
        {
            // retrieve data
            $data = get_post_meta($post->ID, $meta, true) ;

            // set default value
            switch( $meta )
            {
                case CidffPlugin::META_CHOICES:
                    if( empty($data) )
                    {
                        $data = [
                            ['text' => '',],
                        ];
                    }
                    break;
            }

            // load the view
            // _question.php, _choices.php, _answer.php, _level.php
            include( CidffPlugin::views_dir().'_'.$meta.'.php');
        }
    }

    /**
     * For Question post search will return nothing
     * that remove pre-query on post_tile.
     */
    public function wp_posts_search( $args )
    {
        //CidffPlugin::debug(__METHOD__, '$args', $args );
        global $wp ;
        if( $wp->query_vars['post_type'] != CidffPlugin::CPT_CARD )
            return $args ;
        return ;
    }

    /**
     * Add meta_query to search query.
     */
    public function wp_pre_get_posts_search_query( $query )
    {
        //CidffPlugin::debug(__METHOD__, '$query->is_search', $query->is_search);
        if( ! $query->is_search )
            return ;

        if( $query->query_vars['post_type'] != CidffPlugin::CPT_CARD )
            return ;

        $meta_query_args = array(
            //'relation' => 'OR',
            array(
                'key' => [CidffPlugin::META_QUESTION, CidffPlugin::META_ANSWER, CidffPlugin::META_CHOICES],
                'value' => $query->query_vars['s'],
                'compare' => 'LIKE',
            ),/*
            array(
                'key' => CidffPlugin::META_ANSWER,
                'value' => $query->query_vars['s'],
                'compare' => 'LIKE',
            ),
            array(
                'key' => CidffPlugin::META_CHOICES,
                'value' => $query->query_vars['s'],
                'compare' => 'LIKE',
            ),*/
        );
        $query->set('meta_query', $meta_query_args);
        //add_filter( 'get_meta_sql', [$this,'wp_get_meta_sql_search_query'] );

    }

    public function wp_add_meta_boxes()
    {
        /*
         * Don't use metaboxes with TinyMCE !
         * https://make.wordpress.org/core/2012/12/01/more-hooks-on-the-edit-screen/
         */

        /*
        add_meta_box( CidffPlugin::CPT_CARD.'-question', 'La question',
            [$this, 'meta_box_question'], CidffPlugin::CPT_CARD,
            'normal', // "normal", "advanced" and "side"
            'high', // "high", "core", "default", and "low"
            null // $callback_args
            );
            */
        /*
        add_meta_box( CidffPlugin::CPT_CARD.'-choices', 'Les choix de réponse',
            [$this, 'meta_box_choices'], CidffPlugin::CPT_CARD,
            'normal', // "normal", "advanced" and "side"
            "high", // "high", "core", "default", and "low"
            null // $callback_args
            );
            */
    }

    public function wp_manage_taxo_columns( Array $columns )
    {
        //CidffPlugin::debug(__METHOD__,'columns:',$columns);

        $columns['pdf_file'] = __( 'Pdf', 'your_text_domain' );
        unset($columns['description']);
        unset($columns['slug']);
        $columns['posts'] = __( 'Questions', 'your_text_domain' );;

        return $columns ;
    }

    public function wp_manage_taxo_custom_column( $column_content, $column_name, $term_id )
    {
        //CidffPlugin::debug(__METHOD__,'$column_name:',$column_name,'$term_id:',$term_id);
        switch ( $column_name )
        {
        case 'pdf_file':
            $tm_pdf_attach_id = get_term_meta($term_id, CidffPlugin::TERM_META_PDF_ATTACH, true );
            //CidffPlugin::debug(__METHOD__,'$tm_pdf_attach_id:',$tm_pdf_attach_id);
            if( $tm_pdf_attach_id )
            {
                $post = get_post( $tm_pdf_attach_id );
                $column_content = '<a target="_blank" href="'.$post->guid.'">'.$post->post_title.'</a><br/>'.$post->post_modified ;
            }
            break;
        }
        return $column_content ;
    }

    public function wp_manage_taxo_sortable_column( $columns )
    {
        //CidffPlugin::debug(__METHOD__, '$columns:',$columns);
        return [
            'name' => 'name',
        ];
    }

    /**
     * Define columns headers for CPT list.
     * 
     * @param array $columns
     * @return array
     */
    public function wp_manage_posts_columns( Array $columns ) 
    {
        //CidffPlugin::debug(__METHOD__);
        //CidffPlugin::debug(__METHOD__, '$columns:',$columns);

        // Cache (request lifecycle) new columns because this method is called several times ...
        // Only if unset 'title' column, I don't know why ...
        static $cols = null ;

        if( $cols != null )
            return $cols ;

        /*
        unset( $columns['title'] );
        $columns['question'] = __( 'Question', 'your_text_domain' );
        $columns['choices_count'] = __( 'Choix', 'your_text_domain' );
        return $columns;
        */

        $cols = [];
        $cols['cb'] = $columns['cb'];
        $cols['question'] = 'Question';
        $cols['cidff_card_level'] = 'Niveau';
        $cols['choices_count'] = __( 'Choix', 'your_text_domain' );
        $cols['taxonomy-cidff_category'] = $columns['taxonomy-cidff_category'];
        $cols['author'] = $columns['author'];
        //$cols['tags'] = $columns['tags'];
        //$cols['comments'] = $columns['comments'];
        $cols['updated_at'] = 'Modification';

        return $cols ;
    }

    public function wp_manage_posts_sortable_column( $columns )
    {
        $columns['cidff_card_level'] = 'cidff_card_level';
        $columns['taxonomy-cidff_category'] = 'taxonomy-cidff_category';
        $columns['updated_at'] = 'updated_at';
        return $columns;
    }

    public function wp_pre_get_posts_sort_column( $query )
    {
        $orderby = $query->get( 'orderby');
        if( $orderby == 'updated_at' )
        {
            $query->set( 'orderby', 'modified' );
        }
        else if( $orderby == 'cidff_card_level' )
        {
            $query->set( 'meta_key', CidffPlugin::META_LEVEL );
            $query->set( 'orderby', 'meta_value_num' );
        }
    }

    /**
     * Render (echo) column content for column unkown by WP.
     * 
     * @param string $column
     * @param integer $post_id
     */
    public function wp_manage_posts_custom_column( $column, $post_id )
    {
        //CidffPlugin::debug(__METHOD__, '$column:',$column);
        global $post ;

        switch ( $column )
        {
            case 'question' :
                $data = get_post_meta($post_id, CidffPlugin::META_QUESTION, true) ;
                echo $data ;
                break;
            case 'choices_count':
                $data = get_post_meta($post_id, CidffPlugin::META_CHOICES, true) ;
                echo (! empty($data) ? count($data) : '<span style="color:red;">0</span>') ;
                break;
            case 'cidff_card_level':
                $data = get_post_meta($post_id, CidffPlugin::META_LEVEL, true) ;
                echo (! empty($data) ? $data : '<span style="color:red;">?</span>') ;
                break;
            case 'updated_at':
                $dt = get_post_datetime( $post, 'modified' );
                //echo $dt->format( get_option('date_format') ), ' ', $dt->format( get_option('time_format') );
                //echo $dt->format( __( 'Y/m/d g:i a' ) );
                echo $dt->format( __( 'd/m/Y H:i' ) );
                break;
        }
    }

    /**
     * Add a selectBox to filter CPT a custom TAXo.
     * 
     * @param string $post_type
     * @param string $which
     */
    public function wp_restrict_manage_posts( string $post_type, string $which = null )
    {
        //CidffPlugin::debug(__METHOD__, '$post_type:',$post_type,'$which:',$which);

        if( $post_type != CidffPlugin::CPT_CARD )
            return ;

        global $wp_query;
        //CidffPlugin::debug(__METHOD__,'$wp_query:',$wp_query);

        $taxo = CidffPlugin::TAX_CARD ;

        // https://codex.wordpress.org/Function_Reference/wp_dropdown_categories
        wp_dropdown_categories(
        [
            'show_option_all' =>  'Toutes les catégories',
            'taxonomy'        =>  $taxo,
            'name'            =>  $taxo,
            'value_field' => 'slug', // use "slug" to be compatible with default list filter in category column
            'orderby'         =>  'name',
            'selected'        =>  isset($wp_query->query[$taxo]) ? $wp_query->query[$taxo] : null,
            'hierarchical'    =>  true,
            //'depth'           =>  3,
            'show_count'      =>  true, // Show # listings in parens
            'hide_empty'      =>  true, // Don't show businesses w/o listings
        ]);
    }

}
