<?php

namespace CidffPlugin ;

require_once( __DIR__.'/CidffQuiz.php');

if( defined('WP_DEBUG') && WP_DEBUG )
{
    function debug_string_backtrace()
    {
        ob_start();
        debug_print_backtrace();
        $trace = ob_get_contents();
        ob_end_clean();

        // Remove first item from backtrace as it's this function which is redundant.
        $trace = preg_replace ('/^#0\s+' . __FUNCTION__ . "[^\n]*\n/", '', $trace, 1);

        // Renumber backtrace items.
        //$trace = preg_replace ('/^#(\d+)/m', '\'#\' . ($1 - 1)', $trace);
        
        return $trace ;
    }
}

require_once( __DIR__.'/Installer.php');

class CidffPlugin
{
    const PLUGIN_NAME = 'CidffQuiz' ;
    const VERSION = '1.0.4' ;

    const CPT_CARD = 'cidff_card';
    const TAX_CARD = 'cidff_category';

    const META_QUESTION = 'question' ; // string, Text of question.
    const META_CHOICES = 'choices' ; // array, Text and Good answer of choices.
    const META_ANSWER = 'answer' ; // string, Text of answer.
    const META_LEVEL = 'level' ; // string, Level of question.
    protected static $metaKeys = [
        self::META_QUESTION, self::META_CHOICES, self::META_ANSWER, self::META_LEVEL
    ];

    const TERM_META_PDF_ATTACH = 'term_pdf_attachment' ; // int, ID of attachment.
    const TERM_META_PDF_CONFIG = 'term_pdf_config' ; // json

    const SHORTCODE_CARDSDOWNLOAD = 'quiz_cards_download' ;

    const QUESTION_LEVEL_MIN = 1 ;
    const QUESTION_LEVEL_MAX = 2 ;
    const QUESTION_LEVEL_LABELS = [
        //1=>'facile', 2=>'moyen', 3=>'expert',
        1=>'découvrir', 2=>'approfondir',
    ] ;

    const CPT_QUIZPRESET = 'cidff_quizpreset';
    const META_PRESET = 'preset' ;

    public static $sitemap_exclude = [
        'post' => ['post','cidff_quizpreset'],
        'taxo' => ['category','post_tag','post_format'],
    ];
    protected static $debug ;
    protected $installer ;
    protected static $plugin_dir ;
    protected static $plugin_dir_url ;
    protected static $asset_url_public ;
    protected static $asset_url_admin ;

    /**
     * @var \CidffPlugin\CidffPlugin
     */
    private static $_instance ;

    /**
     * Singleton pattern to facilitate plugin instance referencing.
     * 
     * @return \CidffPlugin\CidffPlugin
     */
    public static function getInstance()
    {
        if( self::$_instance == null )
            self::$_instance = new static();
        return self::$_instance ;
    }

    protected function __construct()
    {
        self::$debug = (defined('WP_DEBUG') ? WP_DEBUG : false);

        self::$plugin_dir = plugin_dir_path( __DIR__.'/../README.md' );

        self::$plugin_dir_url = plugin_dir_url( __DIR__.'/../README.md' );
        self::$asset_url_public = self::$plugin_dir_url.'public/';
        self::$asset_url_admin = self::$plugin_dir_url.'admin/';

        add_action( 'init', [$this, 'wp_init'] );
    }

    public function wp_init()
    {
        CidffPlugin::debug(__METHOD__);

        Installer::register_post_type();

        $this->init_taxo_term_img();

        if( defined('DOING_AJAX') && DOING_AJAX  && isset($_REQUEST['action'])
            && in_array( $_REQUEST['action'], ['heartbeat','wp-refresh-post-lock'] )
        )
        {
            //CidffPlugin::debug(__METHOD__,'heartbeat, abort');
            return ;
        }

        CidffPlugin::debug(__METHOD__, 'REQUEST_URI:', $_SERVER['REQUEST_URI'],'REQUEST:',$_REQUEST,'FILES:',$_FILES);

        // Only when needed.
        //CidffPlugin::getTransientKeyForSession();

        /*
        $this->enqueue_styles();
        protected function enqueue_styles()
        {
            if( is_admin() )
                wp_enqueue_style( self::PLUGIN_NAME, self::asset_url().'admin.css', array(), self::VERSION, 'all' );
            else
                wp_enqueue_style( self::PLUGIN_NAME, self::asset_url().'public.css', array(), self::VERSION, 'all' );
        }
        */

        if( is_admin() )
        {
            require_once( __DIR__.'/CidffAdmin.php');
            (new CidffAdmin())->wp_init();

            require_once( __DIR__.'/CidffAdminQuizPreset.php');
            (new CidffAdminQuizPreset())->wp_init();
        }
        else
        {
            require_once( __DIR__.'/CidffPublic.php');
            (new CidffPublic())->wp_init();
        }

    }

    /**
     * The array contains each meta datum key like 'question', 'choices', ...
     * 
     * @return string[]
     */
    public static function getMetaKeys()
    {
        return self::$metaKeys ;
    }

    /**
     * Get a unique session key,
     * because Wordpress does not activate session (by default).
     * @param string $key
     * @return string
     */
    public static function getTransientKeyForSession( $key = null )
    {
        static $id = null ;

        if( empty($id) )
        {
            if( isset($_COOKIE[session_name()]))
            {
                // the php session cookie only present if php session started.
                //CidffPlugin::debug(__METHOD__, 'Get $_COOKIE['.session_name().']');
                $id = $_COOKIE[session_name()];
            }
            else if( isset($_COOKIE['_gid']))
            {
                // "_gid" cookie from Google Analytics responsible for tracking user behavior, it expires after 24h of inactivity.
                //CidffPlugin::debug(__METHOD__, 'Get $_COOKIE[_gid]');
                $id = $_COOKIE['_gid'];
            }
            else if( isset($_COOKIE['cidff45quiz']))
            {
                // Our cookie
                //CidffPlugin::debug(__METHOD__, 'Get $_COOKIE[cidff45quiz]');
                $id = $_COOKIE['cidff45quiz'];
            }
            else
            {
                // Create a id cookie
                $id = uniqid() . random_int(10000,99999) ;
                if( headers_sent() )
                {
                    /*CidffPlugin::debug('ERROR: To late, headers already sent!',
                    \xdebug_call_class()."::".\xdebug_call_function()." is called at ".\xdebug_call_file().":".\xdebug_call_line()
                    );*/
                    //CidffPlugin::debug('ERROR: To late, headers already sent!', debug_string_backtrace());
                    CidffPlugin::debug('ERROR: To late, headers already sent!');
                    return null ;
                }
                else
                {
                    //CidffPlugin::debug(__METHOD__, 'Set $_COOKIE[cidff45quiz]',$id);
                    setcookie( 'cidff45quiz', $id, time() + 30 * DAY_IN_SECONDS );
                }
        
            }
        }
        //CidffPlugin::debug(__METHOD__, 'id:',$id);

        return ! $key ? $id : $key.'_'.$id ;
    }

    /**
     * Initialize Taxonomy Term Image.
     * Check if plugin exists add our taxo,
     * if not load it from embeded version and set our taxo only.
     */
    protected function init_taxo_term_img()
    {
        if( ! class_exists( 'Taxonomy_Term_Image' ) )
        {
            // Plugin does not exist.
            require_once( CidffPlugin::plugin_dir().'lib/taxonomy-term-image/taxonomy-term-image.php');
            add_filter( 'taxonomy-term-image-taxonomy', function ( $taxonomy )
            {
                return self::TAX_CARD ;
            });
            // Don't forget to instanciate !
            \Taxonomy_Term_Image::instance();
        }
        else
        {
            // Plugin alreay present, reuse it
            add_filter( 'taxonomy-term-image-taxonomy', function ( $taxonomy )
            {
                $taxonomy[] = self::TAX_CARD ;
                return $taxonomy ;
            });
        }
    }

    public static function plugin_dir()
    {
        return self::$plugin_dir ;
    }
    
    public static function views_dir( $admin = false )
    {
        if( is_admin() || $admin )
            return self::$plugin_dir.'admin/views/' ;
        else
            return self::$plugin_dir.'public/views/' ;
    }
    
    public static function asset_url( $admin = false )
    {
        if( is_admin() || $admin )
            return self::$asset_url_admin ;
        else
            return self::$asset_url_public ;
    }

    /**
     * TODO: implement $field.
     *
     * @param string $errMsg
     * @param string $field Not implemented
     */
    public static function addError( $errMsg, $field = null )
    {
        CidffPlugin::debug(__METHOD__, 'errMsg:',$errMsg, 'field:', $field );

        $k = self::getTransientKeyForSession( CidffPlugin::CPT_CARD.'-errors-');
        //CidffPlugin::debug(__METHOD__, 'transKey:',$k);

        $errors = get_transient( $k );
        if( ! is_array($errors) )
            $errors = [];
            
        if( $field )
            $errors[$field] = $errMsg ;
        else
            $errors[] = $errMsg ;

        set_transient( $k, $errors );
    }

    public static function debug( ...$items )
    {
        if( ! self::$debug )
            return ;

        $msg = '' ;
        foreach( $items as $item )
        {
            switch ( gettype($item))
            {
                case 'boolean' :
                    $msg.= ($item ? 'true':'false');
                    break;
                case 'NULL' :
                    $msg.= 'null';
                    break;
                case 'integer' :
                case 'double' :
                case 'float' :
                case 'string' :
                    $msg.= $item ;
                    break;
                default:
                    $msg .= var_export($item,true) ;
            }
            $msg.=' ';
        }
        error_log( $msg );
    }

    public function getQuiz()
    {
        $play = new CidffQuizPlay();
        //$play->questions_per_category = 1 ;

        return CidffQuiz::getPlay( $play );
    }
}
