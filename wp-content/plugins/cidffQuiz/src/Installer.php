<?php 
namespace CidffPlugin ;


/**
 *
 */
class Installer
{
    /**
     * https://developer.wordpress.org/plugins/plugin-basics/uninstall-methods/
     * |                                       Hook: |Deactivation | Uninstall |
     * | Flush Cache/Temp                            | Yes         | No        |
     * | Flush Permalinks                            | Yes         | No        |
     * | Remove Options from {$wpdb->prefix}_options | No          | Yes       |
     * | Remove Tables from wpdb                     | No          | Yes       |
     */
    public static function wp_uninstall()
    {
    }

    public static function wp_activation()
    {   
        //error_log(__METHOD__);
        self::check_dependencies();
        flush_rewrite_rules();
    }

    public static function wp_deactivation()
    {
        //error_log(__METHOD__);
        unregister_post_type( CidffPlugin::CPT_CARD );
        flush_rewrite_rules();
    }
    public static function register_post_type()
    {
        self::register_cpt_card();
        self::register_cpt_quizpreset();
    }

    protected static function register_cpt_quizpreset()
    {
        register_post_type( CidffPlugin::CPT_QUIZPRESET,
        [
            'public' => true,
            'description' => 'Des configurations de Quiz',
            'rewrite'     => [
                'slug' => 'quizpresets'
            ],
            'has_archive' => true,
            'show_in_admin_bar' => true,
            // https://developer.wordpress.org/resource/dashicons/
            'menu_icon' => 'dashicons-money',
            'show_in_rest' => true ,
            'can_export' => true ,
            'publicly_queryable'  => true,
            'capability_type'     => 'post',
            'delete_with_user' => false,
            'labels' => [
                'name' => 'Quiz',
                'singular_name' => 'Quiz',
                'add_new_item' => 'Ajouter un Quiz (présélections de questions)',
            ],
            //'supports' => ['title', 'editor', 'comments', 'thumbnail', 'author', 'custom-fields'],
            'supports' => ['title', /*'comments',*/ 'author', 'custom-fields'],
        ] );

        register_post_meta( CidffPlugin::CPT_QUIZPRESET, CidffPlugin::META_PRESET, [
            'show_in_rest'      => true,
            'sanitize_callback' => 'array',
        ] );

    }

    protected static function register_cpt_card()
    {
        // https://developer.wordpress.org/reference/functions/register_post_type/

        register_post_type( CidffPlugin::CPT_CARD,
        [
            'public' => true,
            'description' => 'Des questions pour construire des Quiz',
            'rewrite'     => [
                'slug' => 'questions'
            ],
            'has_archive' => true,
            'show_in_admin_bar' => true,
            // https://developer.wordpress.org/resource/dashicons/
            'menu_icon' => 'dashicons-awards',
            'show_in_rest' => true ,
            'can_export' => true ,
            'publicly_queryable'  => true,
            'capability_type'     => 'post',
            'delete_with_user' => false,
            'labels' => [
                'name' => 'Questions Quiz',
                'singular_name' => 'Question quiz',
                'add_new_item' => 'Ajouter une question',
            ],
            //'supports' => ['title', 'editor', 'comments', 'thumbnail', 'author', 'custom-fields'],
            'supports' => [/*'comments',*/'author', 'custom-fields'],
        ] );

        // https://developer.wordpress.org/plugins/metadata/managing-post-metadata/
        // https://developer.wordpress.org/block-editor/tutorials/metabox/meta-block-2-register-meta/

        register_post_meta( CidffPlugin::CPT_CARD, CidffPlugin::META_QUESTION, [
            'show_in_rest'      => true,
            'sanitize_callback' => 'text',
            'single'       => true,
            'type'         => 'string',
        ] );
        register_post_meta( CidffPlugin::CPT_CARD, CidffPlugin::META_CHOICES, [
            'sanitize_callback' => 'array',
            'single'       => true,
            'type'         => 'array',
            //'show_in_rest'      => true,// Need more definition for array type !
        ] );
        register_post_meta( CidffPlugin::CPT_CARD, CidffPlugin::META_ANSWER, [
            'show_in_rest'      => true,
            'sanitize_callback' => 'text',
            'single'       => true,
            'type'         => 'string',
        ] );
        register_post_meta( CidffPlugin::CPT_CARD, CidffPlugin::META_LEVEL, [
            'show_in_rest'      => true,
            'sanitize_callback' => 'integer',
            'single'       => true,
            'type'         => 'integer',
            'default'      => CidffPlugin::QUESTION_LEVEL_MIN,
        ] );

        // https://codex.wordpress.org/Function_Reference/register_taxonomy
        // https://developer.wordpress.org/reference/functions/register_taxonomy/

        register_taxonomy( CidffPlugin::TAX_CARD, [CidffPlugin::CPT_CARD],
        [
            'hierarchical'      => true,
            'public' => true,
            'show_ui'           => true,
            'show_in_quick_edit' => true,
            'show_in_menu' => true,
            'show_admin_column' => true,
            'show_in_nav_menus' => true ,
            'show_in_rest' => true ,
            'show_tagcloud' => true,
            'query_var'         => true,
            //'rewrite'           => ['slug' => 'quizcategory'],
                /*
            'labels'            => [
                'name'              => _x('Catégories', 'taxonomy general name'),
                'singular_name'     => _x('Catégorie de questions', 'taxonomy singular name'),
                'search_items'      => 'Rechercher',
                'all_items'         => 'All Courses',
                'edit_item'         => 'Edit Course',
                'update_item'       => 'Update Course',
                'add_new_item'      => 'Ajouter une catégorie de questions',
                'new_item_name'     => 'New Course Name',
                'menu_name'         => 'Catégorie',
            ],*/
        ] );
        // From codex: Better be safe than sorry when registering custom taxonomies for custom post types.
        // Use register_taxonomy_for_object_type() right after the function to interconnect them.
        register_taxonomy_for_object_type( CidffPlugin::TAX_CARD, CidffPlugin::CPT_CARD ); 

        // Done by Taxonomy_Term_Image
        //register_term_meta( CidffPlugin::TAX_CARD, 'term_image', array $args );

        // https://developer.wordpress.org/reference/functions/register_term_meta/
        register_term_meta( CidffPlugin::TAX_CARD, CidffPlugin::TERM_META_PDF_ATTACH, [
            'description' => 'Generate PDF file Post.ID',
            'single' => true,
            'type' => 'integer',
            'show_in_rest'      => true,
            'sanitize_callback' => 'absint',
        ] );

        register_term_meta( CidffPlugin::TAX_CARD, CidffPlugin::TERM_META_PDF_CONFIG, [
            'description' => 'Configuration for PDF generation',
            'single' => true,
            'type' => 'array',
            //'show_in_rest'      => true,// Need more definition for array type !
            //'sanitize_callback' => '',
        ] );

    }

    protected static function check_dependencies()
    {
        require_once ABSPATH . 'wp-admin/includes/plugin.php';
        if( ! is_plugin_active( 'advanced-custom-fields/acf.php' ) )
            die('Error: Plugin ACF must be installed and activated');

    }
}
