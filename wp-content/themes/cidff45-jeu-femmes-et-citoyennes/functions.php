<?php
/**
 *
 * Fonctions du Thème Wordpress Courant Alternatif
 *
 * - Includes
 *
 */


/*================================
=            includes            =
================================*/

/*----------  php ----------*/

// Fonction personnalisé du theme
include "_functions/theme.php";		// Fonctions qui appel des templates d'affichages
include "_functions/load-assets.php";	// Fonctions permettant le chargement ciblé de scripts ou style

// Custom post type
// include "_custom/custom-type_nomDuType.php";

// Custom taxonomy
// include "_custom/custom-taxo_nomDeLaTaxo.php";

/*----------  ADMIN  ----------*/

include "_admin/admin.php";		// Fonctions de personnalisation de l'administration, en complement des options présente dans le plugins Courant Alternatif

include "_admin/gravity-custom.php"; // Fonctions de personnalisation de l'administration de gravity pour les editeurs

/* PAGE DE PARAMETRE EDITEUR */
$caParamOptions = get_option('ca-param-option');	// Création du tableau
include '_admin/ca-config-page.php';				// page de paramètre à destination des Editeurs

add_action( 'admin_enqueue_scripts', 'ca_wp_admin_inc' ); // Chargement des styles
add_action( 'login_enqueue_scripts', 'ca_wp_admin_inc' );

	function ca_wp_admin_inc() {

	    // scripts pour admin
	    wp_enqueue_script( 'ca-wp-admin', get_template_directory_uri().'/_admin/ca-wp-admin-scripts.js', false, false );

	    // css pour admin
	    wp_enqueue_style( 'ca-wp-admin', get_template_directory_uri().'/_admin/ca-wp-admin-style.css', false, false, 'screen' );

	    // media uploader
	    wp_enqueue_media();

	}

/*=====  End of includes  ======*/

?>