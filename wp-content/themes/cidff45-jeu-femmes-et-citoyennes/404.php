<?php get_header(); ?>

<main id="main" class="main page">
	<div class="main-inner container container-full">

		<h1>Erreur 404</h1>
		<div class="wp-wysiwyg">
			<h2>Page introuvable</h2>
			<p class="">La page que vous cherchez est introuvable. Nous vous invitons à naviguer sur le site ou à faire une recherche pour la trouver.</p>
			<p><a href="<?= get_home_url(); ?>" class="wysiwyg-btn btn">Retour à l'accueil</a></p>
			<hr class="wysiwyg-separator separator-small">
			<h3>Besoin d'aide, faite une recherche</h3>
			<?php display_search('page','page'); ?>
			<hr class="wysiwyg-separator separator-small">
		</div>
	</div>
</main>

<?php get_footer(); ?>