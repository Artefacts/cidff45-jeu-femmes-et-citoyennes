<?php get_header(); ?>

<main id="main" class="main archive">
	<div class="main-inner container">
		
		<h1>Actualités</h1>
		<?php if ( have_posts() ) : ?>
			<div class="archive-container mb-5"><div class="row">
			<?php while ( have_posts() ) : the_post(); ?>
				<div class="col-md-6 col-lg-4">
					<?php include '_templates/news/resume.php'; ?>
				</div>
			<?php endwhile; ?>
			</div></div>
		<?php else : ?>
		
			<p class="query-no-result">Aucun résultat.</p>

		<?php endif; ?>

		<?php ca_pager('<span aria-hidden="true">&laquo;</span><span class="sr-only">Précédent</span>','<span aria-hidden="true">&raquo;</span><span class="sr-only">Suivant</span>'); ?>

	</div>
</main>

<?php get_footer(); ?>

