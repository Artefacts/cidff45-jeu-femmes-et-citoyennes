<?php

/**
 *
 * fonction spécifique au projet
 * Chargement des templates includes et fonction d'affichage en front
 *
 * - Mail & Comments
 * - Menu
 * - Templates
 * - Aside
 * - Medias & Images size & SVG
 * - Length
 * - Phone
 * - Pagination
 *
 */

/* Inialisation des paramètres */
$caParams = get_option('ca-param-option');

/*=======================================
=            Mail & comments            =
=======================================*/

/*----------  Expé mail  ----------*/

/* Changement du nom de l'expéditeur des mail */
function ca_sender_email( $original_email_address ) {
	$caParams = get_option('ca-param-option');
	$adminEmail = get_option( 'admin_email' );
	
	if( isset($caParams['ca-coord-phone']) && $caParams['ca-coord-phone'] != '' ) {
    	return $caParams['ca-coord-mail'];
	} else {
		return $adminEmail;
	}
    // return 'noreply@monsite.com';
}
add_filter( 'wp_mail_from', 'ca_sender_email' );

function ca_sender_name( $original_email_from ) {
	$caParams = get_option('ca-param-option');
	$siteName = get_option( 'blogname' );

	if( isset($caParams['ca-coord-phone']) && $caParams['ca-coord-phone'] != '' ) {
    	return $caParams['ca-coord-name'];
    } else {
		return $siteName;
    }
    // return 'Monsite';
}
add_filter( 'wp_mail_from_name', 'ca_sender_name' );

// test d'envoi de Mail
// wp_mail( 'contact@courantalternatif.fr', 'test mail', 'Mail afin de tester si les expéditions de mail via wordpress fonctionnent correctement' );

/*=====  End of Mail & comments  ======*/

/*==========================
=            WIDGET            =
==========================*/

/*----------  Initialisation et Création des zones de widget  ----------*/
add_action( 'widgets_init', 'add_widget' );

	function add_widget() {
		register_sidebar( 
			array(
				'name' => 'Zone de widget footer',
				'id' => 'footer-widget',
				'before_widget' => '<div class="footer-widget">',
				'after_widget' => '</div>',
				'before_title' => '<p class="footer-widget-title">',
				'after_title' => '</p>'
			) 
		);
	}
   
   
   


/*=====  FIN WIDGET  =====*/

/*==========================================
=                    MENU                  =
==========================================*/

/*----------  Initialisation et Création des zones de menu  ----------*/
add_action( 'init', 'add_menu' );

	function add_menu() {
		register_nav_menus(
			array(
				'primary' 		=> 'Principal',
				// 'secondary' 	=> 'Secondaire',
				// 'footer' 		=> 'Pied de page',
				// 'footer2' 		=> 'Pied de page 2',
				'legacy' 		=> 'Mention Légale'
				// 'name'		=> 'Name'
			)
		);
	}

/*----------  Fonction de positionnement avec appel du template  ----------*/
// Les Walkers sont dans le template

// Affiche du menu Primary
function display_primary_nav()
{
    include (get_template_directory().'/_templates/navigation/primary-nav.php');
}

// Affiche du menu Secondary
// function display_secondary_nav()
// {
//     include (get_template_directory().'/_templates/navigation/secondary-nav.php');
// }

// Affiche du menu Footer
// function display_footer_nav_1()
// {
//     include (get_template_directory().'/_templates/navigation/footer-nav.php');
// }
// function display_footer_nav_2()
// {
//     include (get_template_directory().'/_templates/navigation/footer-nav-2.php');
// }

// Affiche du menu Legacy
function display_legacy_nav()
{
    include (get_template_directory().'/_templates/navigation/legacy-nav.php');
}

// Affiche du menu Name
// function display_name_nav()
// {
//     include (get_template_directory().'/_templates/navigation/_ex_name-nav.php');
// }

/*=====  End of MENU  ======*/



/*=================================
=            Templates            =
=================================*/

include (get_template_directory().'/_templates/search-form.php');

/*----------  Social link  ----------*/

include (get_template_directory().'/_templates/social-link.php');

/*----------  Social Sharing  ----------*/

include (get_template_directory().'/_templates/social-sharing.php');

/*----------  Gallery  ----------*/

// Gallery classic : Soit l'un
// include (get_template_directory().'/_templates/gallery.php');

// Gallery slider : Soit l'autre
include (get_template_directory().'/_templates/gallery-carousel.php');

/*=====  End of Templates  ======*/


/*=============================
=            Aside            =
=============================*/

// add_action( 'widgets_init', function() {
// 	register_sidebar( array(
// 		'name'          => 'Barre latérale',
// 		'id'            => 'aside',
// 		'description'   => 'Ajouter les widgets ici pour les afficher dans la Barre latérale',
// 		'before_widget' => '<aside class="aside %1$s %2$s" role="complementary">',
// 		'after_widget'  => '</aside>',
// 		'before_title'  => '<p class="box-title">',
// 		'after_title'   => '</p>',
// 	) );
// });

/*=====  End of Aside  ======*/


/*==================================================
=            Medias & Images size & SVG            =
==================================================*/

/*----------  Taille des images  ----------*/

/* Création des différentes tailles de visuel */

// add_action( 'after_setup_theme', 'ca_img' );
	
	function ca_img() {
		add_image_size( 'slider', 1110, 625, true );
	}

/*----------  Sprite SVG  ----------*/

// Le sprite php se trouve dans le dossier /images/
include (get_template_directory().'/images/sprite.php');

function ca_svg( $index, $color = false, $class = false, $hidden = true ) {
	global $sprite;
	$svg = $sprite[$index];

	// no print
	$svg = str_replace('<svg', '<svg class="no-print"', $svg);
	// couleur
	if ( $color ) {	$svg = str_replace('fill="#fff"', 'fill="'.$color.'"', $svg); }
	// aria hidden
	if ( $hidden ) { $svg = str_replace('<svg', '<svg aria-hidden="true"', $svg); }
	// css
	if ( $class ) { $svg = str_replace('class="no-print"', 'class="no-print '.$class.'"', $svg); }
	return $svg;
}

/*=====  End of Medias & Images size & SVG  ======*/


/*==============================
=            Length            =
==============================*/

// Fonctions permettant la remonté des title et excerpt avec une longueur précise

/*----------  Title  ----------*/
// Longueur en caractères
function ca_title($limit) {
    return mb_strimwidth(get_the_title(), 0, $limit, '...');
}

/*----------  Excerpt  ----------*/
// Longueur en mots
function ca_excerpt($limit) {
    return wp_trim_words(get_the_excerpt(), $limit);
}

/*=====  End of Length  ======*/


/*=============================
=            Phone            =
=============================*/

function ca_phone($tel) {

	$tel = str_replace( ' ', '', $tel );
	$tel = '+33'.substr( $tel, 1, strlen($tel) );

	return $tel;

}

/*=====  End of Phone  ======*/


/*==================================
=            Pagination            =
==================================*/

/*----------  Pager  ----------*/

function ca_pager($prevTxt = '<span aria-hidden="true">&laquo;</span><span class="sr-only">Précédent</span>', $nextTxt = '<span aria-hidden="true">&raquo;</span><span class="sr-only">Suivant</span>') {
   	// globale WP
   	global $wp_query;
    // page courante
    $wp_query->query_vars['paged'] > 1 ? $current = $wp_query->query_vars['paged'] : $current = 1;
    // configuration
    // cf. https://codex.wordpress.org/Function_Reference/paginate_links
    $pagination = array(
        'total' 				=> $wp_query->max_num_pages,						// nombre total de page
        'current' 				=> $current,										// index de la page courante
        'mid_size'				=> 0,												// nombre de liens autour de la page courante
        'prev_text' 			=> $prevTxt,										// contenu lien "page précédente"
        'next_text' 			=> $nextTxt,										// contenu lien "page suivante"
        'type' 					=> 'array',											// type de rendu (liste non ordonnée)
        'before_page_number' 	=> '<span class="sr-only">Page </span>'		// insertion avant le numéro de page
	);
    // tableau contenant chaque élément (lien et '...')
    $pagesList = paginate_links($pagination);
    // classes pour les liens
    $pagerOldClass = array('page-numbers', 'prev', 'current', 'dots', 'next');
    $pagerNewClass = array('page-link', 'page-link-prev', 'active', 'page-dots disabled', 'page-link-next');
	// affichage
	if ( isset($pagesList) && count($pagesList) > 0 ) {
	    $pager = '<nav aria-label="Navigation de page"><ul class="pagination justify-content-center">';
	    foreach ($pagesList as $page) {
	    	$page = str_replace($pagerOldClass, $pagerNewClass, $page);
	    	$pager .= '<li class="page-item">'.$page.'</li>';
	    }
	    $pager .= '</ul></nav>';
	    echo $pager;
	}
}

/*----------  Pagination  ----------*/

function ca_pagination($prevTxt = '<span aria-hidden="true">&laquo;</span> <span class="sr-only">Article </span>Précédent', $nextTxt = '<span class="sr-only">Article </span>Suivant <span aria-hidden="true">&raquo;</span>') {
	$pagination = '<nav aria-label="Navigation de page"><ul class="pagination single-pagination justify-content-center">';
	// construction du lien précédent
	$prevObject = get_previous_post();
	if( $prevObject != '' ) {
		$prevTitle 		= $prevObject->post_title;
		$prevUrl 		= get_permalink($prevObject->ID);
		$prevLink 		= '<a href="'.$prevUrl.'" class="page-link page-link-prev" title="'.$prevTitle.'"><span>'.$prevTxt.'</span></a>';
		$pagination 	.= '<li class="page-item">'.$prevLink.'</li>';
	}
	// retour liste
	$pagination .= '<li class="page-item"><a href="../" class="page-link page-link-back" title="Retour à la liste"><span>Retour</span></a></li>';
	// construction du lien suivant
	$nextObject = get_next_post();
	if( $nextObject != '' ) {
		$nextTitle 		= $nextObject->post_title;
		$nextUrl 		= get_permalink($nextObject->ID);
		$nextLink 		= '<a href="'.$nextUrl.'" class="page-link page-link-next" title="'.$nextTitle.'"><span>'.$nextTxt.'</span></a>';
		$pagination 	.= '<li class="page-item">'.$nextLink.'</li>';
	}
	$pagination .= '</ul>';
	echo $pagination;
}

// Ajout de class sur la navigation de la recherche
add_filter('next_posts_link_attributes', 'ca_pagination_search');
add_filter('previous_posts_link_attributes', 'ca_pagination_search');

function ca_pagination_search() {
    return 'class="page-link"';
}

/*=====  End of Pagination  ======*/


?>