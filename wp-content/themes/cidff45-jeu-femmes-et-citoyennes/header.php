<!doctype html>
<html <?php language_attributes(); ?> class="no-js">

<head>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="application-name" content="<?php echo $title; ?>"/>

	<!-- Title -->
	<title><?php wp_title(''); ?></title>

	<?php 
		$title = get_bloginfo();
		$caParams = get_option('ca-param-option');
		$tpl_url = get_bloginfo('stylesheet_directory');
		$tpl_path = \wp_make_link_relative( get_bloginfo('stylesheet_directory') );
		
	?>

	<!-- Color navigator & apps -->
	<meta name="theme-color" content="#af327d">
	<meta name="msapplication-TileColor" content="#af327d" />

<!-- Favicon & Icon apps -->
	<!-- Google favicon search results -->
	<link rel="shortcut icon" href="<?php echo $tpl_path?>/images/favicon/favicon.ico">

	<link rel="icon" type="image/x-icon" href="<?php echo $tpl_path ?>/images/favicon/favicon.ico" />
	<link rel="icon" type="image/png" href="<?php echo $tpl_path ?>/images/favicon/favicon-196x196.png" sizes="196x196" />
	<link rel="icon" type="image/png" href="<?php echo $tpl_path ?>/images/favicon/favicon-128.png" sizes="128x128" />
	<link rel="icon" type="image/png" href="<?php echo $tpl_path ?>/images/favicon/favicon-96x96.png" sizes="96x96" />
	<link rel="icon" type="image/png" href="<?php echo $tpl_path ?>/images/favicon/favicon-32x32.png" sizes="32x32" />
	<link rel="icon" type="image/png" href="<?php echo $tpl_path ?>/images/favicon/favicon-16x16.png" sizes="16x16" />
	
	<link rel="apple-touch-icon" sizes="57x57" href="<?php echo $tpl_path ?>/images/favicon/apple-touch-icon-57x57.png" />
	<link rel="apple-touch-icon" sizes="114x114" href="<?php echo $tpl_path ?>/images/favicon/apple-touch-icon-114x114.png" />
	<link rel="apple-touch-icon" sizes="72x72" href="<?php echo $tpl_path ?>/images/favicon/apple-touch-icon-72x72.png" />
	<link rel="apple-touch-icon" sizes="144x144" href="<?php echo $tpl_path ?>/images/favicon/apple-touch-icon-144x144.png" />
	<link rel="apple-touch-icon" sizes="60x60" href="<?php echo $tpl_path ?>/images/favicon/apple-touch-icon-60x60.png" />
	<link rel="apple-touch-icon" sizes="120x120" href="<?php echo $tpl_path ?>/images/favicon/apple-touch-icon-120x120.png" />
	<link rel="apple-touch-icon" sizes="76x76" href="<?php echo $tpl_path ?>/images/favicon/apple-touch-icon-76x76.png" />
	<link rel="apple-touch-icon" sizes="152x152" href="<?php echo $tpl_path ?>/images/favicon/apple-touch-icon-152x152.png" />

	<meta name="msapplication-TileImage" content="<?php echo $tpl_path ?>/images/favicon/mstile-144x144.png" />
	<meta name="msapplication-square70x70logo" content="<?php echo $tpl_path ?>/images/favicon/mstile-70x70.png" />
	<meta name="msapplication-square150x150logo" content="<?php echo $tpl_path ?>/images/favicon/mstile-150x150.png" />
	<meta name="msapplication-wide310x150logo" content="<?php echo $tpl_path ?>/images/favicon/mstile-310x150.png" />
	<meta name="msapplication-square310x310logo" content="<?php echo $tpl_path ?>/images/favicon/mstile-310x310.png" />

<!-- Open Graph data -->
<meta property="og:url" content="<?= site_url() ?>" />
<meta property="og:title" content="Femmes et Citoyennes, le jeu" />
<meta property="og:site_name" content="Femmes et Citoyennes" />
<meta property="og:description" content="Incollables sur le droit et la citoyenneté en France ! Une façon ludique de s’informer, réfléchir et échanger."/>
<meta property="og:type" content="website" />
<meta property="og:locale" content="fr_FR" />
<meta property="og:image" content="<?php echo $tpl_url ?>/images/og-image.png" />
<meta property="og:image:width" content="512">
<meta property="og:image:height" content="234">
<meta property="og:image:type" content="image/png">
<!-- Twitter Card data -->
<meta name="twitter:card" content="summary_large_image">
<meta name="twitter:site" content="Femmes et Citoyennes">
<meta name="twitter:title" content="Femmes et Citoyennes, le jeu">
<meta name="twitter:url" content="<?= site_url() ?>">
<meta name="twitter:description" content="Incollables sur le droit et la citoyenneté en France ! Une façon ludique de s’informer, réfléchir et échanger.">
<!-- summary card large image >= 280x150px -->
<meta name="twitter:image" content="<?php echo $tpl_url ?>/images/og-image.png">

<!-- Semantic data -->
<script type="application/ld+json">
[
{
  "@context": "https://schema.org",
  "@type": "Article",
  "headline": "Quiz sur le droit et la citoyenneté en France: de la famille, des femmes, de la santé, du travail",
  "image": ["<?php echo $tpl_url ?>/images/og-image.png"],
  "publisher": {
	"@type": "Organization",
	"name": "CIDFF Centre Val-de-Loire (Centre national d'information des droits des femmes et de la famille)"
  }
},
{
 "@context": "https://schema.org",
 "@type": "Course",
 "name": "Jeu Femmes et Citoyennes",
 "description": "Quiz sur le droit et la citoyenneté en France: droit de la famille, droit des femmes, droit de la santé, droit du travail",
 "image": ["<?php echo $tpl_url ?>/images/og-image.png"],
 "provider": {
  "@type": "Organization",
  "name": "CIDFF Centre Val-de-Loire (Centre national d'information des droits des femmes et de la famille)"
 }
}
]
</script>

	<!-- Wordpress head & modules -->
	<?php wp_head(); ?>

	<!-- link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,400i,700" rel="stylesheet" -->
	<style>
		@font-face {
			font-family: 'Open sans'; src: url('<?php echo $tpl_path ?>/fonts/OpenSans-Light.woff') format('woff') ; font-weight: 300; font-style: normal;
		}
		@font-face {
			font-family: 'Open sans'; src: url('<?php echo $tpl_path ?>/fonts/OpenSans-Regular.woff') format('woff') ; font-weight: 400; font-style: normal;
		}
		@font-face {
			font-family: 'Open sans'; src: url('<?php echo $tpl_path ?>/fonts/OpenSans-Italic.woff') format('woff') ; font-weight: 400; font-style: italic;
		}
		@font-face {
			font-family: 'Open sans'; src: url('<?php echo $tpl_path ?>/fonts/OpenSans-Bold.woff') format('woff') ; font-weight: 700; font-style: normal;
		}
	</style>

	<?php /*
	<?php if ( function_exists('ca_analytics') ) {
		ca_analytics();
	} ?>
	*/ ?>

</head>
<body <?php body_class(); ?>>

	<ul class="skip-nav no-print">
		<?php if(!is_home()) { ?><li><a href="<?= get_home_url(); ?>" title="Retour à l'accueil">Accueil</a></li><?php } ?>
		<li><a href="#primary-nav" title="Accès direct au menu">Menu Principal</a></li>
		<li><a href="#main" title="Accès direct au contenu">Contenu</a></li>
	</ul>

	<!--[if lte IE 9]><div class="alert-ie"><p class="mt0"><strong>Attention ! </strong> Votre navigateur (Internet Explorer 9 ou inférieur) présente de sérieuses lacunes en terme de sécurité et de performances, dues à son obsolescence. En conséquence, ce site sera consultable mais de manière moins optimale qu'avec un navigateur récent&nbsp;: <a href="http://www.browserforthebetter.com/download.html">Internet&nbsp;Explorer&nbsp;10 et +</a>, <a href="http://www.mozilla.org/fr/firefox/new/">Firefox</a>, <a href="https://www.google.com/intl/fr/chrome/browser/">Chrome</a>, <a href="http://www.apple.com/fr/safari/">Safari</a>,...</p></div><![endif]-->
	<!-- <header class="header header-fixed header-fixed-resize" data-fixedstart="150" data-fixedsupport="mobile"> -->

	<header class="header">
		<div class="header-container container-fluid container-xl">
			<div class="navbar navbar-dark navbar-expand-md">
				<a href="<?= get_home_url(); ?>" class="navbar-brand" title="<?php echo $title; ?>">
					<img src="<?php echo $tpl_path ?>/images/logo.svg" alt="<?php echo $title; ?>" />
				</a>
				<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#header-nav" aria-controls="header-nav" aria-expanded="false" aria-label="Ouvrir/Fermer le menu">
					<span class="navbar-toggler-icon"></span>
				</button>

				<div class="collapse navbar-collapse" id="header-nav">
					<button type="button" class="close navbar-close" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<div>
						<?php 
							/*----------  Navigation Principale  ----------*/
							display_primary_nav();
						?>	
					</div>
				</div>
			
			</div>
					
		</div>
	</header>
