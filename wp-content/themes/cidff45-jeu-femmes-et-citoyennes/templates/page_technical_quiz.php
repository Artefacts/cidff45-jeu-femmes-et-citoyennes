<?php
/**
 * Template Name: Technical Quiz Page
 *
 * The template for playing on Quiz.
 *
 * @package Cidff
 * @subpackage Quiz
 * @since 1.0.0
 */

/**
 * TODO : how to remove header and footer but get enqueued scripts and styles ?
 */

$title = get_bloginfo();

?><!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>" />
	<link rel="profile" href="https://gmpg.org/xfn/11" />

	<!-- Title -->
	<title><?php wp_title(''); ?></title>

	<!-- Applications Name -->
	<meta name="application-name" content="<?php echo $title; ?>"/>

	
	<!-- Responsive -->
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />

	<!-- Color navigator & apps -->
	<meta name="theme-color" content="#af327d">
	<meta name="msapplication-TileColor" content="#af327d" />

	<!-- Favicon & Icon apps -->
	<link rel="icon" type="image/x-icon" href="<?php bloginfo('template_directory'); ?>/images/favicon/favicon.ico" />
	<link rel="icon" type="image/png" href="<?php bloginfo('template_directory'); ?>/images/favicon/favicon-196x196.png" sizes="196x196" />
	<link rel="icon" type="image/png" href="<?php bloginfo('template_directory'); ?>/images/favicon/favicon-128.png" sizes="128x128" />
	<link rel="icon" type="image/png" href="<?php bloginfo('template_directory'); ?>/images/favicon/favicon-96x96.png" sizes="96x96" />
	<link rel="icon" type="image/png" href="<?php bloginfo('template_directory'); ?>/images/favicon/favicon-32x32.png" sizes="32x32" />
	<link rel="icon" type="image/png" href="<?php bloginfo('template_directory'); ?>/images/favicon/favicon-16x16.png" sizes="16x16" />
	
	<link rel="apple-touch-icon" sizes="57x57" href="<?php bloginfo('template_directory'); ?>/images/favicon/apple-touch-icon-57x57.png" />
	<link rel="apple-touch-icon" sizes="114x114" href="<?php bloginfo('template_directory'); ?>/images/favicon/apple-touch-icon-114x114.png" />
	<link rel="apple-touch-icon" sizes="72x72" href="<?php bloginfo('template_directory'); ?>/images/favicon/apple-touch-icon-72x72.png" />
	<link rel="apple-touch-icon" sizes="144x144" href="<?php bloginfo('template_directory'); ?>/images/favicon/apple-touch-icon-144x144.png" />
	<link rel="apple-touch-icon" sizes="60x60" href="<?php bloginfo('template_directory'); ?>/images/favicon/apple-touch-icon-60x60.png" />
	<link rel="apple-touch-icon" sizes="120x120" href="<?php bloginfo('template_directory'); ?>/images/favicon/apple-touch-icon-120x120.png" />
	<link rel="apple-touch-icon" sizes="76x76" href="<?php bloginfo('template_directory'); ?>/images/favicon/apple-touch-icon-76x76.png" />
	<link rel="apple-touch-icon" sizes="152x152" href="<?php bloginfo('template_directory'); ?>/images/favicon/apple-touch-icon-152x152.png" />

	<meta name="msapplication-TileImage" content="<?php bloginfo('template_directory'); ?>/images/favicon/mstile-144x144.png" />
	<meta name="msapplication-square70x70logo" content="<?php bloginfo('template_directory'); ?>/images/favicon/mstile-70x70.png" />
	<meta name="msapplication-square150x150logo" content="<?php bloginfo('template_directory'); ?>/images/favicon/mstile-150x150.png" />
	<meta name="msapplication-wide310x150logo" content="<?php bloginfo('template_directory'); ?>/images/favicon/mstile-310x150.png" />
	<meta name="msapplication-square310x310logo" content="<?php bloginfo('template_directory'); ?>/images/favicon/mstile-310x310.png" />

	<!-- Wordpress head & modules -->
	<?php wp_head(); ?>

	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,400i,700" rel="stylesheet">
	
	<?php if ( function_exists('ca_analytics') ) {
		ca_analytics();
	} ?>
</head>

<body <?php body_class(); ?>>
    <?php wp_body_open(); ?>
    <div id="page" class="site">
    	<div id="content" class="site-content">

            <div id="cidff-quiz">

				<div class="modal fade" id="out-modal" tabindex="-1" role="dialog" aria-labelledby="out-modal" aria-hidden="true">
					<div class="modal-dialog" role="document">
						<div class="modal-content">
							<div class="modal-body">
								<p>Voulez vous quitter le jeu</p>
								<p>Votre progression sera perdue</p>
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-secondary" data-dismiss="modal">Continuer le jeu</button>
								<a href="<?= get_home_url(); ?>" class="btn btn-primary">Quitter</a>
							</div>
						</div>
					</div>
				</div>
				<div class="modal fade" id="quiz-presets-modal" tabindex="-1" role="dialog" aria-labelledby="quiz-presets-modal" aria-hidden="true">
					<div class="modal-dialog" role="document">
						<div class="modal-content">
							<div class="modal-body">
								<p>Configurations de Quiz:</p>
								<ul class="presets">
								</ul>
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-secondary" data-dismiss="modal">Annuler</button>
								<button type="button" class="btn btn-primary presets-done">Jouer</button>
							</div>
						</div>
					</div>
				</div>

				<a href="<?= get_home_url(); ?>" class="quizz-out" data-toggle="modal" data-target="#out-modal">
					<span class="quizz-out-icon"><?= ca_svg('back','#000'); ?></span>
					<span class="quizz-out-text">Retour au site</span>
				</a>

				<div class="quizz-logo container-quizz">
					<img src="<?php bloginfo('template_directory'); ?>/images/logo.svg" alt="<?php echo $title; ?>" />
				</div>

				<div class="quiz-steps"><div>
					<div class="steps-percent">
						<div class="step past"></div>
						<div class="step past"></div>
						<div class="step step-first"></div>
						<div class="step step-next"></div>
						<div class="step step-next"></div>
						<div class="step step-next"></div>
					</div>
					<div class="steps-counter">
						<span class="steps-counter-number current">00</span> / <span class="steps-counter-number total">00</span>
					</div>
                </div></div>

                <div class="question container-quizz">
					<p class="theme">être femme</p>
                    <h2 class="text quizz-title">Question ?</h2>
					<p class="help"></p>
                </div>

				<div class="responses-content container-quizz">
					<div class="answer" >
						<p>Réponse</p>
					</div>

					<div class="responses">
						<ul>
							<li>réponse 1</li>
							<li>réponse 2</li>
							<li>réponse 3</li>
						</ul>
					</div>
				</div>

                <div class="finish container-quizz">
					<a href="<?= get_home_url(); ?>" class="finish-logo" title="<?php echo $title; ?>">
						<img src="<?php bloginfo('template_directory'); ?>/images/logo.svg" alt="<?php echo $title; ?>" />
					</a>

                    <h3 class="text">Terminé !</h3>

					<div class="score">
						<div class="score-plural">
							<p>Vous avez <span class="good-responses-count">0</span> bonnes réponses sur <span class="questions-count">0</span> questions.</p>
						</div>
						<div class="score-singular">
							<p>Vous avez <span class="good-responses-count">0</span> bonne réponse sur <span class="questions-count">0</span> questions.</p>
						</div>
						<div class="text-end text-center">

							<a href="/le-quiz" class="btn btn-success btn-lg">Nouvelle partie</a>

							<a href="<?= get_home_url(); ?>" class="btn btn-primary">Retour sur le site</a>

						</div>
					</div>
                </div>

                

                <div class="navigation container-quizz"><div>
                    <button class="btn btn-color2 btn-play next-btn next-btn-validation btn-block btn-lg" disabled="disabled" >réponse</button>
                    <button class="btn btn-color2 btn-play next-btn next-btn-question btn-block btn-lg" disabled="disabled" >suivant</button>
                    <button class="btn btn-color2 btn-play next-btn next-btn-last btn-block btn-lg" disabled="disabled" >voir score</button>
                </div></div>

                <div class="settings container-quizz">
                    <form id="settings">

						<h2 class="quizz-title">Choisir une thématique <span class="small">(ou plusieurs)</span></h2>
						<ul class="categories like-button list-unstyled">
							<li>
								<input type="checkbox" name="categories[]" id="settings-category-01"/>
								<label for="settings-category-01">Catégorie 1</label>
							</li>
						</ul>

						<div class="settings-number">
							<strong>Nombre de questions par catégorie </strong> 
							<ul class="number like-button list-unstyled">
								<li>
									<input type="radio" name="number[]" id="settings-number-01" value="1" checked/>
									<label for="settings-number-01">1</label>
								</li>
								<li>
									<input type="radio" name="number[]" id="settings-number-02" value="3" />
									<label for="settings-number-02">3</label>
								</li>
								<li>
									<input type="radio" name="number[]" id="settings-number-03" value="5" />
									<label for="settings-number-03">5</label>
								</li>
							</ul>
						</div>


						<h2 class="quizz-title">Choisir un parcours <span class="small">(ou plusieurs)</span></h2>
						<ul class="levels like-button list-unstyled">
							<li>
								<input type="checkbox" name="levels[]" id="settings-level-01"/>
								<label for="settings-level-01">Level 1</label>
							</li>
						</ul>
						
						<div class="settings-submit text-center">
							<button type="button" class="btn btn-color2 btn-play btn-start btn-lg btn-block" >Jouer</button>
						</div>

						<div class="footer-quizz">
							<div class="footer-quizz-info">
								<span class="quiz-presets-button" ></span>
								<p>Nombre de questions pour la partie: </p>
							</div>
							<div class="footer-quizz-counter">
								<span class="play_questions_count"></span>
							</div>
						</div>
                    
                    
                    </form>
                </div>
            </div>

    	</div><!-- /#content -->
	</div><!-- /#page -->

<?php wp_footer(); ?>

<script type="text/javascript">

(function($, document)
{
	new Quiz( $, cidff_quiz, {
		screen_id: 'cidff-quiz'
	} ).settings();

})(jQuery,document);

</script>
</body>
</html>
