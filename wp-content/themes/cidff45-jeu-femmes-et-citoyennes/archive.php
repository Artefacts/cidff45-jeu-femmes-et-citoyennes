<?php
/**
 * Dédié aux questions, rien d'autre à afficher.
 */
?>
<?php get_header(); ?>

<main id="main" class="main archive">
	<div class="main-inner container">

		<?php
			the_archive_title( '<h1>', '</h1>' );
			the_archive_description( '<div class="wp-wysiwyg page-lead">', '</div>' );
		?>
		<?php if( category_description() ) : ?>
			<div class="wp-wysiwyg page-lead">
				<?php echo category_description(); ?>
			</div>
		<?php endif; ?>

		<?php if ( have_posts() ) : ?>
			<div class="archive-container mb-5"><div class="row">
			<?php while ( have_posts() ) :
				the_post(); ?>
				<div class="col-md-6 col-lg-4">
			<?php
			$title = get_post_meta( get_the_id(), 'question',true) ;
			?>

<article class="resume card mb-5 ml-auto mr-auto resume-no-thumbnail">
	<div class="resume-content card-body">
		<h2 class="resume-title card-title"><a href="<?php the_permalink(); ?>" title="Voir la réponse de <?php echo $title ?>"><?php echo $title ?></a></h2>
		<p class="resume-infos card-subtitle mb-2 text-muted small">
			<strong class="sr-only">Date :</strong>
			<time class="pub-date resume-date" datetime="<?php echo get_the_modified_date('c'); ?>"><?php echo get_the_modified_date(); ?></time>
			<?php if( ! is_tax() ){ ?>
				<?php the_terms(get_the_id(), 'cidff_category'); ?>
			<?php } ?>
		</p>
		<div class="resume-more">
			<a href="/le-quiz" title="Jouer au Quiz" class="resume-btn btn btn-color2 btn-block">Jouer au Quiz</a>
		</div>
	</div>
</article>

				</div>
			<?php endwhile; ?>
			</div></div>
		<?php else : ?>
			<p class="query-no-result">Aucun résultat.</p>
		<?php endif; ?>

		<?php ca_pager('<span aria-hidden="true">&laquo;</span><span class="sr-only">Précédent</span>','<span aria-hidden="true">&raquo;</span><span class="sr-only">Suivant</span>'); ?>
	
	</div>
</main>

<p class="text-center"><a href="/le-quiz" class="btn btn-color2 btn-lg btn-play">Jouer le Quiz</a></p>

<?php get_footer(); ?>