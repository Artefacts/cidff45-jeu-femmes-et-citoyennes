<?php
/**
 * Dédié aux questions, rien d'autre à afficher.
 */
?>
<?php get_header(); ?>

<main id="main" class="main post">
	<div class="main-inner container container-full">

		<?php if ( have_posts() ) : while ( have_posts() )
			: the_post();

		$question = get_post_meta( get_the_id(), 'question',true);
		$answer = get_post_meta( get_the_id(), 'answer',true);
		$choices = get_post_meta( get_the_id(), 'choices',true);
		$choices_count = count($choices) > 1 ? count($choices) : 0 ;
		$good_choices = [] ;
		foreach( $choices as $c )
		{
			if( isset($c['good']) )
				$good_choices[] = $c ;
		}
		$good_choices_count = count($good_choices);
		?>

<script type="application/ld+json">
    {
      "@context": "https://schema.org",
      "@type": "QAPage",
      "mainEntity": {
        "@type": "Question",
        "name": <?php echo json_encode($question) ?>,
        "answerCount": <?php echo ($good_choices_count == 0 ? 1 : $choices_count) ?>,
        "dateCreated": "<?php echo get_the_modified_date('c'); ?>",
        "author": {
          "@type": "Organization",
          "name": "CIDFF Centre Val-de-Loire"
		},
		"acceptedAnswer":
		[
			{
				"@type": "Answer",
				"text": <?php echo json_encode($answer) ?>
			}
			<?php for( $i=0; $i<count($good_choices); $i++ ) {
				echo ',';
			?>
			{
				"@type": "Answer",
				"text": <?php echo json_encode($good_choices[$i]['text']) ?>
			}
			<?php } ?>
		],
		"suggestedAnswer":
		[
			<?php for( $i=0; $i<$choices_count; $i++ ) {
				if( $i>0 )
					echo ',';
			?>
			{
				"@type": "Answer",
				"text": <?php echo json_encode($choices[$i]['text']) ?>
			}
			<?php } ?>
        ]
      }
    }
    </script>

			<h1 class="post-title"><?php echo $question; ?></h1>

			<p class="post-infos">
				<strong class="sr-only">Date :</strong> <time class="pub-date post-date" datetime="<?php echo get_the_modified_date('c'); ?>"><?php echo get_the_modified_date(); ?></time>
				<?php the_terms(get_the_id(), 'cidff_category'); ?>
			</p>

			<div class="responses">
				<ul class="in-question">
				<?php
					foreach( $choices as $i => $c )
					{
						if( isset($c['good']))
						{
							?>
							<li class="good"><?php echo $c['text'] ?></li>
						<?php						
						}
						else
						{
						?>
							<li><?php echo $c['text'] ?></li>
						<?php	
						}
					}
				?>
			</div>

			<div class="wp-wysiwyg">
					<?php echo get_post_meta( get_the_id(), 'answer',true); ?>
			</div>

		<?php endwhile; else : ?>
			<p class="query-no-result">Aucun résultat.</p>
		<?php endif; ?>

	<div class="resume-more" style="text-align: center; margin-top: 1em;">
		<a href="/le-quiz" title="Jouer au Quiz" class="btn btn-color2">Jouer au Quiz</a>
	</div>

	</div>
</main>

<?php get_footer(); ?>