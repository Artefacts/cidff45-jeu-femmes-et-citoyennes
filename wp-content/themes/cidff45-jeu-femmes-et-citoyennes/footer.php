<?php 
	$caParams = get_option('ca-param-option'); 
	$title = get_bloginfo();
?>

	<footer class="footer">
		<div class="footer-legacy">
			<div class="container container-full container-xl">
				<div class="d-flex align-items-sm-center justify-content-center">
					<?php /*
					<?php display_legacy_nav(); ?>
					<?php display_social_link('footer') ?>
					*/ ?>
					<div class="legacy">
						<p class="legacy-title" style="display: inline-block">
							&copy; <?= date('Y') ?> CIDFF Loiret
							<img src="<?php bloginfo('template_directory'); ?>/images/cc-by-nd-88x31.png" />
							<a href="/mentions-legales">mentions légales et confidentialité</a>
						</p>
					</div>
				</div>
			</div>
		</div>
	</footer>
	<div class="fn-nav-overlay"></div>
	<?php /*
		<?php if ( function_exists('ca_cookie') ) {
			ca_cookie();
		} ?>
	*/ ?>
	<?php wp_footer(); ?>
 
</body>
</html>