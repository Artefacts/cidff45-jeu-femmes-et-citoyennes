<article class="resume card mb-5 ml-auto mr-auto<?php if ( !has_post_thumbnail() ) { echo ' resume-no-thumbnail'; } ?>">
	<?php if ( has_post_thumbnail() ) : ?>
		<a href="<?php the_permalink(); ?>" class="resume-img" title="<?php the_title(); ?>">
			<?php the_post_thumbnail('thumbnail', ['class' => 'card-img-top']); ?>
		</a>
	<?php endif; ?>
	<div class="resume-content card-body">
		<h2 class="resume-title card-title"><a href="<?php the_permalink(); ?>" title="Lire la suite de <?php the_title(); ?>"><?php the_title(); ?></a></h2>
		<p class="resume-infos card-subtitle mb-2 text-muted small">
			<strong class="sr-only">Date :</strong> <time class="pub-date resume-date" datetime="<?php echo get_the_date('c'); ?>"><?php echo get_the_date(); ?></time> 
			<?php /* Si les catégories sont activé dans le plugin [He] Config */
			if ( !isset( $caCustomOptions['ca-post-categories'] ) ) { ?>
				| <strong>Catégories :</strong> <?php the_category( ', ' ); ?>
			<?php } ?>
			<?php the_tags( ' | <strong>Tag :</strong> ', ', ', ''); ?>
		</p>
		<p class="resume-excerpt card-text">
			<?php echo ca_excerpt(25); ?>
		</p>
		<div class="resume-more">
			<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>" class="resume-btn btn btn-color1 btn-block">Lire la suite</a>
		</div>
	</div>
</article>