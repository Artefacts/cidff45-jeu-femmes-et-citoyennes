<?php
/**
 *
 * Template : social link
 *
 * 3 variables possibles 'header' , 'footer' , 'aside' , mais d'autre sont possible dans la fonction
 *
 */

function display_social_link($location = 'classic')
{
	$caParams = get_option('ca-param-option');
	// ca_var ($caParams);
	$aGenericRegions = array('header', 'footer', 'asite');

	if( isset($caParams['ca-social-show']) && (int)$caParams['ca-social-show'] == 1 ) {

		// si la région n'est pas connu           ou  si elle est connu et paramètre est = 1
		if(!in_array($location, $aGenericRegions) || (isset($caParams['ca-social-show-' . $location]) && (int)$caParams['ca-social-show-' . $location] == 1 ))
		{

			echo '<ul class="social-nav ' . $location . '-social-nav list-unstyled">';


			if( isset($caParams['ca-social-fb']) && $caParams['ca-social-fb'] != '' ) : ?>
				<li class="social-nav-item">
					<a href="<?= $caParams['ca-social-fb'] ?>" class="social-link social-nav-link social-link-fb kpi_social-link" title="Visiter la page Facebook de <?= get_bloginfo(); ?>" target="_blank">
						<span class="sr-only kpi_social-link_name">Facebook</span>
						<?= ca_svg('facebook'); ?>
					</a>
				</li>
			<?php endif; ?>
			<?php if( isset($caParams['ca-social-tw']) && $caParams['ca-social-tw'] != '' ) : ?>
				<li class="social-nav-item">
					<a href="<?= $caParams['ca-social-tw'] ?>" class="social-link social-nav-link social-link-tw" title="Visiter la page Twitter de  <?= get_bloginfo(); ?>" target="_blank">
						<span class="sr-only">Twitter</span>
						<?= ca_svg('twitter'); ?>
					</a>
				</li>
			<?php endif; ?>
			<?php if( isset($caParams['ca-social-lk']) && $caParams['ca-social-lk'] != '' ) : ?>
				<li class="social-nav-item">
					<a href="<?= $caParams['ca-social-lk'] ?>" class="social-link social-nav-link social-link-lk" title="Visiter la page LinkedIn de  <?= get_bloginfo(); ?>" target="_blank">
						<span class="sr-only">Linkedin</span>
						<?= ca_svg('linkedin'); ?>
					</a>
				</li>
			<?php endif; ?>
			<?php if( isset($caParams['ca-social-vm']) && $caParams['ca-social-vm'] != '' ) : ?>
				<li class="social-nav-item">
					<a href="<?= $caParams['ca-social-vm'] ?>" class="social-link social-nav-link social-link-vm" title="Visiter la page Vimeo de  <?= get_bloginfo(); ?>" target="_blank">
						<span class="sr-only">Vimeo</span>
						<?= ca_svg('vimeo'); ?>
					</a>
				</li>
			<?php endif; ?>
			<?php if( isset($caParams['ca-social-yt']) && $caParams['ca-social-yt'] != '' ) : ?>
				<li class="social-nav-item">
					<a href="<?= $caParams['ca-social-yt'] ?>" class="social-link social-nav-link social-link-yt" title="Visiter la chaine Youtube de <?= get_bloginfo(); ?>" target="_blank">
						<span class="sr-only">Youtube</span>
						<?= ca_svg('youtube'); ?>
					</a>
				</li>
			<?php endif; ?>
			<?php if( isset($caParams['ca-social-pt']) && $caParams['ca-social-pt'] != '' ) : ?>
				<li class="social-nav-item">
					<a href="<?= $caParams['ca-social-pt'] ?>" class="social-link social-nav-link social-link-pt" title="Visiter la page Pinterest de <?= get_bloginfo(); ?>" target="_blank">
						<span class="sr-only">Pinterest</span>
						<?= ca_svg('pinterest'); ?>
					</a>
				</li>
			<?php endif; ?>
			<?php if( isset($caParams['ca-social-it']) && $caParams['ca-social-it'] != '' ) : ?>
				<li class="social-nav-item">
					<a href="<?= $caParams['ca-social-it'] ?>" class="social-link social-nav-link social-link-it" title="Visiter la page Instagram de <?= get_bloginfo(); ?>" target="_blank">
						<span class="sr-only">Instagram</span>
						<?= ca_svg('instagram'); ?>
					</a>
				</li>
			<?php endif; ?>
			<?php if( isset($caParams['ca-social-gg']) && $caParams['ca-social-gg'] != '' ) : ?>
				<li class="social-nav-item">
					<a href="<?= $caParams['ca-social-gg'] ?>" class="social-link social-nav-link social-link-gg" title="Visiter la page Google+ de <?= get_bloginfo(); ?>" target="_blank">
						<span class="sr-only">Google+</span>
						<?= ca_svg('googleplus'); ?>
					</a>
				</li>
			<?php endif; ?>
			<?php if( isset($caParams['ca-social-rss']) && $caParams['ca-social-rss'] != '' ) : ?>
				<li class="social-nav-item">
					<a href="<?php bloginfo('rss2_url'); ?>" class="social-link social-nav-link social-link-rss" title="Flux RSS de <?= get_bloginfo(); ?>" target="_blank">
						<span class="sr-only">Flux RSS</span>
						<?= ca_svg('rss'); ?>
					</a>
				</li>
			<?php endif;

			echo '</ul>';

		}

	}
	
}