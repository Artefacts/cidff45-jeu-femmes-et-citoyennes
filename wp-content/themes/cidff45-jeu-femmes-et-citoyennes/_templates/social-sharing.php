<?php
/**
 *
 * Template : partage sur les réseaux sociaux & Impression
 *
 * Variable possibles : 'toggle', 'toggle-vertical'
 */





function display_social_sharing($style = false)
{

	// url courante
	$currentUrlToShare = 'https://'.$_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];

	$caParams = get_option('ca-param-option');

	// Je n'affiche le template que si je l'ai activé dans les paramètres du thème
	if( isset($caParams['ca-sharing-show']) ) {

		if( $style == 'toggle' ) { ?>
			<div class="share share-toggle share-toggle-x">
				<p class="share-title">Partager</p>
				<div class="share-social">
					<button type="button" class="share-link tool-link share-toggle-btn" aria-expanded="false">
						<span class="sr-only">Partager</span>
						<?= ca_svg('social'); ?>
					</button>
		<?php } elseif( $style == 'toggle-vertical' ) { ?>
			<div class="share share-toggle share-toggle-y">
				<p class="share-title">Partager</p>
				<div class="share-social">
					<button type="button" class="btn btn-ico share-link tool-link share-toggle-btn" aria-expanded="false">
						<span class="sr-only">Partager</span>
						<?= ca_svg('social'); ?>
					</button>
		<?php } else { ?>
			<div class="share">
				<p class="share-title">Partager</p>
				<div class="share-social">
		<?php } ?>

		<ul class="share-list list-unstyled">
			<?php if( isset($caParams['ca-sharing-fb']) && $caParams['ca-sharing-fb'] != '' ) : ?>
			<li class="share-item">
				<a href="https://www.facebook.com/sharer/sharer.php?u=<?= urlencode($currentUrlToShare); ?>" target="_blank" class="share-link social-link social-link-fb kpi_share kpi_share-facebook js-target-popup" rel="nofollow" title="Partager sur Facebook (nouvelle fenêtre)">
					<span class="sr-only">Partager sur Facebook</span>
					<?= ca_svg('facebook'); ?>
				</a>
			</li>
			<?php endif; ?>
			<?php if( isset($caParams['ca-sharing-tw']) && $caParams['ca-sharing-tw'] != '' ) : ?>
			<li class="share-item">
				<a href="http://twitter.com/intent/tweet?url=<?= urlencode($currentUrlToShare); ?>" target="_blank" class="share-link social-link social-link-tw kpi_share kpi_share-twitter js-target-popup" rel="nofollow" title="Partager sur Twitter (nouvelle fenêtre)">
					<span class="sr-only">Partager sur Twitter</span>
					<?= ca_svg('twitter'); ?>	
				</a>
			</li>
			<?php endif; ?>
			<?php if( isset($caParams['ca-sharing-lk']) && $caParams['ca-sharing-lk'] != '' ) : ?>
			<li class="share-item">
				<a href="https://www.linkedin.com/shareArticle?mini=true&url=<?= urlencode($currentUrlToShare); ?>" target="_blank" class="share-link social-link social-link-lk kpi_share kpi_share-linkedin js-target-popup" rel="nofollow" title="Partager sur Linkedin (nouvelle fenêtre)">
					<span class="sr-only">Partager sur Linkedin</span>
					<?= ca_svg('linkedin'); ?>
				</a>
			</li>
			<?php endif; ?>
			<?php if( isset($caParams['ca-sharing-gg']) && $caParams['ca-sharing-gg'] != '' ) : ?>
			<li class="share-item">
				<a href="https://plus.google.com/share?url=<?= urlencode($currentUrlToShare); ?>" target="_blank" class="share-link social-link social-link-gg kpi_share kpi_share-google js-target-popup" rel="nofollow" title="Partager sur Google+ (nouvelle fenêtre)">
					<span class="sr-only">Partager sur Google+</span>
					<?= ca_svg('googleplus'); ?>	
				</a>
			</li>
			<?php endif; ?>
		</ul>
	</div>

	<?php if( isset($caParams['ca-sharing-print']) && $caParams['ca-sharing-print'] != '' ) : ?>
		<button type="button" class="share-link tool-link share-print ga-print js-print">
			<span class="sr-only">Imprimer</span>
			<?= ca_svg('print'); ?>
		</button>
	<?php endif; ?>
	
	</div>

<?php
}
}