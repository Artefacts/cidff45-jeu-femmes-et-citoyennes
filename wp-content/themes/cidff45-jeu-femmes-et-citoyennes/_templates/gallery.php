<?php
/**
 *
 * Template : gallery wysiwyg
 *
 */

add_filter( 'post_gallery', 'ca_gallery_classic', 10, 3 );

    function ca_gallery_classic( $output = '', $atts, $instance ) {

        // liste des images
        $imgIdList = explode( ',' , $atts['ids'] );

        //compteur
        $slider_counter = 0;

        // html contruction
        $return = '<div class="gallery">';

            foreach ( $imgIdList as $imgId ) {       
                
                $imgDatas = wp_prepare_attachment_for_js($imgId); // mais ne retourne pas les tailles personnalisées ! 
                $thumbnailDatas = wp_get_attachment_image_src($imgId,'thumbnail');
                $fullDatas = wp_get_attachment_image_src($imgId,'full');
                if( $slider_counter == 0 ) { 
                    $return .= '<a href="'.$fullDatas[0].'" title="'.$imgDatas['caption'].'" class="gallery-item"><figure>';
                } else {
                    $return .= '<a href="'.$fullDatas[0].'" class="gallery-item"><figure>';
                }
                $return .= '<span><img class="gallery-img" src="'.$thumbnailDatas[0].'" alt="'.$imgDatas['alt'].'" width="'.$thumbnailDatas[1].'" height="'.$thumbnailDatas[2].'"/></span>';
                $return .= ( $imgDatas['caption'] != '' ) ? '<figcaption class="gallery-caption">'.$imgDatas['caption'].'</figcaption>' : '';
                $return .= '</figure></a>';
                $slider_counter++;
            }

        $return .= '</div>';

        // affichage
        return $return;
    }
