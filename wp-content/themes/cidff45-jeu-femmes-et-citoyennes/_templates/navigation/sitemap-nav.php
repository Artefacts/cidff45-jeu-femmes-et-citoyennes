<?php
/**
 *
 * Template : plan de site
 *
 */

// Walker du Primary nav
class Sitemap_Walker extends Walker_Nav_Menu {

	/*----------  Création des UL  ----------*/
	
	function start_lvl( &$output, $depth = 0, $args = array() ) {

		// indentation du code
		$indent = ( $depth > 0  ? str_repeat( "\t", $depth ) : '' );
		// parceque l'index commence à 0
		$display_depth = ( $depth + 2);
		// classes communes et en fonction de la profondeur
		$class_names = 'header-nav-list sitemap-nav-list-l' . $display_depth . ' sitemap-nav-list list-unstyled';
		// construction du <ul>
		$output .= "\n" . $indent . '<ul class="' . $class_names . '" role="menu">' . "\n";

	} // end start_lvl()

	/*----------  Création des LI et A  ----------*/
	
	function start_el(  &$output, $item, $depth = 0, $args = array(), $id = 0 ) {

		// classe ajouté depuis l'admin
		$projectClass = $item->classes[0];
		// indentation du code
		$indent = ( $depth > 0 ? str_repeat( "\t", $depth ) : '' );
		// parceque l'index commence à 0
		$display_depth = ( $depth + 1);
		// classes communes et en fonction de la profondeur
		$depth_class_names = 'sitemap-nav-item sitemap-nav-item-l' . $display_depth;
		// classes wordpress
		$classes = empty( $item->classes ) ? array() : (array) $item->classes;
		// supprime tous les classes sauf celles précisées dans le tableau
		$clean_classes = is_array($classes) ? array_intersect($classes, array('current-menu-item','menu-item-has-children','current-menu-parent')) : '';
		// correpondance avec de nouvelles classes
		$new_classes = array(
			'current-menu-item' => 'is-active',
			'menu-item-has-children' => 'is-parent',
			'current-menu-parent' => 'is-active',
			'current-post-parent' => 'is-active'
		);
		// indique à wordpress d'utiliser les nouvelles classes
		$clean_classes = str_replace(array_keys($new_classes), $new_classes, $clean_classes);
		$class_names = esc_attr( implode(' ', apply_filters('nav_menu_css_class', array_filter($clean_classes ), $item)));

		// si c'est un item parent
		if ( in_array('is-parent', $clean_classes) ) {

			// construction du <li>
			$output .= $indent . '<li class="' . $depth_class_names . ' ' . $class_names . ' ' . $projectClass . '" role="menuitem">';

			// pas de lien mais un bouton
			$item_output = '<span class="sitemap-nav-link sitemap-span sitemap-nav-link-l'.$display_depth.'" ><span class="sitemap-nav-link-inner sitemap-nav-link-inner-l' . $display_depth . '">'.apply_filters( 'the_title', $item->title, $item->ID ).'</span></span>';

		// si ce n'est pas un parent
		} else {

			// construction du <li>
			$output .= $indent . '<li class="' . $depth_class_names . ' ' . $class_names . ' ' . $projectClass . '" role="menuitem">';

			// construction du <a>
			$attributes  = ! empty( $item->attr_title ) ? ' title="'  . esc_attr( $item->attr_title ) .'"' : '';
			$attributes .= ! empty( $item->target )     ? ' target="' . esc_attr( $item->target     ) .'"' : '';
			$attributes .= ! empty( $item->xfn )        ? ' rel="'    . esc_attr( $item->xfn        ) .'"' : '';
			$attributes .= ! empty( $item->url )        ? ' href="'   . esc_attr( $item->url        ) .'"' : '';
			$attributes .= ' class="sitemap-nav-link sitemap-nav-link-l'.$display_depth.'"';

			// création du lien
			$item_output = sprintf( '%1$s<a%2$s>%3$s%4$s%5$s%6$s%7$s</a>%8$s',
				$args->before,
				$attributes,
				$args->link_before,
				'<span class="sitemap-nav-link-inner sitemap-nav-link-inner-l' . $display_depth . '">',
				apply_filters( 'the_title', $item->title, $item->ID ),
				'</span>',
				$args->link_after,
				$args->after
			);

		}

		// indique à wordpress la nouvelle structure à utiliser
		$output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
		
	} // end start_el()

} // end Primary_Walker()