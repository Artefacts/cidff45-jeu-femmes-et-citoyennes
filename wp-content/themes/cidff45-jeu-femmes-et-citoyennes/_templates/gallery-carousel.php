<?php
/**
 *
 * Template : gallery wysiwyg VERSION Carousel
 *
 * Pensez à activer le scripts et la CSS carousel de Bootstrap
 * 
 * Passer en commentaire le add_filter de la gallery classique ou celui-ci, n'activez pas les 2
 * 
 */

add_filter( 'post_gallery', 'ca_gallery_slider', 10, 3 );

    function ca_gallery_slider( $output = '', $atts, $instance ) {

        // ID 
        $galleryId = $instance;
        
        // liste des images
        $imgIdList = explode( ',' , $atts['ids'] );

        //compteur
        $indicator_counter = 0;
        $slider_counter = 0;

        // html contruction
        $return = '<div id="gallery-'.$galleryId.'" class="wysiwyg-carousel carousel slide" data-ride="carousel">';

        // Indicator : Puces, à passer en commentaire au besoin
        $return .= '<ol class="carousel-indicators">';

            foreach ( $imgIdList as $imgId ) {       
                
                if( $indicator_counter == 0 ) { 
                    $return .= '<li data-target="#gallery-'.$galleryId.'" data-slide-to="'.$indicator_counter.'" class="active"></li>';
                } else {
                    $return .= '<li data-target="#gallery-'.$galleryId.'" data-slide-to="'.$indicator_counter.'"></li>';
                }
                
                $indicator_counter++;

            }
        
        $return .= '</ol>'; // Fin Indicator

        // Construction du Inner
        $return .= '<div class="carousel-inner">';

            foreach ( $imgIdList as $imgId ) {       
                
                $imgDatas = wp_prepare_attachment_for_js($imgId); // mais ne retourne pas les tailles personnalisées ! 
                $thumbnailDatas = wp_get_attachment_image_src($imgId,'slider');
                $fullDatas = wp_get_attachment_image_src($imgId,'full');
                if( $slider_counter == 0 ) { 
                    $return .= '<div class="carousel-item active item-'.$slider_counter.'"><a href="'.$fullDatas[0].'" title="'.$imgDatas['caption'].'">';
                } else {
                    $return .= '<div class="carousel-item item-'.$slider_counter.'"><a href="'.$fullDatas[0].'" title="'.$imgDatas['caption'].'">';
                }
                $return .= '<img class="gallery-img" src="'.$thumbnailDatas[0].'" alt="'.$imgDatas['alt'].'" width="'.$thumbnailDatas[1].'" height="'.$thumbnailDatas[2].'"/>';
                $return .= ( $imgDatas['caption'] != '' ) ? '<div class="carousel-caption d-none d-md-block"><p>'.$imgDatas['caption'].'</p></div>' : '';
                $return .= '</a></div>';
                $slider_counter++;
            }
        
        $return .= '</div>'; // Fin Inner

        // Controleurs fleches : Passer en commentaire si inutile
        $return .= '<a class="carousel-control-prev" href="#gallery-'.$galleryId.'" role="button" data-slide="prev"><span class="carousel-control-prev-icon" aria-hidden="true"></span><span class="sr-only">Précédent</span></a><a class="carousel-control-next" href="#gallery-'.$galleryId.'" role="button" data-slide="next"><span class="carousel-control-next-icon" aria-hidden="true"></span><span class="sr-only">Suivant</span></a>';
        // Fin
        $return .= '</div>';

        // affichage
        return $return;
    }
