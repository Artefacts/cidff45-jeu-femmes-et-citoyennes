<?php
/**
 *
 * Template : Search form
 *
 * Style possibles : 'open', 'toggle', 'page'
 * Location possibles : 'header' ou autres
 */

function display_search($style, $location)
{

	$heCustom = get_option('ca-custom-option');

	if( isset($heCustom['ca-search-active']) ) {

		if( $location == 'header' ) { 
			if( $style == 'open' ) { 
				echo '<div class="header-search header-search-full">';
			} elseif( $style == 'toggle' ) {
				echo '<div class="header-search">';
			} else {
				echo '<div class="header-search">';
			} ?>
		<button class="header-search-toggle" type="button" aria-label="Ouvrir/Fermer la recherche" aria-expanded="false">
			<?= ca_svg('search'); ?>
		</button>
		<?php } else {
			echo '<div class="search-form-container">';
		}
?>
	<form method="get" action="<?php bloginfo('url') ?>" role="search" class="search-form">
		<div class="search-inner">
			<label for="<?php if( $location == 'header' ) { echo 'search-field-header'; } else { echo 'search-field'; } ?>" class="sr-only">Rechercher</label>
			<input type="search" id="<?php if( $location == 'header' ) { echo 'search-field-header'; } else { echo 'search-field'; } ?>" name="s" class="search-input" onclick="this.value='';" placeholder="Termes de recherche">
			<button type="submit" class="search-button">
				<span class="sr-only">Rechercher</span>
				<?= ca_svg('search'); ?>
			</button>
		</div>
	</form>
</div>

<?php
}
}