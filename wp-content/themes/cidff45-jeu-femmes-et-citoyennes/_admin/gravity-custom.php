<?php

/**
*
* Customisation du plugin Gravity Forms
*
** Divers
** Champs non disponibles
** Accès compte éditeur
** Submit input to button
*
**/


/*==============================
=            Divers            =
==============================*/

// pas de tabindex
add_filter( 'gform_tabindex', '__return_false' );
// scroll to message de validation_message
add_filter( 'gform_confirmation_anchor', '__return_true' );


/*=====  FIN Divers  ======*/

/*==============================================
=            Champs non disponibles            =
==============================================*/

add_filter( 'gform_add_field_buttons', 'remove_fields' );

    function remove_fields( $field_groups ) {

        foreach ($field_groups as $groupKey => $group) {
            if ( $group['name'] == 'pricing_fields' || $group['name'] == 'post_fields' ) {

                unset($field_groups[$groupKey]);

            }
            if ( $group['name'] == 'advanced_fields' || $group['name'] == 'standard_fields' ) {

                foreach ($group['fields'] as $fieldKey => $field) {

                    if ( $field['data-type'] == 'section' || $field['data-type'] == 'page' || $field['data-type'] == 'time' || $field['data-type'] == 'name' || $field['data-type'] == 'address' || $field['data-type'] == 'hidden' || $field['data-type'] == 'phone' || $field['data-type'] == 'website' ) {
                        unset($field_groups[$groupKey]['fields'][$fieldKey]);

                        // Autres options possibles
                        // $field['data-type'] == 'multiselect'
                        // Tous les fields dispo ici : https://docs.gravityforms.com/form-fields/
                    }

                }

            }
        }

        return $field_groups;
    }


/*=====  FIN Champs non disponibles  ======*/


/*============================================
=            Accès compte Éditeur            =
============================================*/

add_action( 'admin_init', function() {

    // objet role
    $role = get_role( 'editor' );
    // active les menus
    // !!! valeur enregistrée en bdd, utiliser remove_cap pour inverser
    
    // $role->add_cap( 'gravityforms_edit_forms' );
    // $role->add_cap( 'gravityforms_create_form' );
    // $role->add_cap( 'gravityforms_delete_forms' );
    // $role->add_cap( 'gravityforms_view_entries' );
    // $role->add_cap( 'gravityforms_edit_entries' );
    // $role->add_cap( 'gravityforms_delete_entries' );
    // $role->add_cap( 'gravityforms_export_entries' );

    // $role->remove_cap( 'gform_full_access' );
    // $role->remove_cap( 'gravityforms_edit_forms' );
    // $role->remove_cap( 'gravityforms_create_form' );
    // $role->remove_cap( 'gravityforms_delete_forms' );
    // $role->remove_cap( 'gravityforms_view_entries' );
    // $role->remove_cap( 'gravityforms_edit_entries' );
    // $role->remove_cap( 'gravityforms_delete_entries' );
    // $role->remove_cap( 'gravityforms_export_entries' );

});

add_action('admin_menu', function() {

    remove_submenu_page("gf_edit_forms","gf_help");

}, 11);


/*=====  FIN Accès compte Éditeur  ======*/


/*==============================================
=            Input submit to button            =
==============================================*/

add_filter( 'gform_submit_button', 'submit_submit_to_button', 10, 2 );

    function submit_submit_to_button( $submit_button, $form ) {

        $domTemp = new DOMDocument;
        $domTemp->loadHTML($submit_button);
        $input = $domTemp->getElementsByTagName('input')->item(0);
        // js sur le click : champ libre dans l'article
        $postOnClick = get_post_meta( get_the_id(), 'js-clic', true );
        $onclick = str_replace('"', '\'', $input->getAttribute('onclick')) . $postOnClick;
        $onkeypress = str_replace('"', '\'', $input->getAttribute('onkeypress'));

        $submit_button = '<div class="gform_page_footer_right"><button type="submit" id="'.$input->getAttribute('id').'" class="btn btn-color1 '.$input->getAttribute('class').'" onclick="'.$onclick.'" onkeypress="'.$onkeypress.'">'.utf8_decode($input->getAttribute('value')).'</button></div>';

        return $submit_button;

    }

/*----------  Bouton de navigation si étapes  ----------*/

add_filter( 'gform_next_button', 'next_to_button', 10, 2 );

    function next_to_button( $submit_button, $form ) {

        $domTemp = new DOMDocument;
        $domTemp->loadHTML($submit_button);
        $input = $domTemp->getElementsByTagName('input')->item(0);
        // js sur le click : champ libre dans l'article
        $postOnClick = get_post_meta( get_the_id(), 'js-clic', true );
        $onclick = str_replace('"', '\'', $input->getAttribute('onclick')) . $postOnClick;
        $onkeypress = str_replace('"', '\'', $input->getAttribute('onkeypress'));

        $next_button = '<button type="submit" id="'.$input->getAttribute('id').'" class="btn-nav btn-next '.$input->getAttribute('class').'" onclick="'.$onclick.'" onkeypress="'.$onkeypress.'"><span>Étape<br/>suivante</span>'. ca_svg('arrow') .'</button>';
        // $next_button = '<button type="submit" id="'.$input->getAttribute('id').'" class="btn-nav btn-next '.$input->getAttribute('class').'" onclick="'.$onclick.'" onkeypress="'.$onkeypress.'">'.utf8_decode($input->getAttribute('value')).'</button>';

        return $next_button;

    }

add_filter( 'gform_previous_button', 'previous_to_button', 10, 2 );

    function previous_to_button( $submit_button, $form ) {

        $domTemp = new DOMDocument;
        $domTemp->loadHTML($submit_button);
        $input = $domTemp->getElementsByTagName('input')->item(0);
        // js sur le click : champ libre dans l'article
        $postOnClick = get_post_meta( get_the_id(), 'js-clic', true );
        $onclick = str_replace('"', '\'', $input->getAttribute('onclick')) . $postOnClick;
        $onkeypress = str_replace('"', '\'', $input->getAttribute('onkeypress'));

        // $previous_button = '<button type="submit" id="'.$input->getAttribute('id').'" class="btn-nav btn-prev '.$input->getAttribute('class').'" onclick="'.$onclick.'" onkeypress="'.$onkeypress.'">'.utf8_decode($input->getAttribute('value')).'</button>';
        $previous_button = '<button type="submit" id="'.$input->getAttribute('id').'" class="btn-nav btn-prev '.$input->getAttribute('class').'" onclick="'.$onclick.'" onkeypress="'.$onkeypress.'">'. ca_svg('arrow') .'<span>Étape<br/>précédente</span></button>';

        return $previous_button;

    }


/*=====  FIN Input submit to button  ======*/