<?php

/**
 *
 * Functions lié à la création de la page de parametre editeur et des champs qui vont avec
 *
 *
 */

/*----------  Valeurs par défaut  ----------*/

// si l'option n'existe pas
if ( !get_option( 'ca-param-option' ) ) {

	$optionsValues = array(
		'ca-social-show-header'		=> '1',
		'ca-social-show-footer'		=> '1',
		'ca-social-fb'				=> '#',
		'ca-social-tw'				=> '#',
		'ca-social-lk'				=> '#',

		'ca-sharing-show'			=> '1',
		'ca-sharing-fb'				=> '#',
		'ca-sharing-tw'				=> '#',
		'ca-sharing-lk'				=> '#'
	);

	add_option( 'ca-param-option', $optionsValues ,'', 'no');

}

if( class_exists('Ca_Add_Admin_Page') ) {

	$caParamPage = array(
		array(
	        'title'     => 'Liens Réseaux sociaux',
	        'id'        => 'social-link',
	        'desc'      => 'Bloc de liens vers les réseaux sociaux.',
	        'prefix'    => 'ca-social',
	        'fields'    => array(
	        	array(
	                'type'      => 'checkbox',
	                'label_for' => 'show',
	                'label'     => 'Activer les réseaux sociaux',
	                'desc'		=> '! Important : Si vous ne cochez pas ceci, les réseaux sociaux ne seront pas visible sur le site'
	            ),
	            array(
	                'type'      => 'checkbox',
	                'label_for' => 'show-header',
	                'label'     => 'Afficher dans le header'
	            ),
	            array(
	                'type'      => 'checkbox',
	                'label_for' => 'show-footer',
	                'label'     => 'Afficher dans le footer'
	            ),
	            array(
	                'type'      => 'checkbox',
	                'label_for' => 'show-aside',
	                'label'     => 'Afficher dans l\'aside'
	            ),
				array(
					'type'      => 'text',
					'label_for' => 'fb',
					'label'     => 'Facebook',
					'css'       => 'width:100%'
				),
				array(
					'type'      => 'text',
					'label_for' => 'tw',
					'label'     => 'Twitter',
					'css'       => 'width:100%'
				),
				array(
					'type'      => 'text',
					'label_for' => 'lk',
					'label'     => 'Linkedin',
					'css'       => 'width:100%'
				),
				array(
					'type'      => 'text',
					'label_for' => 'vm',
					'label'     => 'Vimeo',
					'css'       => 'width:100%'
				),
				array(
					'type'      => 'text',
					'label_for' => 'yt',
					'label'     => 'Youtube',
					'css'       => 'width:100%'
				),
				array(
					'type'      => 'text',
					'label_for' => 'pt',
					'label'     => 'Pinterest',
					'css'       => 'width:100%'
				),
				array(
					'type'      => 'text',
					'label_for' => 'it',
					'label'     => 'Instagram',
					'css'       => 'width:100%'
				),
				array(
					'type'      => 'text',
					'label_for' => 'gg',
					'label'     => 'Google plus',
					'css'       => 'width:100%'
				),
				array(
					'type'      => 'checkbox',
					'label_for' => 'rss',
					'label'     => 'Flux RSS'
				)
	        )
	    ),
	    array(
	        'title'     => 'Partage Réseaux sociaux',
	        'id'        => 'social-sharing',
	        'desc'      => 'Bloc de partage sur les réseaux sociaux',
	        'prefix'    => 'ca-sharing',
	        'fields'    => array(
	            array(
	                'type'      => 'checkbox',
	                'label_for' => 'show',
	                'label'     => 'Afficher',
	                'desc'		=> '! Important : Si vous ne cochez pas ceci, le partage sur les réseaux sociaux ne sera pas visible'
	            ),
				array(
					'type'      => 'checkbox',
					'label_for' => 'fb',
					'label'     => 'Facebook'
				),
				array(
					'type'      => 'checkbox',
					'label_for' => 'tw',
					'label'     => 'Twitter'
				),
				array(
					'type'      => 'checkbox',
					'label_for' => 'lk',
					'label'     => 'Linkedin'
				),
				array(
					'type'      => 'checkbox',
					'label_for' => 'gg',
					'label'     => 'Google plus'
				),
				array(
					'type'      => 'checkbox',
					'label_for' => 'print',
					'label'     => 'Imprimer',
					'desc'		=> 'Ajoute un bouton imprimer en plus des boutons de partage sur les réseaux sociaux'
				)
	        )
	    ),
	    array(
            'title'     => 'Coordonnées',
            'id'        => 'coord',
            'desc'      => 'Utile pour les microdonnées mais surtout l\'affichage et les données du CMS',
            'prefix'    => 'ca-coord',
            'fields'    => array(
            	array(
	                'type'      => 'checkbox',
	                'label_for' => 'show',
	                'label'     => 'Activer',
	                'desc'		=> '! Important : Si vous ne cochez pas ceci, les coordonnées ne pourront être utilisé et les microdonnées de localisation ne seront pas activées'
	            ),
                array(
                    'type'      => 'text',
                    'label_for' => 'name',
                    'label'     => 'Nom',
                    'css'       => 'width:100%',
                    'desc'		=> 'Utile pour les microdonnées mais surtout pour le nom d\'expéditeur mail',
                    'required'  => true
                ),
                array(
                    'type'      => 'text',
                    'label_for' => 'address',
                    'label'     => 'Adresse',
                    'css'       => 'width:100%',
                    'required'  => true
                ),
                array(
                    'type'      => 'text',
                    'label_for' => 'cp',
                    'label'     => 'Code postal',
                    'css'       => 'width:100%',
                    'required'  => true
                ),
                array(
                    'type'      => 'text',
                    'label_for' => 'town',
                    'label'     => 'Ville',
                    'css'       => 'width:100%',
                    'required'  => true
                ),
                array(
                    'type'      => 'text',
                    'label_for' => 'phone',
                    'label'     => 'Téléphone',
                    'css'       => 'width:100%',
                    'required'  => true
                ),
                array(
                    'type'      => 'text',
                    'label_for' => 'mail',
                    'label'     => 'E-mail',
                    'css'       => 'width:100%',
                    'desc'		=> 'Utile pour les microdonnées mais surtout pour l\'adresse mail d\expédition des mails de wordpress',
                    'required'  => true
                )
            )
        )
	);

	// création de la page
	$caParamSettings = new Ca_Add_Admin_Page( 'Paramètre du site', '', 'Paramètre', 'ca-param', $caParamPage, 'editor', '81', 'dashicons-admin-generic' );

}
