<?php
/**
 *
 * Administration customisé
 * ! important : Une grosse partie de la gestion de l'admin se passe dans le plugin Courant Alternatif, le code inséré ici peut etre ajouté en support, pensez à aller y faire un tour
 *
 *
 */

/*=======================================
=            Tiny_MCE custom            =
=======================================*/

add_filter( 'tiny_mce_before_init', 'tinymce_ca_styles' ); 

    function tinymce_ca_styles( $init_array ) {  
        
        // liste des styles
        $style_formats = array(  
            array(  
                'title' => 'Bouton (Lien)',  
                'selector' => 'a',  
                'classes' => 'wysiwyg-btn btn btn-color1',
                'wrapper' => false          
            ),
            array(  
                'title' => 'Paragraphe intro',  
                'block' => 'p',  
                'classes' => 'wysiwyg-lead lead',
                'wrapper' => false          
            ),
            array(  
                'title' => 'Petit',  
                'inline' => 'span',  
                'classes' => 'wysiwyg-small small'         
            ),
            array(  
                'title' => 'Ligne transparente (petite)',  
                'selector' => 'hr',  
                'classes' => 'wysiwyg-separator separator-small',
                'wrapper' => false          
            ),
            array(  
                'title' => 'Ligne transparente (medium)',  
                'selector' => 'hr',  
                'classes' => 'wysiwyg-separator separator-medium',
                'wrapper' => false          
            ),
            array(  
                'title' => 'Ligne transparente (grande)',  
                'selector' => 'hr',  
                'classes' => 'wysiwyg-separator separator-large',
                'wrapper' => false          
            ),
            array(  
                'title' => 'Bloc mise en avant',  
                'block' => 'div',  
                'classes' => 'wysiwyg-highlight jumbotron',
                'wrapper' => true          
            ),
            array(  
                'title' => '2 colonnes auto',  
                'block' => 'div',  
                'classes' => 'wysiwyg-column',
                'wrapper' => true          
            ),
            array(  
                'title' => '3 colonnes auto',  
                'block' => 'div',  
                'classes' => 'wysiwyg-column-3',
                'wrapper' => true          
            ),
            array(  
                'title' => 'Alerte info',  
                'block' => 'div',  
                'classes' => 'wysiwyg-alert alert alert-primary',
                'wrapper' => true          
            ),
            array(  
                'title' => 'Alerte success',  
                'block' => 'div',  
                'classes' => 'wysiwyg-alert alert alert-success',
                'wrapper' => true          
            ),
            array(  
                'title' => 'Alerte erreur',  
                'block' => 'div',  
                'classes' => 'wysiwyg-alert alert alert-danger',
                'wrapper' => true          
            ),
            array(  
                'title' => 'Alerte ! Attention',  
                'block' => 'div',  
                'classes' => 'wysiwyg-alert alert alert-warning',
                'wrapper' => true          
            ),
            array(  
                'title' => 'Badge info',  
                'inline' => 'span',  
                'classes' => 'wysiwyg-badge badge badge-primary'       
            ),
            array(  
                'title' => 'Badge Success',  
                'inline' => 'span',  
                'classes' => 'wysiwyg-badge badge badge-success'          
            ),
            array(  
                'title' => 'Badge erreur',  
                'inline' => 'span',  
                'classes' => 'wysiwyg-badge badge badge-danger'         
            ),
            array(  
                'title' => 'Badge ! Attention',  
                'inline' => 'span',
                'classes' => 'wysiwyg-badge badge badge-warning'       
            )
            // array(  
            //     'title' => 'Texte rouge',  
            //     'inline' => 'span',  
            //     'classes' => 'wysiwyg-red text-red',
            //     'wrapper' => false          
            // ),
            // array(  
            //     'title' => 'Texte bleu',  
            //     'inline' => 'span',  
            //     'classes' => 'wysiwyg-blue text-blue',
            //     'wrapper' => false          
            // ),
        );      
        // mise à jour tu tableau en paramétre (JSON encodage)
        $init_array['style_formats'] = json_encode( $style_formats );  
        return $init_array;  
      
    }

add_action( 'admin_init', 'tinymce_ca_css' );

    function tinymce_ca_css() {

        add_editor_style( '_admin/ca-wp-admin-style.css' );

    }

/*=====  End of Tiny_MCE custom  ======*/

/*===========================
=            SVG            =
===========================*/

/* Autoriser les fichiers SVG */
function svg_mime_types($mimes) {
  $mimes['svg'] = 'image/svg+xml';
  return $mimes;
}
add_filter('upload_mimes', 'svg_mime_types');

/*=====  End of SVG  ======*/

?>