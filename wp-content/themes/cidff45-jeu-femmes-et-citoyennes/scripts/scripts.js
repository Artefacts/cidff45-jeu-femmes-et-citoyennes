/*=================================================
= Variables d'environnement  =
=================================================*/
var toto = 1 + 1 ;
var $win 		= jQuery(window);
var $body		= jQuery('body');
var $header 	= jQuery('.header');
var $main 		= jQuery('.main');

var $scroll = $win.scrollTop();

/*=====  End of Variables d'environnement  ======*/


jQuery(document).ready(function() { 
	
/*==================================
=            Responsive            =
==================================*/

// fonction executée au chargement de la page et à chaque modification de largeur du navigateur
function winChange() 
{
	if (window.matchMedia('(min-width: 768px)').matches) 
	{ // Desktop
		jQuery('.navbar-toggler').attr('aria-expanded','false');
		jQuery('.navbar-collapse').removeClass('show');
		$body.removeClass('is-overlay-open');
	} 
	else 
	{ // Nomad
		
	}

} // FIN : winWidthChange()

winChange(); // START : winWidthChange()

$win.resize(winChange);

/*=====  End of Responsive  ======*/

/*==========================
=            Menu & Overlay           =
==========================*/

// Button close dans le menu
jQuery('.navbar-close, .fn-nav-overlay').click(function() {
	jQuery('.navbar-toggler').attr('aria-expanded','false');
	jQuery('.navbar-collapse').removeClass('show');
});

// Overlay
jQuery('.navbar-toggler, .navbar-close, .fn-nav-overlay').click(function() {

	if( $body.hasClass('is-overlay-open')) {
		$body.removeClass('is-overlay-open');
	} else {
		$body.addClass('is-overlay-open');
	}

});

/*=====  FIN Overlay  =====*/

/*===============================
=            WYSIWYG            =
===============================*/

jQuery('.wp-wysiwyg iframe').each(function() {
	jQuery(this).wrap('<div class="embed-responsive embed-responsive-16by9">');
});

/*=====  End of WYSIWYG  ======*/
	
}( jQuery ));