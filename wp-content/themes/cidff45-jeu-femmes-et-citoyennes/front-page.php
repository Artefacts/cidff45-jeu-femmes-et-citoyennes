<?php get_header(); ?>

<main id="main" class="main main-home">
	<div class="main-inner">

		<?php the_post(); ?>

		<section class="home-section home-intro bg-purple d-flex justify-content-center align-items-center">
			<div class="container">
				<h1 class="text-center"><?php the_title(); ?></h1>

				<p class="home-intro-desc text-center">
					<?php the_field('_home_desc'); ?>
				</p>

				<p class="text-center"><a href="/le-quiz" class="btn btn-color2 btn-lg btn-play">Jouer</a></p>

				<?php if( get_field('_home_download') ): ?>
					<p class="text-center"><a href="<?php the_field('_home_download'); ?>" class="btn btn-outline-light btn-lg btn-download">Téléchargez <br/><span class="small">et jouez chez vous</a></p>
				<?php endif; ?>

			</div>
		</section>
		<div class="home-bg">
			<section class="home-section home-content d-flex justify-content-center align-items-center">
				<div>
					<div class="container home-desc">
						<div class="row">

							<?php
								$home_img = get_field('_home_img');
								if( $home_img ) :
							?>
								<div class="col-md-6 text-center">
									
									<img src="<?= $home_img['sizes']['medium_large']; ?>" alt="<?= $home_img['alt']; ?>" />

								</div>

								<div class="col-md-6">
							<?php else : ?>

								<div class="col-md-6 offset-md-3">
							<?php endif; ?>
							
								<div class="wp-wysiwyg">
									<?php the_content(); ?>
								</div>

								<?php if( get_field('_home_download') ): ?>
									<p class="text-right">
										<a href="<?php the_field('_home_more'); ?>" class="btn btn-color3">En savoir plus</a>
										</p>
								<?php endif; ?>
							</div>
						</div>
					</div>

					<div class="container home-infos">
						<div class="row">
							<div class="col-md-6">
								<div class="home-infos-item home-infos_green">
									<div class="wp-wysiwyg">
										<h2>Qu'est ce qu'un.e citoyen.ne ?</h2>
										<p><strong>" C’est une personne vivant avec d’autres personnes dans une société donnée "</strong> <span class="small">selon la définition du Conseil de l’Europe.</span></p>
										<p>Cette définition recouvre à la fois les notions de statut et de rôle, ainsi que celles de " droits, responsabilité, devoirs ", mais aussi <strong>l’égalité, la diversité, la justice sociale.</strong></p>
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="home-infos-item home-infos_blue">
									<div class="wp-wysiwyg">
										<h2>Qu’est-ce que signifie "interculturalité" ?</h2>
										<p>L’adjectif <strong>interculturel</strong> vise " les rapports, les échanges entre cultures, entre civilisations différentes. " <span class="small">(définition Larousse)</span></p>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
			<section class="home-section home-theme d-flex justify-content-center align-items-center">
				<div class="container">

					<div class="row align-items-md-stretch">
						
						<div class="col-md-6 d-flex align-items-md-center home-theme-intro">
							<div class="wp-wysiwyg">
								<?php the_field('_home_theme'); ?>
							</div>
						</div>
						
						<div class="col-md-6 d-flex align-items-md-center home-theme-list-container">
							<ul class="row list-unstyled home-theme-list">
								<li class="col-6">
									<p class="home-theme-item cat-citoyennete text-center">
										Citoyenneté
									</p>
								</li>
								<li class="col-6">
									<p class="home-theme-item cat-travail text-center home-theme-item_dark">
										Travail
									</p>
								</li>
								<li class="col-6">
									<p class="home-theme-item cat-sante text-center">
										Santé
									</p>
								</li>
								<li class="col-6">
									<p class="home-theme-item cat-couple text-center">
										Couple
									</p>
								</li>
								<li class="col-6">
									<p class="home-theme-item cat-parents text-center">
										Parents
									</p>
								</li>
								<li class="col-6">
									<p class="home-theme-item cat-quotidien text-center home-theme-item_dark">
										Quotidien
									</p>
								</li>
								<li class="col text-center">
									<a href="/le-quiz" class="btn btn-color2 btn-lg btn-play">Jouer</a>
								</li>
							</ul>
						</div>

					</div>

				</div>
			</section>
			<section class="home-section home-logo d-flex justify-content-center align-items-center">
				<div class="container">
					<div class="row">
						<div class="col-md-4 col-xl-4 home-cidff45">
							<h2 class="home-logo-title">Projet porté par le CIDFF du Loiret</h2>
							<p><img src="/wp-content/uploads/2020/07/logo-cidff45.jpg" alt="CIDFF du Loiret"/></p>
							<p class="small">En collaboration avec les CIDFF de la région Centre Val de Loire : CIDFF du Cher, CIDFF de l’Indre, CIDFF de l’Indre-et-Loire/Eure-et-Loir, CIDFF du Loir-et-Cher.</p>
						</div>
						<div class="col-md-8 col-xl-7 offset-xl-1 home-partner">
							<h2 class="home-logo-title">Nos partenaires</h2>
							<p class="home-partner-logo">
								<img src="/wp-content/uploads/2020/07/logo-Europe-FAMI.jpg" alt="FAMI - Fonds Asile, Migration et Intégration"/>
								<img src="/wp-content/uploads/2020/07/DRDJSCS.jpg" alt="DRDJSCS Centre - Val de Loire, Loiret"/>
								<img src="/wp-content/uploads/2020/07/CGET.png" alt="CGET - Commisariat général à l'égalité des territoire"/>
								<img src="/wp-content/uploads/2020/07/centre-val-de-loire.png" alt="Région Centre - Val de Loire"/>
							</p>
						</div>
					</div>
				</div>
			</section>
		</div>
	</div>
</main>

<?php get_footer(); ?>