<?php get_header(); ?>

<main id="main" class="main page page-search">
	<div class="main-inner container container-full">

		<h1>Recherche</h1>

		<?php 
			/*----------  Moteur de recherche  ----------*/
			display_search('page','page');
		?>

		<?php if ( have_posts() ) : ?>
			
			<?php /*----------  Requete  ----------*/ ?>
			<p class="search-query alert alert-success"><?php global $wp_query; echo $wp_query->found_posts; ?> résultat(s) pour <strong>"<?php the_search_query(); ?>"</strong></p>

			<?php 
				/*----------  Résultats  ----------*/
				
				while ( have_posts() ) : the_post(); ?>
				<?php $post_type = get_post_type(get_the_ID()); ?>
				<article class="search-resume">
					<h2 class="search-resume-title">
						<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
					</h2>
					<div class="search-resume-article">
						<?php if ( has_post_thumbnail() ) : ?>
							<a href="<?php the_permalink(); ?>" title="Lire la suite de <?php the_title(); ?>" class="search-resume-img">
								<?php the_post_thumbnail( 'thumbnail' ); ?>
							</a>
						<?php endif; ?>
						<div class="search-resume-infos">
							<p class="search-resume-uri">
								<?php 
								/*----------  Post Type : en rajouté si il y en a  ----------*/
								
								if ($post_type == 'post') : ?>
									<span class="search-resume-type">Actus</span>
								<?php elseif ($post_type == 'page') : ?>
									<span class="search-resume-type">Page</span>
								<?php endif; ?>

								<a href="<?php the_permalink(); ?>"><?php the_permalink(); ?></a></p>
							<p class="search-resume-excerpt"><?= ca_excerpt(28); ?></p>
						</div>
					</div>
				</article>
		<?php endwhile; else : ?>

			<?php /*----------  Aucun résultat  ----------*/ ?>
			<div class="search-query alert alert-warning">
				<h2>Aucun résultat</h2>
				<p><?php global $wp_query; echo $wp_query->found_posts; ?> r&eacute;sultat(s) pour <strong>"<?php the_search_query(); ?>"</strong></p>
			</div>

		<?php endif; ?>
		

		<?php 
			/*----------  pager de recherche  ----------*/
			
		if( get_previous_posts_link() || get_next_posts_link() ) : ?>
		<nav aria-label="Pagination de recherche">
			<ul class="search-pager pagination justify-content-center">
				<?php if( get_previous_posts_link() ) { echo '<li class="page-item search-pager-item search-pager-prev">'.get_previous_posts_link('<span aria-hidden="true">&laquo;</span> Précédent').'</li>'; } ?>
				<?php if( get_next_posts_link() ) { echo '<li class="page-item search-pager-item search-pager-next">'.get_next_posts_link( 'Suivant <span aria-hidden="true">&raquo;</span>').'</li>'; } ?>
			</ul>
		</nav>
		<?php endif; ?>
		
	</div>
</main>

<?php get_footer(); ?>