/**
*
* Gulp pour Courant Alternatif
*
** cd in theme folder
*
** install: $ npm install
** dépendance : package.json
** Si `gulp` n'est pas installé globalement:
** $ npm install gulp
*
** Compiler le JS et les CSS :
** run: $ ./node_modules/gulp/bin/gulp.js
** dev time: ./node_modules/gulp/bin/gulp.js watch
*
**/

/*======================================
=            Initialisation            =
======================================*/

// Chargement des plugins

    const autoprefixer  = require("autoprefixer"); // préfixes css
    const banner        = require( 'gulp-banner' ); // commentaire pour le thème Wordpress
    // const browsersync = require("browser-sync").create();
    const combineMq     = require( 'gulp-combine-mq' ); // combine les medias queries
    const cssnano       = require("cssnano"); // minification css
    const jshint        = require( 'gulp-jshint' ); // analyse le javascript
    const concat        = require( 'gulp-concat' ); // concaténation de fichiers js
    const gulp          = require("gulp"); // base
    const plumber       = require("gulp-plumber"); // gestion des erreurs gulp
    const postcss       = require("gulp-postcss"); // package optimisations css
    const sass          = require("gulp-sass"); // scss to css
    const uglify        = require( 'gulp-uglify' ); // minification js
    // const watch         = require( 'gulp-watch' ); // monitoring et exécution des tâches

    // Evolution à apporter, live reload, sourcemap js et css

/*----------  Sources  ----------*/

    // css
    var css_src = ['css/style.scss'],

    // jshint sources
    jshint_src = [
        'scripts/scripts.js'
    ],

    // js sources globales + jshint sources
    js_global_src = [

        // JQuery (verifier _functions/load-assets.php pour qu'il ne soit pas chargé 2 fois)
        // 'scripts/vendor/jquery-1.11.1.min.js',

        // Modernizr
        'scripts/vendor/modernizr.js',

        // Bootstrap dependencies
        'scripts/vendor/popper.js',

		// Bootstrap
		
        'scripts/vendor/bootstrap/bootstrap.js', // En attendant de trouver une solution
        // 'scripts/vendor/bootstrap/alert.js',
        // 'scripts/vendor/bootstrap/button.js',
        // 'scripts/vendor/bootstrap/carousel.js',
        // 'scripts/vendor/bootstrap/collapse.js',
        // 'scripts/vendor/bootstrap/dropdown.js',
        // 'scripts/vendor/bootstrap/index.js',
        // 'scripts/vendor/bootstrap/modal.js',
        // 'scripts/vendor/bootstrap/popover.js',
        // 'scripts/vendor/bootstrap/scrollspy.js',
        // 'scripts/vendor/bootstrap/tab.js',
        // 'scripts/vendor/bootstrap/toast.js',
        // 'scripts/vendor/bootstrap/tooltip.js',
        // 'scripts/vendor/bootstrap/util.js',

        // LightGallery
        // 'scripts/vendor/lightGallery/lightgallery.min.js',
        // 'scripts/vendor/lightGallery/lg-autoplay.min.js',
        // 'scripts/vendor/lightGallery/lg-fullscreen.min.js',
        // 'scripts/vendor/lightGallery/lg-hash.min.js',
        // 'scripts/vendor/lightGallery/lg-pager.min.js',
        // 'scripts/vendor/lightGallery/lg-thumbnail.min.js',
        // 'scripts/vendor/lightGallery/lg-video.min.js',
        // 'scripts/vendor/lightGallery/lg-zoom.min.js'
    ].concat(jshint_src);


/*----------  Destinations  ----------*/

    // style
    var css_dir_final = './',

    // js
    js_file_final = 'scripts.min.js',
    js_dir_final = 'scripts/';


/*----------  Monitoring  ----------*/
    
    // style
    var watch_css = 'css/**/*.scss',

    // js
    watch_js = ['scripts/**/*.js', '!scripts/scripts.min.js'];

/*----------  Post CSS plugins  ----------*/

    var plugins = [
        autoprefixer({ browsers: ['last 2 versions', 'iOS 7'] }),
        cssnano()
    ];


/*----------  Commentaire pour le thème Wordpress  ----------*/
    
    var theme_name      = 'CIDFF45 Jeu Femmes et Citoyennes',
        theme_author    = 'artefacts.coop'
        theme_desc      = 'Un Quiz pour jouer et apprendre à se confronter à ses droits et devoirs de femme citoyenne.',
        wp_comment      = '/* \nTheme Name: '+theme_name+' \nAuthor: '+theme_author+' \nDescription: '+theme_desc+' \n*/\n\n';

/*----------  Fonctions  ----------*/

    // simplification du message d'erreur
    var display_error = function ( error ) {

        console.log( error.message );
        this.emit( 'end' );

    };
    
/*=====  FIN Initialisation  ======*/

/*=================================
=            Tâche CSS            =
=================================*/

// exécuter avec "gulp css"

function style() {
    
    // Actions
    return gulp
        .src( css_src )
        .pipe(plumber({ errorHandler: display_error }))
        .pipe(sass({ precision: 3 }))
        .pipe(combineMq())
        .pipe(postcss( plugins ))
        .pipe(banner( wp_comment ))
        .pipe(gulp.dest( css_dir_final ));

}

/*=====  FIN Tâche CSS  ======*/


/*========================================
=            Tâche Javascript            =
========================================*/

// Verification des scripts
function scriptsLint() {
    return gulp
        .src( jshint_src )
        .pipe(plumber({ errorHandler: display_error }))
        .pipe(jshint())
        .pipe(jshint.reporter( 'default' ));
}

// exécuter avec "gulp js"

function scripts() {

    // Actions
    return gulp
        .src( js_global_src )
        .pipe(plumber({ errorHandler: display_error }))
        .pipe(concat( js_file_final ))
        .pipe(uglify().on('error', console.error))
        .pipe(gulp.dest( js_dir_final ));

}

/*=====  FIN Tâche Javascript  ======*/


/*==================================
=            Monitoring            =
==================================*/

// exécuter avec "gulp watch"

function watchFiles() {
    gulp.watch( watch_css, style );
    gulp.watch( watch_js, gulp.series(scriptsLint, scripts));
}

const js = gulp.series(scriptsLint, scripts);
const css = gulp.series(style);
const build = gulp.series(gulp.parallel(css, js));
const watch = gulp.parallel(watchFiles);

exports.css = css;
exports.js = js;
exports.build = build;
exports.watch = watch;
exports.default = build;

/*=====  FIN Monitoring  ======*/